<?php
/**
 * Created by PhpStorm.
 * User: cofirazak
 * Date: 25/10/15
 * Time: 20:07
 */

namespace siteGallery\src;


class Front
{
    public static function autoload()
    {
        require_once __DIR__ . '/PdoFactory.php';
        require_once __DIR__ . '/Queries.php';
        require_once __DIR__ . '/TwigFactory.php';
        require_once __DIR__ . '/MonologFactory.php';
        require_once __DIR__ . '/../Settings.php';
    }

    public function __construct()
    {
        $this->autoload();
        $pdo = PdoFactory::newMysqlPdo();
        $this->log = MonologFactory::newMonolog();
        $twig = TwigFactory::newTwig('public_html', false);

        switch ($_POST['action']) {
            default:
                echo $twig->render('front.html', [

                ]);
        }
    }
}