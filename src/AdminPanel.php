<?php
/**
 * PHP version 5.6
 * @author   cofirazak <cofirazak@gmail.com>
 * @license  http://choosealicense.com/licenses/no-license/ Copyright 2015 cofirazak
 * Date: 2015-09-01 23:07
 * IDE: PhpStorm
 */

namespace siteGallery\src;

class AdminPanel
{
    /**
     * @var \Monolog\Logger
     */
    private $log;

    public function __construct()
    {
        $this->autoload();
        $this->main();
    }

    public static function autoload()
    {
        require_once 'vendor/autoload.php';
        require_once 'Settings.php';
        require_once 'src/PdoFactory.php';
        require_once 'src/Queries.php';
        require_once 'src/TwigFactory.php';
        require_once 'src/MonologFactory.php';
    }

    private function main()
    {
        $action = isset($_POST['action']) ? $_POST['action'] : '';
        $this->log = MonologFactory::newMonolog('AdminPanel');
        $twig = TwigFactory::newTwig('public_html/templates', false);
        $pdo = PdoFactory::newMysqlPdo();
        $queryGetCategories = 'SELECT category_id, category_name FROM categories';
        $queryGetProductTypes = 'SELECT product_type_id, product_type_name FROM typesofproduct';
        $queryGetProducts = 'SELECT product_id, product_name FROM products';
        $categories = [];
        $productTypes = [];
        switch ($action) {
            case 'addCategory':
                $queryAdditionPrepare = "INSERT INTO categories (category_name) VALUES(:name)";
                $queryAddition = $pdo->prepare($queryAdditionPrepare);
                $queryAddition->bindValue(':name', $_POST['categoryName']);
                if (!$pdoQueryResult = $queryAddition->execute()) {
                    $this->log->addError(
                        'Line:' . __LINE__ . " Код 610. Не получилось выполнить запрос: {$queryAdditionPrepare}",
                        [$_POST['categoryName']]
                    );
                } else {
                    $categories = Queries::executeQuery(
                        $queryGetCategories,
                        'category_name',
                        'category_id'
                    );
                }
                echo $twig->render('adminListCategory.html', [
                    'categoriesOption' => $categories,
                    'error' => isset($categories['error']) ? $categories['error'] : ''
                ]);
                break;
            case 'updateCategory':
                $updateQueryPrepare = "UPDATE categories SET category_name = :name WHERE category_id = :id";
                $updateQuery = $pdo->prepare($updateQueryPrepare);
                $updateQuery->bindValue(':name', $_POST['updateCategoryName']);
                $updateQuery->bindValue(':id', $_POST['categoryId']);
                if (!$pdoQueryResult = $updateQuery->execute()) {
                    $this->log->addError(
                        'Line:' . __LINE__ . " Код 610. Не получилось выполнить запрос: {$updateQueryPrepare}",
                        [$_POST['updateCategoryName'], $_POST['categoryId']]
                    );
                } else {
                    $categories = Queries::executeQuery(
                        $queryGetCategories,
                        'category_name',
                        'category_id'
                    );
                }
                echo $twig->render('adminListCategory.html', [
                    'categoriesOption' => $categories,
                    'error' => isset($categories['error']) ? $categories['error'] : ''
                ]);
                break;
            case 'deleteCategory':
                $queryDeletionPrepare = "DELETE FROM categories WHERE category_id = :id";
                $queryDeletion = $pdo->prepare($queryDeletionPrepare);
                $queryDeletion->bindValue(':id', $_POST['categoryId']);
                if (!$pdoQueryResult = $queryDeletion->execute()) {
                    $this->log->addError(
                        'Line:' . __LINE__ . " Код 612. Не получилось выполнить запрос: {$queryDeletionPrepare}",
                        [$_POST['categoryId']]
                    );
                } else {
                    $categories = Queries::executeQuery(
                        $queryGetCategories,
                        'category_name',
                        'category_id'
                    );
                }
                echo $twig->render('adminListCategory.html', [
                    'categoriesOption' => $categories,
                    'error' => isset($categories['error']) ? $categories['error'] : ''
                ]);
                break;
            case 'addProductType':
                $queryAdditionPrepare = "INSERT INTO typesofproduct (product_type_name, category_id)
VALUES(:name,:categoryId)";
                $queryAddition = $pdo->prepare($queryAdditionPrepare);
                $queryAddition->bindValue(':name', $_POST['productTypeName']);
                $queryAddition->bindValue(':categoryId', $_POST['productCategoryId']);
                if (!$pdoQueryResult = $queryAddition->execute()) {
                    $this->log->addError(
                        'Line:' . __LINE__ . " Код 618. Не получилось выполнить запрос: {$queryAdditionPrepare}",
                        [$_POST['productTypeName']]
                    );
                } else {
                    $productTypes = Queries::executeQuery(
                        $queryGetProductTypes,
                        'product_type_name',
                        'product_type_id'
                    );
                }
                echo $twig->render('adminProductTypeList.html', [
                    'productTypeOption' => $productTypes,
                    'error' => isset($productTypes['error']) ? $productTypes['error'] : ''
                ]);
                break;
            case 'updateProductType':
                $queryUpdatePrepare = "UPDATE typesofproduct SET product_type_name=:name , category_id=:categoryId
WHERE product_type_id = :id";
                $updateQuery = $pdo->prepare($queryUpdatePrepare);
                $updateQuery->bindValue(':name', $_POST['updateProductTypeName']);
                $updateQuery->bindValue(':categoryId', $_POST['productCategoryId']);
                $updateQuery->bindValue(':id', $_POST['productTypeId']);
                if (!$pdoQueryResult = $updateQuery->execute()) {
                    $this->log->addError(
                        'Line:' . __LINE__ . " Код 618. Не получилось выполнить запрос: {$queryUpdatePrepare}",
                        [$_POST['updateProductTypeName'], [$_POST['productCategoryId']], [$_POST['productTypeId']]]
                    );
                } else {
                    $productTypes = Queries::executeQuery(
                        $queryGetProductTypes,
                        'product_type_name',
                        'product_type_id'
                    );
                }
                echo $twig->render('adminProductTypeList.html', [
                    'productTypeOption' => $productTypes,
                    'error' => isset($productTypes['error']) ? $productTypes['error'] : ''
                ]);
                break;
            case 'deleteProductType':
                $queryDeletionPrepare = "DELETE FROM typesofproduct WHERE product_type_id = :id";
                $queryDeletion = $pdo->prepare($queryDeletionPrepare);
                $queryDeletion->bindValue(':id', $_POST['productTypeId']);
                if (!$pdoQueryResult = $queryDeletion->execute()) {
                    $this->log->addError(
                        'Line:' . __LINE__ . " Код 616. Не получилось выполнить запрос: {$queryDeletionPrepare}",
                        [$_POST['productTypeItem']]
                    );
                    $productTypes['error'] = 621;
                } else {
                    $productTypes = Queries::executeQuery(
                        $queryGetProductTypes,
                        'product_type_name',
                        'product_type_id'
                    );
                }
                echo $twig->render('adminProductTypeList.html', [
                    'productTypeOption' => $productTypes,
                    'error' => isset($productTypes['error']) ? $productTypes['error'] : ''
                ]);
                break;
            case 'addProduct':
                $queryAddProduct = "INSERT INTO products
(product_name, description, size, price, product_image, product_type_id)
VALUES(:name, :description, :size, :price, :product_image, :product_type_id)
";
                try {
                    $queryAddition = $pdo->prepare($queryAddProduct);
                    $queryAddition->bindValue(':name', $_POST['productName']);
                    $queryAddition->bindValue(':description', $_POST['productDescription']);
                    $queryAddition->bindValue(':size', $_POST['productSize']);
                    $queryAddition->bindValue(':price', $_POST['productPrice']);
                    $queryAddition->bindValue(':product_image', $_POST['productImage']);
                    $queryAddition->bindValue(':product_type_id', $_POST['productTypeId']);
                    if (!$pdoQueryResult = $queryAddition->execute()) {
                            $this->log->addError(
                                'Line:' . __LINE__ . " Код 618. Не получилось выполнить запрос: {$queryAddProduct}",
                                [$_POST]
                            );
                    } else {
                        $products = Queries::executeQuery(
                            $queryGetProducts,
                            'product_name',
                            'product_id'
                        );
                        $productTypes['ok'] = 1;
                        $productTypes['products'] = $twig->render('adminDeleteProduct.html', [
                            'productOption' => $products
                        ]);
                    }
                } catch (\Exception $e) {
                    $productTypes['error'] = $e->getMessage();
                }
                echo json_encode($productTypes);
                break;
            case 'deleteProduct':
                $queryDeletionPrepare = "DELETE FROM products WHERE product_id = :id";
                $queryDeletion = $pdo->prepare($queryDeletionPrepare);
                $queryDeletion->bindValue(':id', $_POST['productId']);
                $products = [];
                if (!$pdoQueryResult = $queryDeletion->execute()) {
                    $this->log->addError(
                        'Line:' . __LINE__ . " Код 616. Не получилось выполнить запрос: {$queryDeletionPrepare}",
                        [$_POST['productTypeItem']]
                    );
                } else {
                    $products = Queries::executeQuery(
                        $queryGetProducts,
                        'product_name',
                        'product_id'
                    );
                }
                echo $twig->render('adminDeleteProduct.html', [
                    'productOption' => $products,
                    'error' => isset($products['error']) ? $products['error'] : ''
                ]);
                break;
            case 'uploadImages':
                $uploads_dir = __DIR__ . '/../public_html/images';
                foreach ($_FILES["productImage"]["error"] as $key => $error) {
                    if ($error == UPLOAD_ERR_OK) {
                        $tmp_name = $_FILES["productImage"]["tmp_name"][$key];
                        $name = $_FILES["productImage"]["name"][$key];
                        move_uploaded_file($tmp_name, "$uploads_dir/$name");
                    }
                }
                $this->defaultAdminPage($queryGetCategories, $queryGetProductTypes, $queryGetProducts, $twig);
                break;
            case 'deleteImage':
                if (file_exists($_POST['deleteProductImage'])) {
                    unlink($_POST['deleteProductImage']);
                }
                $productImages = $this->getImagesList();
                echo $twig->render('adminImagesList.html', [
                    'productImageOption' => $productImages,
                ]);
                break;
            default:
                $this->defaultAdminPage($queryGetCategories, $queryGetProductTypes, $queryGetProducts, $twig);
        }
    }

    /**
     * Description
     * @return array
     */
    private function getImagesList()
    {
        $directory = new \DirectoryIterator(__DIR__ . '/../public_html/images');
        $productImages = [];
        foreach ($directory as $file) {
            if ($file->isFile()) {
                $productImages[$file->getPathname()] = $file->getFilename();
            }
        }
        return $productImages;
    }

    /**
     * @param $queryGetCategories
     * @param $queryGetProductTypes
     * @param $queryGetProducts
     * @param \Twig_Environment $twig
     */
    private function defaultAdminPage($queryGetCategories, $queryGetProductTypes, $queryGetProducts, $twig)
    {
        $productImages = $this->getImagesList();
        $categories = Queries::executeQuery($queryGetCategories, 'category_name', 'category_id');
        $productTypes = Queries::executeQuery($queryGetProductTypes, 'product_type_name', 'product_type_id');
        $products = Queries::executeQuery($queryGetProducts, 'product_name', 'product_id');
        $error = '';
        if (isset($categories['error'])) {
            $error = $categories['error'];
        } elseif (isset($productTypes['error'])) {
            $error = $productTypes['error'];
        } elseif (isset($products['error'])) {
            $error = $products['error'];
        }
        echo $twig->render('adminPanel.html', [
            'categoriesOption' => $categories,
            'productTypeOption' => $productTypes,
            'productImageOption' => $productImages,
            'productOption' => $products,
            'error' => $error
        ]);
    }
}
