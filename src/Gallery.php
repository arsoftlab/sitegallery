<?php

/**
 * PHP version 5.6
 * @author   cofirazak <cofirazak@gmail.com>
 * @license  http://choosealicense.com/licenses/no-license/ Copyright 2015 cofirazak
 * Date: 2015-09-22 00:14
 * IDE: PhpStorm
 */

namespace siteGallery\src;

class Gallery
{
    public static function autoload()
    {
        require_once __DIR__ . '/PdoFactory.php';
        require_once __DIR__ . '/Queries.php';
        require_once __DIR__ . '/TwigFactory.php';
        require_once __DIR__ . '/MonologFactory.php';
        require_once __DIR__ . '/../Settings.php';
    }

    public function __construct()
    {
        $this->autoload();
        $pdo = PdoFactory::newMysqlPdo();
        $this->log = MonologFactory::newMonolog();
        $twig = TwigFactory::newTwig('public_html', false);
        switch ($_POST['action']) {
            case 'updateProductList':
                $getImages = 'SELECT * FROM products WHERE product_type_id = :id';
                $prepareGetImages = $pdo->prepare($getImages);
                $prepareGetImages->bindValue(':id', $_POST['productTypeId']);
                if (!$pdoQueryResult = $prepareGetImages->execute()) {
                    $this->log->addError(
                        'Line:' . __LINE__ . " Код 618. Не получилось выполнить запрос: {$getImages}",
                        [$_POST['productTypeId']]
                    );
                } else {
                    foreach ($prepareGetImages->fetchAll() as $data) {
                        $result[] = $data;
                    }
                    echo $twig->render('templates/galleryListConteiner.html', [
                        'galmin' => $result
                    ]);
                }
                break;
            default:
                $categories = Queries::executeQuery(
                    'SELECT category_id, category_name FROM categories',
                    'category_name',
                    'category_id'
                );
                $categoryResult = [];
                foreach ($categories as $key => $categoryName) {
                    $query = 'SELECT product_type_id, product_type_name FROM typesofproduct WHERE category_id = :id';
                    $preparedQuery = $pdo->prepare(
                        $query
                    );
                    $preparedQuery->bindValue(':id', $key);
                    if (!$pdoQueryResult = $preparedQuery->execute()) {
                        $this->log->addError(
                            'Line:' . __LINE__ . " Код 618. Не получилось выполнить запрос: {$query}",
                            [$key]
                        );
                    } else {
                        $categoryResult[$categoryName] = [];
                        $product_type_id = '';
                        foreach ($preparedQuery->fetchAll() as $data) {
                            foreach ($data as $key => $value) {
                                if ($key === 'product_type_id') {
                                    $product_type_id = $value;
                                }
                                if ($key === 'product_type_name') {
//                            array_push($categoryResult[$categoryName], $value);
                                    $categoryResult[$categoryName][$product_type_id] = $value;
                                }
                            }
                        }
                    }
                }
                echo $twig->render('gallery.html', [
                    'categoryList' => $categoryResult
                ]);
        }
    }
}
