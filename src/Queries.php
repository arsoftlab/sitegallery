<?php
/**
 * PHP version 5.6
 * @author   cofirazak <cofirazak@gmail.com>
 * @license  http://choosealicense.com/licenses/no-license/ Copyright 2015 cofirazak
 * Date: 2015-09-23 23:38
 * IDE: PhpStorm
 */

namespace siteGallery\src;

class Queries
{
    private function __construct()
    {
    }

    /**
     * Description
     * @param string $query
     * @param string $value
     * @param string $key
     * @return array
     */
    public static function executeQuery($query, $value, $key)
    {
        $result = [];
        $pdo = PdoFactory::newMysqlPdo();
        $log = MonologFactory::newMonolog();
        if (!$pdoQueryResult = $pdo->query($query)) {
            $log->addError(
                'Line:' . __LINE__ . " Код 614. Не получилось выполнить запрос: {$query}"
            );
            $result['error'] = 614;
        } else {
            foreach ($pdoQueryResult as $row) {
                $result[$row[$key]] = $row[$value];
            }
        }

        return $result;
    }
}
