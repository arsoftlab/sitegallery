<?php
/**
 * PHP version 5.6
 * @author   cofirazak <cofirazak@gmail.com>
 * @license  http://choosealicense.com/licenses/no-license/ Copyright 2015 cofirazak
 * Date: 2015-09-17 22:19
 * IDE: PhpStorm
 */

namespace siteGallery\src;

class PdoFactory
{
    private function __construct()
    {
    }

    /**
     * Array of pdo connections
     * @var \PDO[] array
     */
    static protected $instances = [];

    /**
     * Logger instance
     * @var \Monolog\Logger
     */
    private static $log;

    private static function getLoggerInstance()
    {
        $calledClass = explode('\\', get_called_class());
        $calledClass = end($calledClass);
        self::$log = MonologFactory::newMonolog($calledClass);
    }

    /**
     * mysql PDO connection factory
     * @param string $connectionName
     * @return \PDO
     */
    public static function newMysqlPdo($connectionName = 'defaultConnection')
    {
        if (!array_key_exists($connectionName, self::$instances)) {
            self::getLoggerInstance();
            try {
                $pdo = new \PDO(
                    sprintf(
                        'mysql:host=%s;dbname=%s;port=%s;charset=%s',
                        \Settings::$mysql['host'],
                        \Settings::$mysql['dbname'],
                        \Settings::$mysql['port'],
                        \Settings::$mysql['charset']
                    ),
                    \Settings::$mysql['username'],
                    \Settings::$mysql['password']
                );
            } catch (\PDOException $e) {
                $host = \Settings::$mysql['host'];
                self::$log->addError(
                    'Line:' . __LINE__ . " Код 615. Не удалось создать PDO инстанс: {$e->getMessage()}. Хост:{$host}"
                );
                exit;
            }
            $pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            self::$instances[$connectionName] = $pdo;
        }

        return self::$instances[$connectionName];
    }
}
