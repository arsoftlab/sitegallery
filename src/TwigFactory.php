<?php
/**
 * PHP version 5.6
 * @author   cofirazak <cofirazak@gmail.com>
 * @license  http://choosealicense.com/licenses/no-license/ Copyright 2015 cofirazak
 * Date: 2015-09-17 22:19
 * IDE: PhpStorm
 */

namespace siteGallery\src;

class TwigFactory
{
    /**
     * Array of twig instances
     * @var \Twig_Environment[] array
     */
    static protected $instances = [];

    /**
     * Twig factory
     * @param string $twigPath
     * @param bool $twigCache
     * @return \Twig_Environment
     */
    public static function newTwig($twigPath =  '../public_html/templates', $twigCache = true)
    {
        self::autoload();
        if (!array_key_exists($twigPath, self::$instances)) {
            $cache = $twigCache ? ['cache' => $twigPath . '/compilation_cache'] : [];
            $loader = new \Twig_Loader_Filesystem($twigPath);
            self::$instances[$twigPath] = new \Twig_Environment(
                $loader,
                $cache
            );
        }

        return self::$instances[$twigPath];
    }

    private static function autoload()
    {
        require_once __DIR__ . '/../vendor/autoload.php';
    }
}
