<?php
/**
 * PHP version 5.6
 * @author   cofirazak <cofirazak@gmail.com>
 * @license  http://choosealicense.com/licenses/no-license/ Copyright 2015 cofirazak
 * Date: 2015-09-17 22:19
 * IDE: PhpStorm
 */

namespace siteGallery\src;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class MonologFactory
{
    /**
     * Array of loggers
     * @var Logger[] array
     */
    static protected $instances = [];

    private static function autoload()
    {
        require_once __DIR__ . '/../vendor/autoload.php';
    }

    /**
     * Monolog logger factory
     * @param string $logName Example: called className
     * @return Logger Returns a monolog instance
     */
    public static function newMonolog($logName = 'defaultLogName')
    {
        if (!array_key_exists($logName, self::$instances)) {
            self::autoload();
            $calledClass = explode('\\', get_called_class());
            $calledClass = end($calledClass);
            self::$instances[$logName] = new Logger($calledClass);
            $streamHandler = new StreamHandler("logs/{$calledClass}.log");
            $lineFormatter = new LineFormatter(null, null, false, true);
            $streamHandler->setFormatter($lineFormatter);
            self::$instances[$logName]->pushHandler($streamHandler);
        }
        return self::$instances[$logName];
    }
}
