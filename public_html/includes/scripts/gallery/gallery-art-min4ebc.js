var cookie = new com.art.core.cookie.Cookie();
function linkForm(a) {
    var b;
    b = document.getElementById("globalLinkForm");
    if (!b) {
        b = document.getElementById("aspnetForm")
    }
    b.action = a;
    b.submit()
}
function swapFooter(a) {
    document.getElementById(a).style.display = "none";
    document.getElementById(a + "_a").style.display = "block";
    document.getElementById(a).parentNode.style.backgroundColor = "#CDC2B1"
}
function restoreFooter(a) {
    document.getElementById(a + "_a").style.display = "none";
    document.getElementById(a).style.display = "block";
    document.getElementById(a).parentNode.style.backgroundColor = "#E7E2D9"
}
function opencat(b) {
    var a = document.getElementById(b);
    if (a) {
        if (a.style.display == "") {
            a.style.display = "none"
        } else {
            a.style.display = ""
        }
    }
}
function styledisp(b, a) {
    var b = document.getElementById(b);
    if (b) {
        b.style.display = a
    }
}
function curl(a) {
    window.location = a.replace(/___/g, "'")
}
function ShipSameDayWindow(a, b) {
    ShipWindow = window.open("", "ShipTime", "height=320,width=430,resizable=yes,top=100,left=100");
    var c = ShipWindow.document;
    c.write("<html><head><title>Ship Time</title>");
    c.write('</head><body><p style="font-family:Verdana;font-size:12px;"><b>Ship Time</b></p>');
    if (!a) {
        if (b == 3) {
            c.write('<p style="font-family:Verdana;font-size:12px;line-height:120%">Items that are designated by "Usually ships within 24 hours" normally leave our facilities on the same business day if the order is placed before 5:00pm EST.</p>')
        } else {
            c.write('<p style="font-family:Verdana;font-size:12px;line-height:120%">Items that are designated by "Usually ships within 24 hours" normally leave our facilities on the same business day if the order is placed before 16:00GMT.</p>')
        }
    }
    c.write('<p style="font-family:Verdana;font-size:12px;line-height:120%">Ship time indicates the typical number of business days it takes for your item(s) to leave our facilities but does not include transit time from our facilities to the final destination.</p>');
    c.write('<p style="font-family:Verdana;font-size:12px;line-height:120%">Orders that contain multiple items with different ship times will be shipped out based on the item with the longest ship time.</p>');
    if (!isTrustedDomain()) {
        c.write('<p style="font-family:Verdana;font-size:12px;line-height:120%"><b>Please note: Ship time is determined based on the method of payment chosen.</b></p>');
        c.write("<p><div style=\"font-family:Verdana;font-size:12px;cursor:pointer;color:#000077\" onmouseover=\"this.style.textDecoration='underline'\" onmouseout=\"this.style.textDecoration='none'\" onclick=\"window.open('http://www.romvek.ru/asp/customerservice/shipping.asp','','height=678,width=900,scrollbars=yes,resizable=yes,titlebar=yes,toolbar=yes,menubar=yes')\">Shipping FAQ</div>");
        c.write("<div style=\"font-family:Verdana;font-size:12px;cursor:pointer;color:#000077\" onmouseover=\"this.style.textDecoration='underline'\" onmouseout=\"this.style.textDecoration='none'\" onclick=\"window.open('http://www.romvek.ru/asp/ship_rates.asp','','height=678,width=900,scrollbars=yes,resizable=yes,titlebar=yes,toolbar=yes,menubar=yes')\">Shipping Rates</div><br/></p>")
    }
    c.write("</body></html>");
    c.close()
}
function setCookie(a, d, c) {
    var b = new Date();
    b.setDate(b.getDate() + c);
    document.cookie = a + "=" + d + ((c == null) ? "" : ";expires=" + b.toGMTString()) + ";path=/"
}
function getCookie(a) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(a + "=");
        if (c_start != -1) {
            c_start = c_start + a.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = document.cookie.length
            }
            return unescape(document.cookie.substring(c_start, c_end))
        }
    }
    return ""
}
function checkcookie() {
    alert("all cookie:" + document.cookie);
    alert("ENDECA cookie:" + getCookie("ENDECA"))
}
function selectNs(b) {
    setEndecaCookie(b, "&Ns=");
    var a = redirectNtt(changePg(window.location, 1));
    window.location = a
}
function selectNn(d, b, a, e) {
    setEndecaCookie(d, "&Nn=");
    cookie.setCookieSession("WT", "WT.cg_n", "Gallery+Number+Change");
    var c = calcPageNumber(d, b, a, e);
    var f = redirectNtt(changePg(window.location, c));
    window.location = f
}
function calcPageNumber(c, b, a, d) {
    var c = parseInt((parseInt(b) * parseInt(a - 1)) / parseInt(c)) + 1;
    return c
}
function redirectNtt(b) {
    if (ntt && ntt.length > 0 && b.indexOf("searchstring") == -1) {
        var a = b.lastIndexOf("http://www.romvek.ru/");
        if (a > -1) {
            b = b.substring(0, a) + b.substring(a) + "&searchstring=" + ntt
        }
    }
    return b
}
var ntt;
function addSearchString(a) {
    this.ntt = a
}
function gobacktosearch(a) {
    this.ntt = a;
    var b = window.location.toString();
    b = b.replace(/___/g, "'");
    window.location = redirectNtt(b)
}
function setEndecaCookie(g, e) {
    var a = getCookie("ENDECA");
    if (a.length > 0) {
        a = a.replace(/\+\&\+/g, "___");
        var f = a.split("&");
        var d = "";
        var b = false;
        for (var c = 0; c < f.length; c++) {
            if (f[c].indexOf(e) == 0) {
                f[c] = e + g;
                b = true
            }
            d += "&" + f[c]
        }
        a = d.substring(1);
        if (!b) {
            a += "&" + e + g
        }
    } else {
        a = e + g
    }
    a = a.replace(/&&/g, "&");
    a = a.replace(/___/g, escape("+&+"));
    setCookie("ENDECA", a, 365)
}
function changePg(j, h) {
    var i = j.toString();
    var a = i.indexOf("_p");
    if (h == 1) {
        if (a > 0) {
            var b = i.substring(0, a);
            var e = i.indexOf(".", a);
            var d = i.substring(e);
            i = b + d
        }
    }
    if (h > 1) {
        var c = i.lastIndexOf("_p");
        if (c > 0) {
            var f = i.indexOf(".", c);
            var g = i.substring(f);
            i = i.substring(0, c) + "_p" + h + g
        }
    }
    return i
}
var XMLHttpFactories = [function () {
    return new XMLHttpRequest()
}, function () {
    return new ActiveXObject("Msxml2.XMLHTTP")
}, function () {
    return new ActiveXObject("Msxml3.XMLHTTP")
}, function () {
    return new ActiveXObject("Microsoft.XMLHTTP")
}];
function isCurrentPageInSsl() {
    var a = window.location.protocol;
    if (a.indexOf("https") >= 0) {
        return true
    } else {
        return false
    }
}
var WTFlag = false;
function requestHeaderData(a) {
    if (a != undefined) {
        WTFlag = true
    }
    var c = "";
    if (isCurrentPageInSsl()) {
        c = "&ssl=true"
    }
    var b = Math.floor(Math.random() * 10000);
    theURL = "http://www.romvek.ru/asp/include/global/getHeader.asp?ui=" + getuid() + "&r=" + b + c + "&clientcall=YES";
    req = com.art.core.utils.BrowserUtil.createXMLHTTPObject();
    req.open("http://www.romvek.ru/shop_images/id--b1824/GET", theURL, true);
    req.onreadystatechange = getHeaderResponse;
    req.send(null);
    requestFooterData()
}
function getHeaderResponse() {
    if (req.readyState == 4) {
        var c = document.getElementById("ctl00_ctl00_mc_hdr_headersection");
        if (c != null) {
            var d = req.responseText;
            try {
                if (WTFlag) {
                    var a = d.indexOf("<!-- @@Webtrends  -->");
                    var f = d.indexOf("<!-- @@Webtrends  -->", a + 1);
                    if (a > 0 && f > 0) {
                        d = d.substring(0, a + 21) + " " + d.substring(f)
                    }
                    c.innerHTML = d
                } else {
                    c.innerHTML = (req.responseText)
                }
                var h = 1000;
                $("#Header > div").each(function () {
                    $(this).css("zIndex", h);
                    h -= 1
                })
            } catch (b) {
                var g = document.createElement("div");
                g.innerHTML = req.responseText;
                c.removeChild(c.firstChild);
                c.appendChild(g)
            }
            hidePromo()
        }
    }
}
function getuid() {
    var a = document.getElementById("uid");
    if (a) {
        return a.value
    } else {
        return ""
    }
}
function hidePromo() {
    hideElem("dhtml_Email_Container");
    hideElem("CSModuleChangeLanguageContainer");
    hideElem("CSModuleChangeCountryContainer")
}
function hideElem(b) {
    var a = document.getElementById(b);
    if (a) {
        a.style.display = "none"
    }
}
var reqObj, req, theURL;
function requestFooterData() {
    var a = "";
    if (isCurrentPageInSsl()) {
        a = "&ssl=true"
    }
    theURL = "http://www.romvek.ru/asp/include/global/getFooter.asp?ui=" + getuid() + a;
    reqObj = com.art.core.utils.BrowserUtil.createXMLHTTPObject();
    reqObj.open("http://www.romvek.ru/shop_images/id--b1824/GET", theURL, true);
    reqObj.onreadystatechange = getFooterResponse;
    reqObj.send(null)
}
function getFooterResponse() {
    if (reqObj.readyState == 4) {
        var a = document.getElementById("ctl00_ctl00_mc_ftr_footersection");
        if (a != null) {
            var b = reqObj.responseText;
            if (b.indexOf("PageContainer") > 0) {
                b = b.substring(b.indexOf("PageContainer") - 14);
                var c = b.indexOf("<script");
                if (c > 0) {
                    b = b.substring(0, c)
                }
                a.innerHTML = b
            }
        }
    }
}
function fireSearch(a) {
    var c = document.getElementById(a);
    if (c) {
        var d = c.value;
        d = d.replace(/@/g, "").replace(/#/g, "").replace(/$/g, "").replace(/%/g, "").replace(/^/g, "");
        d = d.replace(/^\s+|\s+$/g, "");
        if (d.length == 0) {
            document.getElementById("yoursearch").innerHTML = "Please enter a valid search term.";
            c.value = "";
            return
        }
        var b = document.forms.aspnetForm;
        if (!b) {
            b = document.aspnetForm
        }
        b.action = "http://www.romvek.ru/asp/search_do-asp/_/posters.htm?ID=0&amp;searchstring=" + escape(d) + "&ui=" + gup("ui");
        b.submit()
    }
}
function gup(a) {
    a = a.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var c = "[\\?&]" + a + "=([^&#]*)";
    var b = new RegExp(c);
    var d = b.exec(window.location.href);
    if (d == null) {
        return ""
    } else {
        return d[1]
    }
}
function popOpen(c, b, a) {
    windowHandle = window.open(c, b, a)
}
var judyGalleryEntryPoint = function (b) {
    var a = false;
    if (!com.art.core.utils.BusinessUtil.isMobile()) {
        if ($("#divJudyGalleryEntryCmsContent").html().trim().length > 0) {
            if (typeof(b) == "undefined") {
                console.log("No filter mentioned.All contents will be shown")
            }
            $(".judyEntryGalleryParent").each(function () {
                var g = $(this).data("insertatindex");
                var f = $(this).data("abtest");
                if (typeof(b) == "undefined" || (typeof(f) != "undefined" && f.toLowerCase() == b.toLowerCase())) {
                    console.log("Inserting AD at index =>> " + g);
                    a = true;
                    c($(this), g);
                    $(this).click(function () {
                        d($(this), g, f)
                    })
                }
            });
            if (!a) {
                console.log("No matching content found")
            }
        } else {
            console.log("No Promo content found")
        }
    } else {
        console.log("MObile found => not firing promo")
    }
    function c(f, g) {
        var h = g + 1;
        $(".galmin .galThumbContainer:nth-child(" + h + ")").before($(f));
        $(f).removeClass("hidden").addClass("active")
    }

    function e(f, g) {
        gemini2GA = new com.art.core.tracking.GoogleAnalytics("Judy");
        gemini2GA.trackEventWithCategory(f, g)
    }

    function d(h, j, f) {
        if (typeof(h) != "undefined") {
            var k = "";
            if ($(h).hasClass("judyEntryGallery-W1")) {
                k = "Gallery Page inline - small"
            } else {
                if ($(h).hasClass("judyEntryGallery-W2")) {
                    k = "Gallery Page inline - big"
                } else {
                    if ($(h).hasClass("judyEntryGallery-W3")) {
                        k = "Gallery Page inline - large"
                    } else {
                        if ($(h).hasClass("judyEntryGallery-W4")) {
                            k = "Gallery Page inline - full"
                        }
                    }
                }
            }
            var g = "Gemini Entry Points";
            var i = k + ", index:" + j + ", A/B:" + f.toUpperCase();
            console.log("Promo clicked => " + i + ". Firing GA");
            e(g, i)
        }
    }
};
function rollover(a, b, c) {
    a.src = a.src.replace(b, c)
}
function rolloverparent(c, d, e) {
    for (var b = 0; b < c.childNodes.length; b++) {
        var a = c.childNodes[b];
        if (a.nodeName == "IMG") {
            a.src = a.src.replace(d, e)
        }
    }
}
function GotoStudio(a, g, c, e, d, k, m, f, b) {
    var o;
    var j = false;
    var i = 10;
    var h = 10;
    var l = "apnum=" + a + "&PODConfigID=" + k;
    var n = "FrameStudio" + new Date().getTime();
    o = window.open("http://www.romvek.ru/FrameStudio/default.asp?" + l + "&ui=" + m + "&customerzoneid=" + f, n, "toolbar=no,location=no,status=no,statusbar=no,menubar=no,height=678,width=900,scrollbars=no,resizable=no,top=" + i + ",left=" + h);
    if (o) {
        if (o.focus) {
            o.focus()
        }
    } else {
        window.location.href = "http://www.romvek.ru/FrameStudio/error.asp?error=popup&amp;" + l + "&ui=" + m
    }
}
function GotoStudioInline(a, g, c, e, d, i, k, f, b, h, l) {
    var m;
    var j = "apnum=" + a + "&PODConfigID=" + i + "&pd=" + h + "&sp=" + l;
    m = "http://www.romvek.ru/FrameStep/default.asp?" + j + "&ui=" + k + "&customerzoneid=" + f;
    window.location.href = m
}
function toggle(c, a) {
    var b = document.getElementById(c);
    if (a.innerHTML == "close") {
        a.innerHTML = "learn more";
        b.style.display = "none"
    } else {
        a.innerHTML = "close";
        b.style.display = "block"
    }
};
var mycarousel_itemList = new Array();
function fnLoadImages() {
    if ($("#ctl00_ctl00_mc_mc_dimsrch_hdnImageUrl").length > 0) {
        var p = document.getElementById("ctl00_ctl00_mc_mc_dimsrch_hdnImageUrl");
        var j = document.getElementById("ctl00_ctl00_mc_mc_dimsrch_hdnGalleryUrl");
        var a = document.getElementById("ctl00_ctl00_mc_mc_dimsrch_hdnAltText");
        var e = document.getElementById("ctl00_ctl00_mc_mc_dimsrch_hdnDimensionName");
        var l = document.getElementById("ctl00_ctl00_mc_mc_dimsrch_hdnImageHght");
        var r = document.getElementById("ctl00_ctl00_mc_mc_dimsrch_hdnImageWidth");
        var n = document.getElementById("ctl00_ctl00_mc_mc_dimsrch_hdnImageMarginTop");
        var c = document.getElementById("ctl00_ctl00_mc_mc_dimsrch_hdnDimensionCnt");
        var g = document.getElementById("ctl00_ctl00_mc_mc_dimsrch_hdnGalleryItemStr");
        var q = p.innerHTML.split(";");
        var k = j.innerHTML.split(";");
        var b = a.innerHTML.split(";");
        var f = e.innerHTML.split(";");
        var m = l.innerHTML.split(";");
        var s = r.innerHTML.split(";");
        var o = n.innerHTML.split(";");
        var d = c.innerHTML.split(";");
        var h = g.innerHTML.split(";");
        for (i = 0; i < q.length - 1; i++) {
            var t = new Object();
            t.ImgSrc = q[i];
            t.GalUrl = k[i];
            t.AltTxt = b[i];
            t.DimName = f[i];
            t.ImgHeight = m[i];
            t.ImgWidth = s[i];
            t.ImgMarginTop = o[i];
            t.ItemCount = d[i];
            t.ParentPath = h[i];
            mycarousel_itemList[i] = t
        }
    }
}
function mycarousel_itemLoadCallback(a, c) {
    for (var b = a.first; b <= a.last; b++) {
        if (a.has(b)) {
            continue
        }
        if (b > mycarousel_itemList.length) {
            break
        }
        a.add(b, mycarousel_getItemHTML(mycarousel_itemList[b - 1]))
    }
}
function fnRedirectToUrl(a) {
    location.href = a
}
function mycarousel_getItemHTML(b) {
    b.ImgMarginTop = 82 - (b.ImgHeight);
    var a = '<a class="jcarousel-atag" href="' + b.GalUrl + '" onclick="setDMCookie()"; title="' + b.AltTxt + ' Gallery"><div class="jcarousel-imgParent"><logo class="shadow" style="margin-top:' + b.ImgMarginTop + 'px;" src="' + b.ImgSrc + '" width="' + b.ImgWidth + '" height="' + b.ImgHeight + '" alt="' + b.AltTxt + '" onclick="fnRedirectToUrl(\'' + b.GalUrl + "');\" /></div>";
    a += '<div class="jcarousel-text"><span>' + b.DimName + '</span> <span class="jcarousel_nodeco_text">in</span> <span>' + b.ParentPath + "</span></div>";
    a += "</a>";
    return a
}
jQuery(document).ready(function () {
    fnLoadImages();
    jQuery("#mycarousel").jcarousel({
        scroll: 5,
        visible: 5,
        size: mycarousel_itemList.length,
        itemLoadCallback: {onBeforeAnimation: mycarousel_itemLoadCallback}
    });
    $(".dmsrchseeall").hover(function () {
        $(this).css("text-decoration", "underline")
    }, function () {
        $(this).css("text-decoration", "none")
    })
});
function createonclickforli() {
    $(".jcarousel-item").each(function (a) {
        var b = "curl('" + $(this).find(".jcarousel-atag").attr("href") + "')";
        $(this).attr("onclick", b)
    })
}
function Comma(b) {
    b = "" + b;
    if (b.length > 3) {
        var a = b.length % 3;
        var c = (a > 0 ? (b.substring(0, a)) : "");
        for (i = 0; i < Math.floor(b.length / 3); i++) {
            if ((a == 0) && (i == 0)) {
                c += b.substring(a + 3 * i, a + 3 * i + 3)
            } else {
                c += "," + b.substring(a + 3 * i, a + 3 * i + 3)
            }
        }
        return (c)
    } else {
        return b
    }
}
function setDMCookie() {
    Set_wt_Cookie("WTDS", "1", 365, "http://www.romvek.ru/", "", "")
}
function Set_wt_Cookie(d, h, b, e, a, f) {
    var g = new Date();
    g.setTime(g.getTime());
    if (b) {
        b = b * 1000 * 60 * 60 * 24
    }
    var c = new Date(g.getTime() + (b));
    document.cookie = d + "=" + escape(h) + ((e) ? ";path=" + e : "") + ((a) ? ";domain=" + a : "") + ((f) ? ";secure" : "")
};
var popup, displayBoxPos, isWorking, divobj, req;
isWorking = false;
function GetPopupDivByName() {
    var a = document.getElementById("popupplaceholder");
    if (a == null) {
        a = document.body.insertBefore(document.createElement("div"), document.body.firstChild);
        a.style.position = "absolute";
        a.style.display = "block";
        a.style.zIndex = 1001;
        a.id = "popupplaceholder"
    }
    return a
}
function CallbackReload(b) {
    if (isWorking) {
        return
    }
    isWorking = true;
    var a = b.indexOf("http://www.romvek.ru/asp/");
    if (a > 0) {
        b = b.substring(a)
    }
    req = com.art.core.utils.BrowserUtil.createXMLHTTPObject();
    req.open("http://www.romvek.ru/shop_images/id--b1824/GET", b + "&rnd=" + Math.random(), true);
    req.onreadystatechange = ReloadPage;
    req.send(null);
    return true
}
function ReloadPage() {
    if (req.readyState == 4) {
        var b = window.location.toString();
        var a = b.length;
        if (b.substring(a - 1) == "#") {
            b = b.substring(0, a - 1)
        }
        window.location = b
    }
}
function PopupCallback(b, a) {
    if (isWorking) {
        return
    }
    isWorking = true;
    req = com.art.core.utils.BrowserUtil.createXMLHTTPObject();
    var c = "http://www.romvek.ru/asp/pop_up/popup_callback.asp?tmpl=" + b + a + "&rnd=" + Math.random();
    req.open("http://www.romvek.ru/shop_images/id--b1824/GET", c, true);
    req.onreadystatechange = CallbackResponse;
    req.send(null);
    return true
}
function CallbackResponse() {
    if (req.readyState == 4) {
        var response = eval("(" + req.responseText + ")");
        var template = response.jsonRecord.Template.replace(/&quot;/g, '"');
        popup = GetPopupDivByName();
        if (popup != null) {
            $popup = $("#popupplaceholder");
            $popup.html(template);
            popup.style.top = (displayBoxPos[1]) + "px";
            popup.style.left = (displayBoxPos[0] - 75) + "px";
            if (displayBoxPos[0] < 100) {
                popup.style.left = "100px"
            }
            theItemTop = parseInt(popup.style.top);
            theItemLeft = parseInt(popup.style.left);
            var w = window, d = document, e = d.documentElement, g = d.getElementsByTagName("body")[0], x = w.innerWidth || e.clientWidth || g.clientWidth, y = w.innerHeight || e.clientHeight || g.clientHeight;
            qscreenHeight = y;
            qscreenWidth = x;
            var theScrollTop = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop;
            theScrollTop = parseInt(theScrollTop);
            if ((theItemTop - theScrollTop + $popup.height()) > (qscreenHeight)) {
                theItemTop = theScrollTop - (($popup.height()) - qscreenHeight) - 30
            } else {
                if (theItemTop < theScrollTop) {
                    theItemTop = theScrollTop
                }
            }
            if ((theItemLeft + $popup.width()) > (qscreenWidth)) {
                theItemLeft = qscreenWidth - $popup.width()
            }
            $popup.css("top", theItemTop);
            $popup.css("left", theItemLeft);
            $popup.show();
            if (isVisualSearch) {
                centerCartPopup()
            }
        }
        isWorking = false;
        divobj.style.cursor = "pointer";
        setFocus("emailaddress");
        setFocus("password")
    }
}
function setFocus(a) {
    var b = document.getElementById(a);
    if (b) {
        b.focus()
    }
}
function findElementPos(c) {
    var a = 0;
    var b = 0;
    if (c.offsetLeft == 0 || c.offsetTop == 0) {
        if (c.offsetParent) {
            a = c.offsetLeft;
            b = c.offsetTop;
            while (c = c.offsetParent) {
                a += c.offsetLeft;
                b += c.offsetTop
            }
        }
    } else {
        a = c.offsetLeft;
        b = c.offsetTop
    }
    return [a, b]
}
var XMLHttpFactories = [function () {
    return new XMLHttpRequest()
}, function () {
    return new ActiveXObject("Msxml2.XMLHTTP")
}, function () {
    return new ActiveXObject("Msxml3.XMLHTTP")
}, function () {
    return new ActiveXObject("Microsoft.XMLHTTP")
}];
function Browser() {
    var c, b, a;
    this.isIE = false;
    this.isNS = false;
    this.version = null;
    c = navigator.userAgent;
    b = "MSIE";
    if ((a = c.indexOf(b)) >= 0) {
        this.isIE = true;
        this.version = parseFloat(c.substr(a + b.length));
        return
    }
    b = "http://www.romvek.ru/shop_images/id--b1824/Netscape6/";
    if ((a = c.indexOf(b)) >= 0) {
        this.isNS = true;
        this.version = parseFloat(c.substr(a + b.length));
        return
    }
    b = "Gecko";
    if ((a = c.indexOf(b)) >= 0) {
        this.isNS = true;
        this.version = 6.1;
        return
    }
}
var browser = new Browser();
var dragObj = new Object();
dragObj.zIndex = 0;
var x, y;
function dragStart(b, c) {
    var a;
    if (c) {
        dragObj.elNode = document.getElementById(c)
    } else {
        if (browser.isIE) {
            dragObj.elNode = window.event.srcElement
        }
        if (browser.isNS) {
            dragObj.elNode = b.target
        }
        if (dragObj.elNode.nodeType == 3) {
            dragObj.elNode = dragObj.elNode.parentNode
        }
    }
    findMousePos(b);
    dragObj.cursorStartX = x;
    dragObj.cursorStartY = y;
    dragObj.elStartLeft = parseInt(dragObj.elNode.style.left, 10);
    dragObj.elStartTop = parseInt(dragObj.elNode.style.top, 10);
    if (isNaN(dragObj.elStartLeft)) {
        dragObj.elStartLeft = 0
    }
    if (isNaN(dragObj.elStartTop)) {
        dragObj.elStartTop = 0
    }
    dragObj.zIndex = 100;
    dragObj.elNode.style.zIndex = ++dragObj.zIndex;
    if (browser.isIE) {
        document.attachEvent("onmousemove", dragGo);
        document.attachEvent("onmouseup", dragStop);
        window.event.cancelBubble = true;
        window.event.returnValue = false
    }
    if (browser.isNS) {
        document.addEventListener("mousemove", dragGo, true);
        document.addEventListener("mouseup", dragStop, true);
        b.preventDefault()
    }
}
function dragGo(a) {
    findMousePos(a);
    dragObj.elNode.style.left = (dragObj.elStartLeft + x - dragObj.cursorStartX) + "px";
    dragObj.elNode.style.top = (dragObj.elStartTop + y - dragObj.cursorStartY) + "px";
    if (browser.isIE) {
        window.event.cancelBubble = true;
        window.event.returnValue = false
    }
    if (browser.isNS) {
        a.preventDefault()
    }
}
function dragStop(a) {
    dragObj.elNode = null;
    if (browser.isIE) {
        document.detachEvent("onmousemove", dragGo);
        document.detachEvent("onmouseup", dragStop)
    }
    if (browser.isNS) {
        document.removeEventListener("mousemove", dragGo, true);
        document.removeEventListener("mouseup", dragStop, true)
    }
}
function findMousePos(a) {
    if (browser.isIE) {
        x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
        y = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop
    }
    if (browser.isNS) {
        x = a.clientX + window.scrollX;
        y = a.clientY + window.scrollY
    }
};
var vsDataType = {similarcolors: 0, similarimages: 1, sameartist: 2, samesubject: 3, cloudTag: 4, recentlyviewed: 5};
var vsTagType = {light: 0, medium: 1, heavy: 2, superHeavy: 3};
var BackgroundImagesForRencetlyViewed = new Array("http://cache1.artprintimages.com/images/VisualSearch/STEP2Image01.png", "http://cache1.artprintimages.com/images/VisualSearch/STEP2Image02.png", "http://cache1.artprintimages.com/images/VisualSearch/STEP2Image03.png", "http://cache1.artprintimages.com/images/VisualSearch/STEP2Image04.png", "http://cache1.artprintimages.com/images/VisualSearch/STEP2Image05.png", "http://cache1.artprintimages.com/images/VisualSearch/STEP2Image06.png", "http://cache1.artprintimages.com/images/VisualSearch/STEP2Image07.png", "http://cache1.artprintimages.com/images/VisualSearch/STEP2Image08.png");
var vsSearchResult;
var vsRecentlyViewedResult;
var selectedItemToPass = "";
var SessionInfo;
var RecordThumbDetails;
var tagWithCategoryID = "";
var vsHoverSearchClicked = false;
var $vsHoverPopup;
var $vsTagCloud, $vsTagCloudDetail;
var gTagID;
var wt_Enabled = "false";
var vsData = {
    directionLeft_top: 100,
    directionTop_left: 122,
    directionTop_width: 160,
    directionTop_height: 60,
    directionLeft_width: 60,
    directionLeft_height: 196,
    popup_height: 454,
    popup_width: 436,
    image_size: 400,
    popupPadding: 5,
    noArtist: false
};
var cookie = new com.art.core.cookie.Cookie();
function SetGlobalSessionInfo(a) {
    var b = a.find("#hiddenValues").metadata({type: "attr", name: "data"});
    SessionInfo = new Object();
    SessionInfo.CountryCode = b.CountryCode;
    SessionInfo.CurrencyCode = b.CurrencyCode;
    SessionInfo.CurrencyID = b.CurrencyId;
    SessionInfo.CustomerZoneID = b.CustomerZoneId;
    SessionInfo.LanguageID = b.LanguageId;
    SessionInfo.RecordsPerPage = "20";
    SessionInfo.PageNo = "1";
    SessionInfo.SortBy = "P_SiteRank";
    SessionInfo.RequestMode = "Subjects";
    SessionInfo.PopupMode = "Initial";
    SessionInfo.VisualSearchUrl = b.SearchServerUrl
}
function SetRecordThumbDetails(a) {
    var b = a.find("#hiddenValues").metadata({type: "attr", name: "data"});
    RecordThumbDetails = new Object();
    RecordThumbDetails.Title = b.Title;
    RecordThumbDetails.ImageUrl = b.ImageUrl;
    RecordThumbDetails.ArtistName = b.ArtistName;
    RecordThumbDetails.Price = b.Price;
    RecordThumbDetails.LargeImageWidth = b.ImageLargeWidth;
    RecordThumbDetails.LargeImageHeight = b.ImageLargeHeight;
    RecordThumbDetails.Position = b.ItemId;
    RecordThumbDetails.ImageID = b.ImageId;
    RecordThumbDetails.ArtistCategoryID = b.ArtistCategoryId;
    RecordThumbDetails.CategoryID = b.CategoryId;
    RecordThumbDetails.ProductPageUrl = b.ProductPageUrl;
    RecordThumbDetails.MasterProductID = b.MasterProductId;
    RecordThumbDetails.MasterSpecID = b.MasterSpecId;
    RecordThumbDetails.Apnum = b.MasterApnum;
    RecordThumbDetails.OtherSizes = b.OtherSizes;
    RecordThumbDetails.ImageMedWidth = b.ImageMedWidth;
    RecordThumbDetails.ImageMedHeight = b.ImageMedHeight;
    RecordThumbDetails.ArtistUrl = b.ArtistUrl;
    RecordThumbDetails.ShowMarkDown = b.ShowMarkDown;
    RecordThumbDetails.MarkDownPrice = b.MarkDownPrice;
    RecordThumbDetails.ItemGroupTypeID = b.ItemGroupTypeID;
    RecordThumbDetails.ItemWidth = b.ItemWidth;
    RecordThumbDetails.ItemHeight = b.ItemHeight;
    RecordThumbDetails.ShipTime = b.ShipTime;
    RecordThumbDetails.Metric = b.Metric;
    RecordThumbDetails.PODConfigID = b.PODConfigID;
    RecordThumbDetails.MyGalItemDisplayedTypeId = b.MyGalItemDisplayedTypeId;
    RecordThumbDetails.MyGalItemPrice = b.MyGalItemPrice;
    RecordThumbDetails.MyGalImageUrl = b.MyGalImageUrl;
    RecordThumbDetails.ShowCanvas = b.ShowCanvas
}
function SifPop(b, a, f, g, e) {
    SetPos(b, a, -60);
    var d = $(b).parents(".galActions").children("#FilteredItemLeastPODConfig").text();
    var c = $(b).parents(".galActions").children("#FilteredItemLeastZProductID").text();
    if (d.length > 0) {
        PopupCallback("SeeItFramed", "&pid=" + f + "&spid=" + g + "&BuyingFunnel=" + e + "&PODConfig=" + d)
    } else {
        if (!(d.length > 0) && c.length > 0) {
            PopupCallback("SeeItFramed", "&pid=" + f + "&spid=" + g + "&BuyingFunnel=" + e + "&FZoneProductID=" + c)
        } else {
            PopupCallback("SeeItFramed", "&pid=" + f + "&spid=" + g + "&BuyingFunnel=" + e)
        }
    }
}
function CartPop(d, c, j, l, b, k, g, a, h) {
    SetPos(d, c, -60, h);
    var f = $(d).parents(".galActions").children("#FilteredItemLeastPODConfig").text();
    var e = $(d).parents(".galActions").children("#FilteredItemLeastZProductID").text();
    if (f.length > 0) {
        PopupCallback("AddToCart", "&pid=" + j + "&spid=" + l + "&cid=" + b + "&scid=" + escape(k) + "&BuyingFunnel=" + g + "&Apnum=" + a + "&PODConfig=" + f)
    } else {
        if (!(f.length > 0) && e.length > 0) {
            PopupCallback("AddToCart", "&pid=" + j + "&spid=" + l + "&cid=" + b + "&scid=" + escape(k) + "&BuyingFunnel=" + g + "&Apnum=" + a + "&FZoneProductID=" + e)
        } else {
            PopupCallback("AddToCart", "&pid=" + j + "&spid=" + l + "&cid=" + b + "&scid=" + escape(k) + "&BuyingFunnel=" + g + "&Apnum=" + a)
        }
    }
}
function SetPos(c, b, e, f) {
    isVisualSearch = false;
    this.divobj = c;
    c.style.cursor = "wait";
    var a = b % 4;
    var d = b - a;
    displayBoxPos = findElementPos(document.getElementById(d));
    displayBoxPos[0] = displayBoxPos[0] + 195 * a + e;
    displayBoxPos[1] = displayBoxPos[1] + 40;
    var g = parseInt(document.body.scrollTop) - parseInt(displayBoxPos[1]);
    if (g > 0) {
        displayBoxPos[1] = displayBoxPos[1] + 250
    }
    if (f != null) {
        isVisualSearch = true;
        displayBoxPos[0] = f.left + 260;
        displayBoxPos[1] = f.top - 70
    }
}
function ShowDiv(c, a) {
    if (browser.isIE) {
        var b = document.getElementById(c);
        if (b) {
            b.style.visibility = a
        }
    }
}
function getRadioButtonValue(a) {
    for (i = 0; i < a.length; i++) {
        if (a[i].checked) {
            return a[i].value
        }
    }
    return "not found"
}
function replaceSingleQuote(a) {
    return a.replace(/'/g, "@(singlequote)")
}
function removeSingleQuote(a) {
    return a.replace(/'/g, "")
}
function HighlightRow(b) {
    var c = document.getElementsByName("radiobutton");
    if (c) {
        for (var a = 0; a < c.length; a++) {
            c[a].parentNode.parentNode.className = "pop_text"
        }
    }
    b.parentNode.parentNode.className = "pop_bldtext"
}
function launchStudio(j, l, a, f, b, d, c, g) {
    var h = document.getElementsByName("radiobutton");
    if (h) {
        for (var e = 0; e < h.length; e++) {
            if (h[e].checked) {
                var k = (h[e].value).split(":");
                GotoStudio(k[0], f, b, d, c, k[1], j, l, a)
            }
        }
    }
}
function launchGalleryFramingStudio(k, m, b, g, c, e, d, h, a) {
    var j = document.getElementsByName("radiobutton");
    if (j) {
        for (var f = 0; f < j.length; f++) {
            if (j[f].checked) {
                var l = (j[f].value).split(":");
                if (a.toLowerCase() == "true") {
                    GotoStudioInline(l[0], g, c, e, d, l[1], k, m, b, l[2], l[3])
                } else {
                    GotoStudio(l[0], g, c, e, d, l[1], k, m, b)
                }
            }
        }
    }
}
function launchCart(h, p, c, g, b, a, m) {
    var f = document.getElementsByName("radiobutton");
    var n = $("#wtEnabledDiv").text();
    var j;
    var k;
    var e = com.art.core.utils.BrowserUtil.isiOSDevice();
    if (f) {
        for (var d = 0; d < f.length; d++) {
            if (f[d].checked) {
                var l = (f[d].value).split(":");
                if (n == "true") {
                    var o = l[0] + l[1]
                }
                if (b.length > 0 && l.length > 3 && !(e)) {
                    if (b.toLowerCase() == "true" && l[3].toLowerCase() == "true") {
                        j = "/asp/include/addtocart.asp/_/PDTA--" + l[0] + "/SP--" + l[1] + "/ID--" + c + "/posters.htm?Add_to_Cart=Y&XHN=1" + g + "&ui=" + h + "&PODConfigID=" + l[2];
                        k = "/framestep/?Add_To_Cart=Y&XHN=1" + g + "&ApNum=" + l[4] + "&PODConfigID=" + l[2] + "&pd=" + l[0] + "&sp=" + l[1];
                        "javascript:" + makeAjaxCallToAddToCart(j, k)
                    } else {
                        GotoCart(l[0], l[1], h, p, c, g, l[2])
                    }
                } else {
                    GotoCart(l[0], l[1], h, p, c, g, l[2])
                }
            }
        }
    }
}
function GotoCart(c, g, e, b, a, f, d) {
    window.location = "/asp/place_order-asp/_/PDTA--" + c + "/SP--" + g + "/ID--" + a + "/posters.htm?Add_to_Cart=Y&XHN=1" + f + "&ui=" + e + "&PODConfigID=" + d
}
var ImageUrlForPickitUp = "";
var ApnumForVisualSearch = "";
var titlePrefix = "More items like ";
var recordThumbHtlml = "";
var popupStatus = "";
var popupHovered = false;
var initialPopUp = false;
function RefreshRecordThumbDetailsPanel(e) {
    var c = e.ZoneProductID;
    var b = c.substring(c.length - 1);
    var a = c.substring(0, c.length - 1);
    RecordThumbDetails.MasterProductID = a;
    RecordThumbDetails.MasterSpecID = b;
    RecordThumbDetails.Apnum = e.APNum;
    RecordThumbDetails.OtherSizes = e.NumberOfSizes;
    $("#ImageDetContainer").html("");
    var d = "";
    d = GetRecordDetailsHtml(e, false);
    $("#ImageDetContainer").html(d)
}
function RefreshResultsGridPanel(a) {
    $(".pageparent").remove();
    var f = GetTotalRecordCount(a);
    var e = Math.ceil(f / 20);
    var d = "";
    var c;
    for (var b = 0; b < Math.min(a.ImageDetails.length, 20); b++) {
        c = a.ImageDetails[b];
        d += '<div id="dlstImages' + b + '" class="clstImages" dataIndex="' + b + '"onmouseover="fnMouseOverRV(this);" onmouseout="fnMouseOutRV(this);" ><logo id="eachimg" src="' + c.UrlInfo.CroppedSquareImageUrl + '" title="' + c.Title + '" alt="' + c.Title + '"/><div id="dProductPageUrl" class="cProductPageUrl">' + c.UrlInfo.ProductPageUrl + "|" + c.Title + "|" + c.UrlInfo.ImageUrl + "|" + c.ImageId + "|" + c.Artist.ArtistCategoryId + "</div></div>"
    }
    if ($.browser.msie) {
        d += '<div style="clear:both;height:4px;"></div>'
    }
    d += GetPaginationHtml(SessionInfo.PageNo, e);
    if (d != "") {
        $("#dpopupImg").append(d)
    }
    $("#dpopupImg").css("opacity", "1");
    $("#dpopupImg").css("left", "0");
    $("#dpopupImg").fadeIn("normal")
}
function MakeAjaxCallToVisualSearchForDetailedPage(c, d, e, a) {
    var f = "";
    if (c == vsDataType.similarcolors) {
        SessionInfo.RequestMode = c;
        SetTitle(d.Title, c);
        f = GetServerUrl(c, d.APNum)
    } else {
        if (c == vsDataType.similarimages) {
            SessionInfo.RequestMode = c;
            SetTitle(d.Title, c);
            f = GetServerUrl(c, d.APNum)
        } else {
            if (c == vsDataType.sameartist) {
                SessionInfo.RequestMode = c;
                var b = d.Artist.FirstName + d.Artist.LastName;
                SetTitle(b, c);
                f = GetServerUrl(c, "")
            } else {
                if (c == vsDataType.cloudTag) {
                    SessionInfo.RequestMode = vsDataType.cloudTag;
                    f = GetServerUrl(c, gTagID)
                } else {
                    f = GetServerUrl(c, "")
                }
            }
        }
    }
    $.ajax({
        url: f,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
        },
        success: function (g) {
            if (!CheckErrorsOrNoResults(g)) {
                SetCookieForRecentlyViewed(RecordThumbDetails.ImageID);
                vsSearchResult = g.ImageDetails;
                $("#bottomtitle").hide();
                $("#dback").hide();
                $("#highResImage").fadeOut("normal", function () {
                    $("#dpopupImg").empty();
                    var h = GetSubjectsTitle(g);
                    if (h.length > 1 && c == vsDataType.cloudTag) {
                        SetTitle(h, c)
                    }
                    RefreshRecordThumbDetailsPanel(d);
                    RefreshResultsGridPanel(g)
                });
                MakeAjaxCallToShowRecentlyViewedImages()
            } else {
                busyMode(a, c, true, false, true);
                vsHoverSearchClicked = false
            }
        },
        error: function (h, g) {
        }
    })
}
function MakeAjaxCallToVisualSearchService(a, b) {
    var c = "";
    var d = "";
    if (b == vsDataType.similarcolors) {
        d = GetServerUrl(b, ApnumForVisualSearch);
        SetTitle(RecordThumbDetails.Title, b);
        SessionInfo.RequestMode = b
    } else {
        if (b == vsDataType.similarimages) {
            d = GetServerUrl(b, ApnumForVisualSearch);
            SetTitle(RecordThumbDetails.Title, b);
            SessionInfo.RequestMode = b
        } else {
            if (b == vsDataType.sameartist) {
                SetTitle(RecordThumbDetails.ArtistName, b);
                SessionInfo.RequestMode = b;
                d = GetServerUrl(b, "")
            } else {
                if (b == vsDataType.cloudTag) {
                    SessionInfo.RequestMode = vsDataType.cloudTag;
                    d = GetServerUrl(b, tagWithCategoryID)
                }
            }
        }
    }
    $.ajax({
        url: d,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
        },
        success: function (e) {
            vsSearchResult = e.ImageDetails;
            if (!CheckErrorsOrNoResults(e)) {
                var g = GetSubjectsTitle(e);
                c = GetTotalRecordCount(e);
                if (g.length > 1 && b == vsDataType.cloudTag) {
                    SetTitle(g, b)
                }
                var f = callBackSeeMoreResults(e, c);
                if (a.length) {
                    if (f.length > 0) {
                        GreyoutBackground();
                        PositionSeeMorePopUp(a.offset(), f, "ResultsWindow");
                        MakeAjaxCallToShowRecentlyViewedImages();
                        $(".cSeemoreLikeItInnerImg").fadeIn(600)
                    }
                } else {
                    PositionSeeMorePopUp("", f, "DetailedWindow")
                }
            } else {
                busyMode(a, b, false, false, true);
                vsHoverSearchClicked = false
            }
        },
        error: function (f, e) {
        }
    })
}
function MakeAjaxCallToShowRecentlyViewedImages() {
    var a = GetServerUrl(vsDataType.recentlyviewed, GetImageIDsFromCookie());
    $.ajax({
        url: a,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
        },
        success: function (b) {
            if (!CheckErrorsOrNoResults(b)) {
                vsRecentlyViewedResult = b.ImageDetails;
                SetRecentlyViewedImagesBlock(b)
            }
        },
        error: function (c, b) {
        }
    })
}
var jsonobjSeeMore = "";
var imageTitle = "";
var parentDiv = "";
var recordThumbPosition = "";
function loadPopup() {
    if ($(this).parent()[0].className == "canvaswrap3") {
        parentDiv = $(this).parent().parent().parent().parent().parent().parent()
    } else {
        parentDiv = $(this).parent().parent()
    }
    NewVisualSearchPopup($(parentDiv))
}
function NewVisualSearchPopup(a) {
    SetRecordThumbDetails(a);
    SetGlobalSessionInfo(a);
    imageTitle = RecordThumbDetails.Title;
    $("#vsNewTitle").text(imageTitle);
    if (a.find(".pLnk").length > 0) {
        $("#vsArtist").show();
        $("#vsBy").show();
        $("#vsArtist").text(RecordThumbDetails.ArtistName);
        vsData.noArtist = false
    } else {
        $("#vsArtist").hide();
        $("#vsBy").hide();
        vsData.noArtist = true
    }
    if (RecordThumbDetails.OtherSizes > 1) {
        var f = "(" + RecordThumbDetails.OtherSizes + " sizes available)";
        $("#vsNewSize").text(f)
    } else {
        $("#vsNewSize").text(RecordThumbDetails.ItemWidth + " x " + RecordThumbDetails.ItemHeight + " " + RecordThumbDetails.Metric)
    }
    $("#vsNewPrice").text(RecordThumbDetails.Price);
    ApnumForVisualSearch = RecordThumbDetails.Apnum;
    ImageUrlForPickitUp = RecordThumbDetails.ImageUrl;
    recordThumbPosition = RecordThumbDetails.Position;
    var d = RecordThumbDetails.LargeImageWidth;
    var c = RecordThumbDetails.LargeImageHeight;
    var g = "";
    ImageUrlForPickitUp = ImageUrlForPickitUp.replace("MED", "LRG");
    var e = ImageUrlForPickitUp.indexOf("MXW");
    if (e > 1) {
        ImageUrlForPickitUp = ImageUrlForPickitUp.replace(ImageUrlForPickitUp.substr(e, ImageUrlForPickitUp.indexOf("MXH") - e), "MXW:" + d + "+");
        e = ImageUrlForPickitUp.indexOf("MXH");
        ImageUrlForPickitUp = ImageUrlForPickitUp.replace(ImageUrlForPickitUp.substr(e, ImageUrlForPickitUp.indexOf("QLT") - e), "MXH:" + c + "+")
    }
    var b = $("#popImage")
}
function unloadPopup() {
    disablePopup(false)
}
$(document).ready(function () {
    $(".createStreamGalleries").click(function () {
        var obj = $(".hideDiv").attr("data");
        var catObject = eval("(" + obj + ")");
        var categoryIds = catObject.CategoryId.split("-");
        var stream = {};
        stream.CategoryID = categoryIds;
        if (!isMygalleriesMainLoaded) {
            $.getScript("http://www.romvek.ru/js/mygalleriesmain.js", function () {
                isMygalleriesMainLoaded = true;
                MyGalleriesCore.registerModule(new com.art.myGalleries.modules.VerticalNavigationModule({}, MyGalleriesCore));
                MyGalleriesCore.getModel().setStreamObject(stream);
                MyGalleriesCore.sendNotification(new com.art.core.utils.Note(MyGalleriesCore.events.SHOW_CREATE_GALLERY_MODAL))
            })
        }
        return false
    })
});
$(document).ready(function () {
    wt_Enabled = $("#webtrendsEnabled").text();
    wt_Enabled = wt_Enabled.toLowerCase();
    $vsTagCloud = $("#tagCloud").hide();
    $(".centerImage").hoverIntent({over: loadPopup, timeout: 500, interval: 300, out: unloadPopup});
    $(document).on("click", ".direction", function () {
        vsHoverSearchClicked = true;
        var b;
        if (this.id == "directionRight") {
            b = vsDataType.samesubject;
            vsHoverSearchClicked = false;
            trackEventsForGA("HoverClickSubjectsArrow")
        } else {
            if (this.id == "directionTop") {
                b = vsDataType.similarimages;
                trackEventsForGA("HoverClickSimilarArrow")
            } else {
                if (this.id == "directionLeft") {
                    b = vsDataType.similarcolors;
                    trackEventsForGA("HoverClickColorsArrow")
                } else {
                    b = vsDataType.sameartist;
                    trackEventsForGA("HoverClickArtistArrow")
                }
            }
        }
        busyMode($(this), b, false, false, false);
        if (b == vsDataType.samesubject) {
            trackEventsForGA("HoverClickTagClaud");
            clickProcess4TagCloud($(this))
        } else {
            clickProcess($(this), b)
        }
    });
    $(document).on("click", "#tagCloud a", function () {
        busyMode($("#directionRight"), vsDataType.samesubject, false, false, false);
        vsHoverSearchClicked = true;
        tagWithCategoryID = $(this).attr("tagid");
        clickProcess($(this), vsDataType.cloudTag);
        trackEventsForGA("HoverClickTagID")
    });
    $(document).on("click", "#tagCloudDetail a", function () {
        busyMode($("#directionDetailsRight"), vsDataType.samesubject, false, false, false);
        vsHoverSearchClicked = true;
        var b = $(this).attr("tagid");
        gTagID = b;
        MakeAjaxCallToVisualSearchForDetailedPage(vsDataType.cloudTag, selectedItemToPass, b);
        trackEventsForGA("ResultsClickTagID")
    });
    $(document).on("click", ".iconcart", function () {
        var b = "VSControlATC";
        showVisualSearch = $("#dShowVisualSearch").text();
        if (showVisualSearch.toLowerCase() == "false") {
            trackEventsForGA(b)
        } else {
            if (showVisualSearch.toLowerCase() == "true") {
                trackEventsForGA("ResultClickAddtoCart")
            }
        }
    });
    $(document).on("click", ".thmbd", function () {
        var b = "VSControlProductclick";
        showVisualSearch = $("#dShowVisualSearch").text();
        if (showVisualSearch.toLowerCase() == "false") {
            trackEventsForGA(b)
        }
    });
    $(document).on("click", ".directionDetails", function () {
        var b;
        SessionInfo.PopupMode = "Det";
        SessionInfo.PageNo = "1";
        if (this.id == "directionDetailsRight") {
            b = vsDataType.samesubject;
            trackEventsForGA("ResultsClickSubjectsArrow")
        } else {
            if (this.id == "directionDetailsTop") {
                b = vsDataType.similarimages;
                trackEventsForGA("ResultsClickSimilarArrow")
            } else {
                if (this.id == "directionDetailsLeft") {
                    b = vsDataType.similarcolors;
                    trackEventsForGA("ResultsClickColorsArrow")
                } else {
                    b = vsDataType.sameartist;
                    trackEventsForGA("ResultsClickArtistArrow")
                }
            }
        }
        busyMode($(this), b, true, false, false);
        var c = RecordThumbDetails.ImageUrl;
        c = c.replace("LRG", "MED");
        if (b == vsDataType.samesubject) {
            clickProcess4TagCloud($(this));
            trackEventsForGA("ResultsClickTagClaud")
        } else {
            MakeAjaxCallToVisualSearchForDetailedPage(b, selectedItemToPass, "", $(this))
        }
    });
    $(document).on("click", "#dclose", function () {
        closeVSPopup();
        trackEventsForGA("ResultsClickCloseButton")
    });
    $(document).on("click", "#dvsdAddtoCart", function () {
        var h = $("#dvsdAddtoCart").offset();
        var f = RecordThumbDetails.MasterProductID;
        var g = RecordThumbDetails.MasterSpecID;
        var b = RecordThumbDetails.Apnum;
        var d = "&iSalesCategoryId=0&dimVals=" + RecordThumbDetails.CategoryID;
        if (RecordThumbDetails.OtherSizes > 1) {
            CartPop(this, RecordThumbDetails.Position, f, g, "0", d, "true", b, h)
        } else {
            var j = getCookie("CustSessionID");
            var c = "/asp/include/addtocart.asp/_/PDTA--" + f + "/SP--" + g + "/ID--0/posters.htm?Add_to_Cart=Y&XHN=1&iSalesCategoryId=0&ui=" + j + "&dimVals=" + RecordThumbDetails.CategoryID;
            var e = "/framestep/?Add_To_Cart=Y&ApNum=" + b + "&dimVals=" + RecordThumbDetails.CategoryID + "&iSalesCategoryId=0&PODConfigID=&pd=" + f + "&sp=" + g;
            makeAjaxCallToAddToCart(c, e)
        }
        trackEventsForGA("ResultClickAddtoCart")
    });
    $(document).keypress(function (b) {
        if (b.keyCode == 27 && popupStatus == 1) {
            disablePopup(true)
        }
    });
    $(document).on("click", "#dproductDetails", function () {
        var b = $(this).find(".cproductPageUrl").text();
        location.href = b + "?sorig=cat&sorigid=0&dimvals=" + RecordThumbDetails.CategoryID + "&ui=" + getCookie("CustSessionID");
        trackEventsForGA("ResultClickImageLeft")
    });
    $(document).on("click", ".cproductDtails", function () {
        var b = $(this).find("#hiddenProductUrl").text();
        location.href = b + "?sorig=cat&sorigid=0&dimvals=" + RecordThumbDetails.CategoryID + "&ui=" + getCookie("CustSessionID");
        trackEventsForGA("ResultClickProductDetails")
    });
    $(document).on("click", "#wrapper,#fullDetailsInfoGallery, #vsMoreProductLink", function () {
        var b = RecordThumbDetails.ProductPageUrl;
        location.href = b;
        trackEventsForGA("HoverImageClick")
    });
    $(document).on("click", "#fullDetailsAddToCartGallery", function () {
        var h = $("#fullDetailsAddToCartGallery").offset();
        var f = RecordThumbDetails.MasterProductID;
        var g = RecordThumbDetails.MasterSpecID;
        var b = RecordThumbDetails.Apnum;
        var d = "&iSalesCategoryId=0&dimVals=" + RecordThumbDetails.CategoryID;
        if (RecordThumbDetails.OtherSizes > 1) {
            CartPop(this, RecordThumbDetails.Position, f, g, "0", d, "true", b, h)
        } else {
            var j = getCookie("CustSessionID");
            var c = "/asp/include/addtocart.asp/_/PDTA--" + f + "/SP--" + g + "/ID--0/posters.htm?Add_to_Cart=Y&XHN=1&iSalesCategoryId=0&ui=" + j + "&dimVals=" + RecordThumbDetails.CategoryID;
            var e = "/framestep/?Add_To_Cart=Y&ApNum=" + b + "&dimVals=" + RecordThumbDetails.CategoryID + "&iSalesCategoryId=0&PODConfigID=&pd=" + f + "&sp=" + g;
            makeAjaxCallToAddToCart(c, e)
        }
        trackEventsForGA("ResultClickAddtoCart");
        popupHovered = false;
        disablePopup(false)
    });
    $(document).on("click", ".pop_x", function () {
        $("#popupplaceholder").hide()
    });
    $(document).on("click", "#lfProductPageBk", function () {
        var b = $(this).find("#lfProductPageBkUrl").text();
        b = b + "?sorig=cat&sorigid=0&dimvals=" + RecordThumbDetails.CategoryID + "&ui=" + getCookie("CustSessionID");
        location.href = b;
        trackEventsForGA("ResultClickImageLeft")
    });
    $("#vsfullDetailsAddSelectionGallery").mouseenter(function () {
        $(this).css("background-position", "0px -1387px")
    });
    $("#vsfullDetailsAddSelectionGallery").mouseleave(function () {
        $(this).css("background-position", "0px -1362px")
    });
    $("#vsfullDetailsSaveToGallery").mouseenter(function () {
        $(this).css("background-position", "-66px -1387px")
    });
    $("#vsfullDetailsSaveToGallery").mouseleave(function () {
        $(this).css("background-position", "-66px -1362px")
    });
    $("#fullDetailsAddToCartGallery").mouseenter(function () {
        $(this).css("background-position", "-132px -1387px")
    });
    $("#fullDetailsAddToCartGallery").mouseleave(function () {
        $(this).css("background-position", "-132px -1362px")
    });
    $("#fullDetailsInfoGallery").mouseenter(function () {
        $(this).css("background-position", "-27px -27px")
    });
    $("#fullDetailsInfoGallery").mouseleave(function () {
        $(this).css("background-position", "0px -27px")
    });
    $(document).on("click", "#vsfullDetailsAddSelectionGallery", function () {
        popupHovered = false;
        disablePopup(false);
        trackEventsForGA("VSPopupSimilarImages");
        if (!isVSALoaded) {
            $.getScript(VSjsFilepath, function () {
                a();
                isVSALoaded = true
            })
        } else {
            a()
        }
    });
    function a() {
        VisualSearchCore.init(visualSearchCoreEnvironment);
        var b = new com.art.core.vos.ImageVO();
        b.parseCatalogImageDetail(RecordThumbDetails.Apnum, RecordThumbDetails.Title, RecordThumbDetails.ImageID, RecordThumbDetails.ArtistCategoryID, RecordThumbDetails.ImageUrl, RecordThumbDetails.ImageUrl, RecordThumbDetails.Price, RecordThumbDetails.MarkDownPrice, RecordThumbDetails.ArtistName, RecordThumbDetails.MasterProductID, RecordThumbDetails.MasterSpecID, RecordThumbDetails.MasterProductID + RecordThumbDetails.MasterSpecID, RecordThumbDetails.LargeImageWidth, RecordThumbDetails.LargeImageHeight, RecordThumbDetails.ImageMedWidth, RecordThumbDetails.ImageMedHeight, RecordThumbDetails.ArtistUrl, RecordThumbDetails.ItemWidth, RecordThumbDetails.ItemHeight, RecordThumbDetails.ShipTime, RecordThumbDetails.ProductPageUrl);
        VisualSearchCore.registerModule(new _art_.visualSearch.modules.ModuleLightbox({target: "body"}, VisualSearchCore));
        VisualSearchCore.registerModule(new _art_.visualSearch.modules.ModuleCurrentSearch({
            target: "body",
            content: {}
        }, VisualSearchCore));
        VisualSearchCore.registerModule(new _art_.visualSearch.modules.ModuleSearchResults({
            target: ".VSResultsContentRight",
            content: {}
        }, VisualSearchCore));
        VisualSearchCore.startModule("ModuleLightbox");
        VisualSearchCore.sendNotification(new _art_.core.Note(VisualSearchCore.events.SEARCH_FROM_CATALOG, {pivotImage: b}, "vo"))
    }

    $(document).on("click", "#vsfullDetailsSaveToGallery", function () {
        var b = {};
        b.ImageUrl = RecordThumbDetails.MyGalImageUrl;
        b.Imageid = RecordThumbDetails.ImageID;
        b.ItemId = RecordThumbDetails.Position;
        b.Width = RecordThumbDetails.LargeImageWidth;
        b.Height = RecordThumbDetails.LargeImageHeight;
        b.ZoneProductID = RecordThumbDetails.MasterProductID;
        b.ItemPrice = RecordThumbDetails.MyGalItemPrice;
        b.Price = RecordThumbDetails.Price;
        b.ArtistName = RecordThumbDetails.ArtistName;
        if (RecordThumbDetails.ArtistCategoryID.length == 0) {
            b.ArtistId = 0
        } else {
            b.ArtistId = RecordThumbDetails.ArtistCategoryID
        }
        b.Title = RecordThumbDetails.Title;
        b.PODConfigID = RecordThumbDetails.PODConfigID;
        b.APNum = RecordThumbDetails.Apnum;
        if (RecordThumbDetails.OtherSizes > 1) {
            b.AvailableInOtherSizes = "True"
        } else {
            b.AvailableInOtherSizes = "False"
        }
        b.ItemDisplayTypeID = RecordThumbDetails.MyGalItemDisplayedTypeId;
        b.Source = "GalleryPage";
        MyGalleriesCore.getModel().setSelectedImageObject(b)
    });
    $(document).on("click", "#vsTitle", function () {
        location.href = RecordThumbDetails.ProductPageUrl
    });
    $(document).on("click", "#vsArtist", function () {
        location.href = RecordThumbDetails.ArtistUrl
    });
    $("#vsTitle,#vsArtist, #vsMoreProductLink").hover(function () {
        $(this).addClass("ImageTitleHoverStyle")
    }, function () {
        $(this).removeClass("ImageTitleHoverStyle")
    });
    $(window).scroll(function () {
        if (initialPopUp == false) {
            var b = ($(window).height() - $(".cSeemoreLikeItInnerImg").height()) / 2 + $(window).scrollTop();
            $(".cSeemoreLikeItInnerImg").stop(true).animate({top: b}, 350)
        }
    })
});
var ImageWidthFromService;
var ImageHeightFromService;
function UpdateHtmlforDetailedImageDirections(g) {
    var c = "";
    var d = "";
    var a = false;
    ImageWidthFromService = g.PhysicalDimensions.Width;
    ImageHeightFromService = g.PhysicalDimensions.Height;
    d = g.UrlInfo.ImageUrl;
    d = d.replace("MED", "LRG");
    imageSrc = d;
    var f = 390;
    var e = 280;
    var b;
    if (g.Artist.ArtistCategoryId > 0) {
        a = true;
        b = g.Artist.FirstName + g.Artist.LastName
    }
    c += '<div id="dproductDetails">';
    c += '<logo id="hiResImg" title="' + g.Title + '" alt="' + g.Title + '" src="' + d + '"/>';
    c += '<div id="dproductPageUrl" class="cproductPageUrl" style="display:none">' + g.UrlInfo.ProductPageUrl + "</div> ";
    c += "</div>";
    c += '<div class="cproductDtails"><span id="hiddenProductUrl" style="display:none">' + g.UrlInfo.ProductPageUrl + "</span>view product details </div>";
    c += '<div id="bottomInfo"><div id="eachImgtitle">' + com.art.core.utils.StringUtil.autoEllipseText(g.Title, 52) + "</div>";
    c += '<div id="bottomArtistLine">by <span id="bottom"/>' + b + "</div>";
    c += "</div>";
    c += '<div id="tagCloudDetail" />';
    c += '<div id="directionDetailsTop" class="directionDetails" ><a href="" onclick="return false;" /></div>';
    c += '<div id="directionDetailsRight" class="directionDetails" ><a href="" onclick="return false;" /></div>';
    c += '<div id="directionDetailsBottom" class="directionDetails" ><a href="" onclick="return false;" /></div>';
    c += '<div id="directionDetailsLeft" class="directionDetails"  ><a href="" onclick="return false;" /></div>';
    c += '<div id="busyTopDetail" class="vsBusyDetail"/>';
    c += '<div id="busyLeftDetail" class="vsBusyDetail"/>';
    c += '<div id="busyRightDetail" class="vsBusyDetail"/>';
    c += '<div id="busyBottomDetail" class="vsBusyDetail"/>';
    $("#highResImage").hide().html(c);
    if (!a) {
        $("#directionDetailsBottom").css("visibility", "hidden");
        $("#bottomArtistLine").css("visibility", "hidden")
    }
    $vsTagCloudDetail = $("#tagCloudDetail");
    $vsTagCloudDetail.hide()
}
function SetDetailedFourDirectionsWithHighResImage() {
    var b = $("#highResImage");
    var j = b.width();
    var g = b.height();
    var q = -10;
    $(".vsBusyDetail").hide();
    $("#bottomtitle").hide();
    $("#dpopupImg").hide();
    $(".pagenav").hide();
    $("#toptitle").hide();
    $("#bottomtitle").hide();
    var c = $("#hiResImg");
    var k = ImageHeightFromService;
    var r = ImageWidthFromService;
    var o = 390;
    var n = 280;
    var p, l;
    if ((k / r) < (n / o)) {
        c.css("height", "");
        c.css("width", o);
        n = k / r * o
    } else {
        c.css("height", n);
        o = r / k * n;
        c.css("width", "")
    }
    p = ((g - n) / 2) + q;
    l = (j - o) / 2;
    c.css("top", p);
    c.css("left", l);
    var m = 5;
    var f = $("#directionDetailsTop");
    var a = $("#directionDetailsBottom");
    var d = $("#directionDetailsLeft");
    var e = $("#directionDetailsRight");
    p = ((g - n) / 2) - vsData.directionTop_height - m;
    l = (j - vsData.directionTop_width) / 2;
    f.css({top: p + q, left: l - 1});
    a.css({bottom: p - q, left: l});
    p = ((g - vsData.directionLeft_height) / 2) + q;
    l = ((j - o) / 2) - vsData.directionLeft_width - m;
    if ($.browser.msie) {
        d.css({top: p + 10, left: l});
        e.css({top: p + 10, right: l})
    } else {
        d.css({top: p, left: l});
        e.css({top: p, right: l})
    }
    $("#dback").fadeIn("slow");
    $("#highResImage").fadeIn("slow")
}
function busyMode(a, d, e, g, f) {
    var c = "vsBusy";
    var k = "direction";
    var j;
    var h = 0;
    var b = $("#cSeemoreLikeIt").css("display") == "block" ? $("#cSeemoreLikeIt") : $("#highResImage");
    if (d == vsDataType.similarcolors) {
        j = "Left"
    } else {
        if (d == vsDataType.similarimages) {
            j = "Top";
            h = (b.width() - 16) / 2
        } else {
            if (d == vsDataType.sameartist) {
                j = "Bottom";
                h = (b.width() - 16) / 2
            } else {
                j = "Right"
            }
        }
    }
    if (e) {
        j += "Detail";
        c += "Detail";
        k += "Details"
    }
    if (f) {
        a.attr("id", a.attr("id") + "NoResult").show().css("visibility", "").removeClass(k);
        $("#busy" + j).hide().css("visibility", "hidden");
        if (d == vsDataType.sameartist) {
            a.css("visibility", "visible")
        }
    } else {
        if (g) {
            $("#busy" + j).hide().css("visibility", "hidden");
            if (d == vsDataType.sameartist) {
                a.css("visibility", "visible")
            }
            a.show()
        } else {
            if (d == vsDataType.sameartist) {
                a.css("visibility", "hidden")
            } else {
                a.hide()
            }
            if (j === "Bottom" || j === "Top") {
                h = h - 7
            }
            if (j === "TopDetail") {
                h = h - 13
            }
            $("#busy" + j).css("margin-left", h);
            $("#busy" + j).show().css("visibility", "")
        }
    }
}
function clickProcess(a, b) {
    SetCookieForRecentlyViewed(RecordThumbDetails.ImageID);
    initialPopUp = false;
    recordThumbHtlml = "";
    recordThumbHtlml += GetRecordDetailsHtml(RecordThumbDetails, true);
    recordThumbHtlml += AddToCart();
    recordThumbHtlml += '<div id="recentlyViewedMainBlock" class="recentlyViewedMainBlock"></div>';
    recordThumbHtlml += '<div class="clearHeight"></div>';
    recordThumbHtlml += FeedBack();
    MakeAjaxCallToVisualSearchService(a, b)
}
function GetRecordDetailsHtml(h, e) {
    var p = "";
    var s, g, o, j, r, b, a, m, q, l, n = "";
    var k = false;
    if (e) {
        s = h.ImageMedWidth;
        g = h.ImageMedHeight;
        o = h.ProductPageUrl;
        j = h.ImageUrl;
        r = h.Title;
        b = h.ArtistUrl;
        if (h.ArtistName != "") {
            a = h.ArtistName;
            k = true
        }
        m = h.Price;
        q = RecordThumbDetails.ShowMarkDown;
        l = RecordThumbDetails.MarkDownPrice;
        if (q.toLowerCase() == "true") {
            q = true
        } else {
            q = false
        }
    } else {
        s = h.ImageDimensions[0].PixelWidth;
        g = h.ImageDimensions[0].PixelHeight;
        o = h.UrlInfo.ProductPageUrl;
        j = h.UrlInfo.ImageUrl;
        r = h.Title;
        b = h.Artist.ArtistUrl;
        b = "http://" + window.location.hostname + b + "?ui=" + getCookie("CustSessionID");
        if (h.Artist.ArtistCategoryId > 0) {
            a = h.Artist.FirstName + " " + h.Artist.LastName;
            k = true
        }
        m = h.ItemPrice.DisplayPrice;
        q = h.ItemPrice.ShowMarkDownPrice;
        l = h.ItemPrice.MarkDownPrice
    }
    if (q) {
        n = "galleryPriceStrike"
    } else {
        n = "price"
    }
    var c = (176 - g) / 2;
    var d = (195 - s) / 2;
    c = c + 15;
    d = d + 10;
    p += '<div id="ImageDetContainer" style="width:200px;margin-top:5px;">';
    p += '<div style="height:176px;width:195px;margin-bottom:10px;">';
    p += '<div style="padding-left:' + d + ";padding-top:" + c + ';" class="lfProductPageBk" id="lfProductPageBk">';
    p += '<div id="lfProductPageBkUrl" class="lfProductPageBkUrl" style="display:none">' + o + "</div> ";
    p += '<div class="logo-shadow" style="margin-left:6px;"><logo id="eachimg" src="' + j + '" title="' + r + '" alt="' + r + '"width="' + s + '"height="' + g + '"/></div></div>';
    p += "</div>";
    p += '<div class="pttl pCntr">';
    p += '<div class="VsFontType">' + com.art.core.utils.StringUtil.autoEllipseText(r, 27) + "</div>";
    if (k) {
        var f = "trackEventsForGA('ResultsClickArtistLeft')";
        p += '<div class="VsFontTypeArtist" style="margin-top:7px;"><span>by </span><a href="' + b + '" title="' + a + '" class="pLnk" onclick="' + f + '">' + a + "</a></div>"
    }
    p += "</div>";
    p += '<div class="pDsp41 pCntr VsFontTypePrice" style="padding-top:12px;height:20px;padding-bottom:7px;"> <span class="' + n + '" style="font-size:12px;color:#4e4e4e;font-family:Vardana;">' + m + "</span>";
    if (q) {
        p += '<span class="gallerySalePrice">' + l + "</span>"
    }
    p += "</div>";
    p += "</div>";
    p += '<div class="clear"></div>';
    return p
}
function SetCookieForRecentlyViewed(d) {
    var c = "";
    var b = "";
    c = GetCookieDictionary("arts", "ImageidVisualSearch");
    if (c.length > 0) {
        var a = c.split("|");
        if (a.length == 8 && (!CheckIFImageidExistInCookie(d))) {
            a.unshift(d);
            a.pop();
            b = "";
            for (x in a) {
                b = b + "|" + a[x]
            }
            b = b.substring(1)
        } else {
            if (!CheckIFImageidExistInCookie(d)) {
                b = d + "|" + c
            }
        }
        if (b.length > 0) {
            cookie.setCookieSession("arts", "ImageidVisualSearch", b)
        }
    } else {
        if (!CheckIFImageidExistInCookie(d)) {
            cookie.setCookieSession("arts", "ImageidVisualSearch", d)
        }
    }
}
function GetImageIDsFromCookie() {
    var c = GetCookieDictionary("arts", "ImageidVisualSearch");
    var b = "";
    if (c.length > 0) {
        var a = c.split("|");
        b = "";
        for (x in a) {
            b = b + "," + a[x]
        }
    }
    b = b.substring(1);
    return b
}
function CheckIFImageidExistInCookie(c) {
    var b = "";
    b = GetCookieDictionary("arts", "ImageidVisualSearch");
    var a = b.split("|");
    if (a.length > 0) {
        for (x in a) {
            if (a[x] == c) {
                return true
            }
        }
    }
    return false
}
function SetRecentlyViewedImagesBlock(l) {
    var k = 8;
    var g = l.ImageDetails;
    var c = "";
    var b = "";
    if (g != "") {
        k = g.length
    }
    if (k > 8) {
        k = 8
    }
    var f = GetImageIDsFromCookie();
    var e = f.split(",");
    c += '<div class="cRecentlyViewedImages">';
    c += "</div>";
    c += '<div class="rViewedImgLst">';
    for (var h = 0; h < e.length; h++) {
        for (var d = 0; d < k; d++) {
            if (e[h] == g[d].ImageId) {
                b = g[d].UrlInfo.CroppedSquareImageUrl.replace("&maxw=100&maxh=100", "&maxw=42&maxh=42");
                c += '<div id="rViewedImg' + d + '" class="rViewedImg" dataIndex="' + d + '"  style="background-image:url(' + b + ');"></div>'
            }
        }
    }
    for (var a = k; a < 8; a++) {
        c += '<div  class="rViewedImg_numbers" style="background-image:url(' + BackgroundImagesForRencetlyViewed[a] + ');"></div>'
    }
    c += "</div>";
    c += '<div class="clearHeight"></div>';
    $("#recentlyViewedMainBlock").html(c);
    trackEventsForGA("LoadingRecentlyShowBreadCrumb")
}
function clickProcess4TagCloud(a) {
    var b = false;
    var c = GetServerUrl(vsDataType.samesubject, "");
    $.ajax({
        url: c,
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        beforeSend: function () {
        },
        success: function (d) {
            if (!CheckErrorsOrNoResults(d) && d.ImageDetails[0].Tags) {
                var g = "";
                var k = d.ImageDetails[0].Tags;
                var j;
                if (a.attr("id").indexOf("Detail") > 0) {
                    j = $vsTagCloudDetail;
                    b = true;
                    busyMode(a, vsDataType.samesubject, true, true, false)
                } else {
                    j = $vsTagCloud;
                    busyMode(a, vsDataType.sazmesubject, false, true, false)
                }
                j.empty();
                for (var f = 0; f < k.length; f++) {
                    g += '<a href="" onclick="return false;" tagid="' + k[f].TagId + '" class="weight' + (k[f].TagFontSize - 1) + '" > ' + k[f].TagName + "</a>"
                }
                var h = "";
                if (b) {
                    h = $("#cSeemoreLikeItInnerImg").height()
                } else {
                    h = $("#cSeemoreLikeIt").height()
                }
                j.append(g).show();
                var e = h - j.height();
                e = e / 2;
                if (b) {
                    e = e - 35
                } else {
                    e = e - 15
                }
                j.css("top", e)
            } else {
                if (a.attr("id").indexOf("Detail") > 0) {
                    busyMode(a, vsDataType.samesubject, true, true, true)
                } else {
                    busyMode(a, vsDataType.sazmesubject, false, true, true)
                }
                vsHoverSearchClicked = false
            }
        },
        error: function (e, d) {
        }
    })
}
function centerPopup() {
    var g = document.body.clientWidth;
    var f = document.body.clientHeight;
    var c = $("#cSeemoreLikeItInnerImg").height();
    var d = $("#cSeemoreLikeItInnerImg").width();
    if (!window.screen) {
        g = 800;
        f = 600
    }
    if (d > g) {
        d = (g) * (0.9)
    }
    if (c > f) {
        c = (f) * (0.9)
    }
    var b = document.getElementById("cSeemoreLikeItInnerImg");
    if (b != null) {
        if (b.offsetParent != null) {
            var a = (b.offsetParent.clientWidth / 2) - (b.clientWidth / 2) + b.offsetParent.scrollLeft;
            var e = (b.offsetParent.clientHeight / 2) - (b.clientHeight / 2) + b.offsetParent.scrollTop;
            $("#cSeemoreLikeItInnerImg").css({position: "absolute", top: e, left: a})
        }
    }
}
function centerCartPopup() {
    var g = document.body.clientWidth;
    var f = document.body.clientHeight;
    var c = $("#popupplaceholder").height();
    var d = $("#popupplaceholder").width();
    if (!window.screen) {
        g = 800;
        f = 600
    }
    if (d > g) {
        d = (g) * (0.9)
    }
    if (c > f) {
        c = (f) * (0.9)
    }
    var b = document.getElementById("popupplaceholder");
    if (b != null) {
        if (b.offsetParent != null) {
            var a = (b.offsetParent.clientWidth / 2) - (b.clientWidth / 2) + b.offsetParent.scrollLeft;
            var e = (b.offsetParent.clientHeight / 2) - (b.clientHeight / 2) + b.offsetParent.scrollTop;
            $("#popupplaceholder").css({position: "absolute", top: e, left: a})
        }
    }
}
function GreyoutBackground() {
    if ($.browser.msie) {
        var a = Math.max($(document).height(), $(window).height(), document.documentElement.clientHeight);
        var b = Math.max($(document).width(), $(window).width(), document.documentElement.clientWidth);
        $("#VbackgroundPopup").css({height: a, width: b - 20})
    }
    $("#VbackgroundPopup").css({
        "background-color": "#000000",
        filter: "alpha (opacity=70)",
        filter: "progid:DXImageTransform.Microsoft.Alpha(style=0, opacity=70)",
        "-moz-opacity": "0.7",
        opacity: "0.7",
        "-khtml-opacity": ".7",
        zoom: "1"
    });
    $("#VbackgroundPopup").css({opacity: "0.6"});
    $("#VbackgroundPopup").show()
}
function disablePopup(a) {
    if (a || (popupHovered == false && !vsHoverSearchClicked)) {
        $("#cSeemoreLikeIt").fadeOut();
        popupStatus = 0;
        $vsTagCloud.hide()
    }
}
function closeVSPopup() {
    $(".cSeemoreLikeItInnerImg").fadeOut();
    $(".cSeemoreLikeItInnerImg").hide();
    $("#highResImage").hide();
    $("#VbackgroundPopup").fadeOut("slow");
    $("#cSeemoreLikeIt").fadeOut();
    $("#popupplaceholder").hide()
}
function AddToCart() {
    var a = "";
    a += '<div class="pfuButton btnAddtoCart"  id="dvsdAddtoCart" ><div class="primary_buttons btns_orange" style="width:15em" onmouseover="fnMouseOverVS(this);" onmouseout="fnMouseOutVS(this);"><div class="txtlabel">ADD TO MY CART</div></div></div>';
    return a
}
function GetSubjectsTitle(a) {
    var b = "";
    if (a.ResultSummary.SelectedDimensionValues.length) {
        b = a.ResultSummary.SelectedDimensionValues[0].Name
    }
    return b
}
function GetTotalRecordCount(a) {
    var b = "";
    if (a.ResultSummary.TotalRecordCount > 20) {
        b = a.ResultSummary.TotalRecordCount
    }
    return b
}
function GetServerUrl(c, b) {
    var d = SessionInfo.VisualSearchUrl;
    var a = "";
    if (RecordThumbDetails.ItemGroupTypeID != "1" && SessionInfo.PopupMode == "Initial") {
        a = "ImageFilePath=" + RecordThumbDetails.ImageUrl
    } else {
        a = "Apnum=" + b
    }
    if (c == vsDataType.similarcolors) {
        d += "GetImagesByImageColor?method=?&" + a + "&RecordsPerPage=" + SessionInfo.RecordsPerPage + "&PageNumber=" + SessionInfo.PageNo + "&CustomerZoneId=" + SessionInfo.CustomerZoneID + "&CurrencyCode=" + SessionInfo.CurrencyCode
    } else {
        if (c == vsDataType.similarimages) {
            d += "GetSimilarImagesForImage?method=?&" + a + "&RecordsPerPage=" + SessionInfo.RecordsPerPage + "&PageNumber=" + SessionInfo.PageNo + "&CustomerZoneId=" + SessionInfo.CustomerZoneID + "&CurrencyCode=" + SessionInfo.CurrencyCode
        } else {
            if (c == vsDataType.sameartist) {
                d += "GetSearchResultInSimpleFormat?method=?&Refinements=&Search=&RecordsPerPage=" + SessionInfo.RecordsPerPage + "&PageNumber=" + SessionInfo.PageNo + "&CustomerZoneId=" + SessionInfo.CustomerZoneID + "&SortBy=" + SessionInfo.SortBy + "&ArtistCategoryId=" + RecordThumbDetails.ArtistCategoryID + "&APNumList=&ImageIdList=&CurrencyCode=" + SessionInfo.CurrencyCode
            } else {
                if (c == vsDataType.cloudTag) {
                    d += "GetSearchResultInSimpleFormat?method=?&Refinements=" + b + "&Search=&RecordsPerPage=" + SessionInfo.RecordsPerPage + "&PageNumber=" + SessionInfo.PageNo + "&CustomerZoneId=" + SessionInfo.CustomerZoneID + "&SortBy=" + SessionInfo.SortBy + "&ArtistCategoryId=0&APNumList=&ImageIdList=&CurrencyCode=" + SessionInfo.CurrencyCode
                } else {
                    if (c == vsDataType.recentlyviewed) {
                        d += "GetSearchResultInSimpleFormat?method=?&Refinements=&Search=&RecordsPerPage=" + SessionInfo.RecordsPerPage + "&PageNumber=1&CustomerZoneId=" + SessionInfo.CustomerZoneID + "&SortBy=" + SessionInfo.SortBy + "&ArtistCategoryId=0&APNumList=&ImageIdList=" + b + "&CurrencyCode=" + SessionInfo.CurrencyCode
                    } else {
                        d += "GetImageInformation?method=?&ImageId=" + RecordThumbDetails.ImageID + "&CustomerZoneId=" + SessionInfo.CustomerZoneID + "&CurrencyCode=" + SessionInfo.CurrencyCode
                    }
                }
            }
        }
    }
    return d
}
function CheckErrorsOrNoResults(a) {
    if (a.ResponseCode != "200") {
        return true
    }
    if (a.ResultSummary != "") {
        if (a.ResultSummary.TotalRecordCount < 1) {
            return true
        }
    }
    false
}
function trackEventsForGA(a) {
    var b = "'" + a + "'";
    siteWideGA.trackEventWithGroupCategory("VisualSearch", b, b)
}
function trackGAEvent(a, b) {
    var c = "'" + a + "'";
    var d = "'" + b + "'";
    siteWideGA.trackEventWithGroupCategory(d, c, c)
}
$(document).on("click", "#p2alink", function () {
    trackGAEvent("_trackPageview", "/p2a-cta/shop_images-page");
    setTimeout(function () {
        location.href = "/photostoart"
    }, 200);
    return false
});
$(document).on("click", "#popup-link", function () {
    trackGAEvent("_trackPageview", "/p2a-cta/popup-store");
    location.href = "http://www.romvek.ru/shop_images/id--b827333/art-com-store-posters.htm";
    return false
});
$(document).on("click", "div.divBtn > logo.icongal", function () {
    var imgObject = $(this).parent().find(".mygalrecInfo").text();
    var myObject = eval("(" + imgObject + ")");
    MyGalleriesCore.getModel().setSelectedImageObject(myObject);
    MyGalleriesCore.externalAddToGallery(MyGalleriesCore.constants.GALLERY_PAGE)
});
$(document).ready(function () {
    var a = $("#lastPageUrl").text();
    cookie.setCookieSession("arts", "lastPage", a)
});
var ImageCount;
var cookie = new com.art.core.cookie.Cookie();
$(document).ready(function () {
    ImageCount = $("#arItemCount").text();
    jQuery(".jcarousel-skin-ar").jcarousel({
        size: ImageCount,
        visible: 4,
        initCallback: carousel_callbackTest,
        scroll: 4
    });
    jQuery(".jcarousel-no-scroll").jcarousel({
        size: ImageCount,
        visible: 8,
        scroll: 8,
        buttonNextHTML: null,
        buttonPrevHTML: null
    })
});
function carousel_callbackTest(a, b) {
    $("#arBottomModule").touchwipe({
        wipeLeft: function () {
            a.next()
        }, wipeRight: function () {
            a.prev()
        }, wipeUp: function () {
            trace("up")
        }, wipeDown: function () {
            trace("down")
        }, min_move_x: 20, min_move_y: 20, preventDefaultEvents: true
    })
}
$(".jcarousel-skin-ar li .itemBox").bind("mouseenter", function () {
    $(this).css("cursor", "pointer")
});
$(".jcarousel-skin-ar li .itemBox").bind("mouseleave", function () {
    $(this).css("cursor", "hand")
});
$(".jcarousel-skin-ar li logo").bind("mouseenter", function () {
    $(this).css("cursor", "pointer")
});
$(".jcarousel-skin-ar li logo").bind("mouseleave", function () {
    $(this).css("cursor", "hand")
});
$(".arTop li .itemBoxSeeMore").bind("mouseenter", function () {
    $(this).css("cursor", "pointer")
});
$(".arTop li .itemBoxSeeMore").bind("mouseleave", function () {
    $(this).css("cursor", "hand")
});
$(".arTop li .itemBox").bind("mouseenter", function () {
    $(this).css("cursor", "pointer")
});
$(".arTop li .itemBox").bind("mouseleave", function () {
    $(this).css("cursor", "hand")
});
$(".arTop li logo").bind("mouseenter", function () {
    $(this).css("cursor", "pointer")
});
$(".arTop li logo").bind("mouseleave", function () {
    $(this).css("cursor", "hand")
});
$(document).on("click", ".infoIcon", function () {
    var b = $(this).offset();
    var a = $(".arModuleToolTip").height();
    var c = $(".arModuleToolTip").width();
    if ($(".arModuleToolTip").css("display") == "none") {
        showToolTip((b.top - (a + 62)), (b.left - (c / 2)))
    } else {
        if ($(".arModuleToolTip").css("display") == "block") {
            $(".arModuleToolTip").hide()
        }
    }
});
$(document).on("click", ".infoIconArTop", function () {
    var b = $(this).offset();
    var a = $(".arModuleToolTip").height();
    if ($(".arModuleToolTip").css("display") == "none") {
        showToolTip((b.top - a), (b.left + 30))
    } else {
        if ($(".arModuleToolTip").css("display") == "block") {
            $(".arModuleToolTip").hide()
        }
    }
});
function showToolTip(b, a) {
    if ($.browser.msie && parseInt($.browser.version, 10) < 9) {
        $(".arcontainer").css("top", b + 5)
    } else {
        $(".arcontainer").css("top", b)
    }
    $(".arModuleToolTip").show()
}
$(document).on("click", ".tooltipClose", function () {
    $(".arModuleToolTip").hide()
});
$(document).on("click", ".arTop li", function () {
    var a = $(this).index();
    var b = $(this).find("#destinationURL").text();
    if (ImageCount == a + 1) {
        goToURL(b, "gallery")
    } else {
        goToURL(b, "product")
    }
});
$(document).on("click", ".jcarousel-skin-ar li", function () {
    var a = $(this).index();
    var b = $(this).find("#destinationURL").text();
    if (ImageCount == a + 1) {
        goToURL(b, "gallery")
    } else {
        goToURL(b, "product")
    }
});
$(document).on("click", ".arClose", function () {
    $(".arProductContainer").hide();
    $("#ModuleLightbox").remove()
});
$(".arClose").bind("mouseenter", function () {
    $(this).css("cursor", "pointer")
});
$(".arClose").bind("mouseleave", function () {
    $(this).css("cursor", "hand")
});
function goToURL(c, b) {
    SetHeaderCookie();
    if (b == "product") {
        var a = new com.art.core.components.LightBox("ModuleLightbox", "body", 0.7);
        a.show();
        $(".arProductContainer").css({position: "absolute", "z-index": "1008"});
        $(".arProductDetailsContainer").html(showLoadingImage());
        $(".arProductDetailsContainer logo").centerElement();
        $(".arProductContainer").center();
        this.detailsDelayTimerID = setTimeout(function () {
            $(".arProductDetailsContainer").html(getProductPageHTML(c))
        }, 1000);
        $(".arProductContainer").show()
    } else {
        window.location.href = c
    }
}
function getProductPageHTML(a) {
    var b = "";
    a = a + "?inLightBox=true";
    b += "<iframe src='" + a + "' class='arProductDetails' frameBorder=0></iframe>";
    return b
}
function showLoadingImage() {
    var a = "<logo src='http://cache1.artprintimages.com/images/photostoart/loading.gif' id='spinner'/>";
    return a
}
function SetHeaderCookie() {
    var a = 1;
    cookie.setCookiePersistent("ap", "ARvisited", a);
    SetARTCookie(window.location.href, "false")
};
var t;
var currentSortByID = "P_SiteRank";
var strSortBy = '<font color="#888888">SORT BY: </font>';
var $selectedThumb;
var saveMenuFlag = false;
var cookie = new com.art.core.cookie.Cookie();
$(document).ready(function () {
    //showToggleifOn();
    var a = "";
    a = GetCookieDictionary("ENDECA", "Ns");
    if (a != "") {
        $(".gal-sortby").each(function () {
            if ($(this).attr("id") == a) {
                currentSortByID = $(this).attr("id");
                newSortByText = $(this).text();
                $("#gal-customcombobox").text(newSortByText);
                setSelectedSortBy(currentSortByID)
            }
        })
    }
    $("#gal-customcomboboxcontainer").hover(function () {
        if (com.art.core.utils.BrowserUtil.isiOSDevice()) {
            $("#gal-customcombooptions").toggle();
            return false
        } else {
            $("#gal-hovercombobox").show();
            $("#gal-hovercombobox").html($.trim($("#gal-customcombobox").html()))
        }
    }, function () {
        $("#gal-hovercombobox").hide()
    }).click(function () {
        $("#gal-customcombooptions").toggle();
        return false
    });
    $("#gal-customcombobox").html(strSortBy + $.trim($("#gal-customcombobox").text()));
    $(".gal-sortby").bind("click", function () {
        var c = $.trim($(this).text());
        $("#gal-customcombobox").html(strSortBy + c);
        currentSortByID = $(this).attr("id");
        $("#gal-customcombooptions").hide();
        cookie.setCookieSession("WT", "WT.cg_n", "Gallery+Sort+Change");
        selectNs(currentSortByID);
        setSelectedSortBy(currentSortByID)
    }).hover(function () {
        $(".gal-sortby").css({backgroundColor: ""});
        $(this).css({backgroundColor: "#000000"})
    }, function () {
    });
    $(".galActions .galButton,.gal-button,.gal-icon-button,.galRankHeart").hover(function () {
        if (!$(this).hasClass("disabled")) {
            $(this).addClass("hover")
        }
    }, function () {
        $(this).removeClass("hover")
    });
    $("#gal-gotop,.arrow-icon").hover(function () {
        $("#gal-gotop").addClass("hover");
        $(".arrow-icon").addClass("hover")
    }, function () {
        $("#gal-gotop").removeClass("hover");
        $(".arrow-icon").removeClass("hover")
    });
    $(document).on("click", "#gal-gotop-icon,#gal-gotop", function () {
        $("html, body").animate({scrollTop: 0}, "slow")
    });
    $("html").click(function () {
        $(".VdwhDetails").hide()
    });
    $(document).on("click", ".Vdwh", function () {
        siteWideGA.trackEventWithGroupCategory("Judy", "Toggle Go to Gemini", "	What�s This Link")
    });
    $(document).on("click", ".VDiscovery", function () {
        var c = checkIffiltersSelected();
        if (c) {
            trace("filter exists");
            showConfirmationDialog()
        } else {
            siteWideGA.trackEventWithGroupCategory("Judy", "Toggle Go to Gemini", "Go to Gemini");
            cookie.setCookiePersistent("ap", "abtest1", "1");
            cookie.setCookiePersistent("ap", "tgo", "v");
            redirectToJudy()
        }
    });
    $(document).bind("click", function () {
        $("#gal-customcombooptions").hide();
        if (!saveMenuFlag) {
            $("#gal-zoom").hide()
        }
    });
    $("#gal-zoom").bind("click", function () {
        if (!saveMenuFlag) {
            $(this).hide()
        }
    });
    $(".gal-product-size-multi").hover(function () {
        $(this).addClass("gal-hover")
    }, function () {
        $(this).removeClass("gal-hover")
    });
    $(".galThumbContainer .galTitle, .galThumbContainer .galSalePrice, .galThumbContainer .galPrice, .galThumbContainer .galProductType, .galThumbContainer .gal-product-size, .galThumbContainer .galProductType, .galThumbContainer .gal-product-size-multi ").bind("click", function () {
        productHref = $(this).parents(".galThumbContainer").find(".galImageCell a").attr("href");
        location.href = productHref
    });
    $(document).on("click", "#gal-zoom .galTitle,#gal-zoom .gal-product-size-multi", function () {
        productHref = $(this).parents("#gal-zoom").find(".gal-zoom-logo-href").attr("href");
        location.href = productHref
    });
    $(".galImage").click(function (c) {
        if ($("#popupplaceholder").is(":visible")) {
            c.preventDefault()
        }
    });
    $(".galArtistProduct").find(".galProductType").each(function () {
        var c = $(this).text();
        if (c == "Art Print" || c == "Giclee Print" || c == "Photographic Print" || c == "Serigraph" || c == "Wall Tapestry" || c == "Stetched Canvas Transfer" || c == "Hand Painted Art" || c == "Limited Edition") {
            $(this).addClass("type-tooltip-link")
        }
    });
    $(document).on("mouseenter", ".type-tooltip-link", function () {
        if ($(this).parents("#gal-zoom") > 0) {
            return false
        }
        clearTimeout(t);
        displayTypeTooltip($(this))
    });
    $(document).on("mouseleave", ".type-tooltip-link", function () {
        clearTimeout(t);
        t = setTimeout("hideTypeTooltip()", 500)
    });
    $("#type-tooltip").hover(function () {
        clearTimeout(t)
    }, function () {
        clearTimeout(t);
        t = setTimeout("hideTypeTooltip()", 500)
    });
    $("#type-tooltip-more").hover(function () {
        $(this).css("color", "#EF9223")
    }, function () {
        $(this).css("color", "#888")
    });
    $("#type-tooltip-more").bind("click", function () {
        var d = $("#type-tooltip-title").text();
        var c = "dialog=1,resizable=0,menubar=0,scrollbars=0,status=0,toolbar=0,width=430,height=456";
        var e = "productType=" + d;
        window.open("http://www.romvek.ru/asp/ProductTypeInfo/ProductTypeInfo.asp?" + e, "", c)
    });
    if ($.browser.msie && parseInt($.browser.version, 10) < 9) {
        $("<style>.gal-zoom-logo-href .shadow{margin: 0px auto !important;} #gal-zoom-inner {padding-right:8px !important;} </style>").appendTo("head")
    }
    $(".gal-zoom-add-to-cart").bind("click", function () {
        $selectedThumb.find(".galAddToCart").trigger("click")
    });
    $(".gal-zoom-frame-it").bind("click", function () {
        $selectedThumb.find(".galFrameIt").trigger("click")
    });
    $(".gal-save-to-gallery").bind("click", function () {
        $selectedThumb.find(".gal-save-to-gal").trigger("click")
    });
    $(".galImage").mouseenter(function () {
        var h = $(this).parents(".galThumbContainer");
        $selectedThumb = h;
        var j = $("#gal-zoom");
        j.hide();
        var f = $(this);
        var k = j.find(".gal-zoom-image");
        var s = $(this).parent().attr("href");
        $(".gal-zoom-logo-href").attr("href", s);
        if ($("#SaveMenuContainer").length > 0) {
            $(document).off("#SaveMenuContainer");
            $("#SaveMenuContainer").unbind("click");
            $("#SaveMenuContainer").remove()
        }
        if ($("#popupplaceholder").is(":visible")) {
            clearTimeout(t);
            return false
        }
        $theImage = f;
        medImageWidth = $theImage.width();
        medImageHeight = $theImage.height();
        var v = 350;
        var u = 390;
        var B;
        if (medImageWidth >= medImageHeight) {
            B = medImageHeight / medImageWidth;
            lrgImageWidth = v;
            lrgImageHeight = Math.round(lrgImageWidth * B)
        } else {
            B = medImageWidth / medImageHeight;
            lrgImageHeight = u;
            lrgImageWidth = Math.round(lrgImageHeight * B)
        }
        theItemImageSrc = $theImage.attr("src");
        k.attr("src", theItemImageSrc);
        k.attr("width", lrgImageWidth);
        k.attr("height", lrgImageHeight);
        newItemImageSrc = theItemImageSrc.replace("MED", "LRG");
        var A = theItemImageSrc.indexOf("MXW");
        if (A > 1) {
            newItemImageSrc = theItemImageSrc.replace(theItemImageSrc.substr(A, theItemImageSrc.indexOf("MXH") - A), "MXW:" + v + "+");
            A = newItemImageSrc.indexOf("MXH");
            newItemImageSrc = newItemImageSrc.replace(newItemImageSrc.substr(A, newItemImageSrc.indexOf("QLT") - A), "MXH:" + u + "+")
        }
        k.attr("src", newItemImageSrc);
        if (f.hasClass("shadow")) {
            k.addClass("shadow")
        } else {
            k.removeClass("shadow")
        }
        var n = h.find(".galDetailsContainer").html();
        j.find(".gal-zoom-text").html(n);
        j.find(".galPricing > .galRanking").css("display", "none");
        j.find(".gal-zoom-text > .galArtistProduct").css("margin-bottom", "15px");
        j.find(".galTitle").text(f.attr("title"));
        var c = j.find(".gal-artist");
        c.attr("onmouseover", 'this.style.color="#EF9223"');
        c.attr("onmouseout", 'this.style.color="#888"');
        var l = h.find(".galAddToCart").attr("onclick");
        var p = h.find(".galFrameIt").attr("onclick");
        var r = h.find(".gal-save-to-gal").attr("onclick");
        j.find(".gal-zoom-frame-it").removeClass("disabled").text("frame it");
        if (h.find(".galFrameIt").css("visibility") == "hidden") {
            j.find(".gal-zoom-frame-it").addClass("disabled").text("can't frame");
            j.find(".gal-zoom-frame-it").attr("onclick", "")
        }
        var z = h.offset();
        theItemTop = z.top;
        theItemLeft = z.left;
        zoomWidth = lrgImageWidth + 42;
        if (zoomWidth < 335) {
            zoomWidth = 335
        }
        j.width(zoomWidth);
        largeImageTop = $theImage.offset().top - (lrgImageHeight - medImageHeight) / 2;
        theItemLeft = theItemLeft - (j.width() - h.width()) / 2;
        theItemTop = largeImageTop - 21;
        var D = window, m = document, o = m.documentElement, q = m.getElementsByTagName("body")[0], E = D.innerWidth || o.clientWidth || q.clientWidth, F = D.innerHeight || o.clientHeight || q.clientHeight;
        qscreenHeight = F;
        qscreenWidth = E;
        var C = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop;
        C = parseInt(C);
        if ((theItemTop - C + j.height()) > (qscreenHeight)) {
            theItemTop = C - ((j.height()) - qscreenHeight) - 30
        } else {
            if (theItemTop < C) {
                theItemTop = C
            }
        }
        if ((theItemLeft + j.width()) > (qscreenWidth)) {
            theItemLeft = qscreenWidth - j.width()
        }
        j.css("top", theItemTop);
        j.css("left", theItemLeft);
        t = setTimeout("showImageZoom()", 500);
        SetRecordThumbDetails(h);
        if (RecordThumbDetails.ShowCanvas == "True") {
            $("#gal-zoom-inner > #wrap1").addClass("canvaswrap1");
            $("#gal-zoom-inner > #wrap1 > #wrap2").addClass("canvaswrap2");
            $("#gal-zoom-inner > #wrap1 > #wrap2 > #wrap3").addClass("canvaswrap3")
        } else {
            $("#gal-zoom-inner > #wrap1").removeClass("canvaswrap1");
            $("#gal-zoom-inner > #wrap1 > #wrap2").removeClass("canvaswrap2");
            $("#gal-zoom-inner > #wrap1 > #wrap2 > #wrap3").removeClass("canvaswrap3")
        }
        SetGlobalSessionInfo(h)
    });
    $("#gal-zoom").bind("mouseleave", function () {
        if (!saveMenuFlag) {
            hideImageZoom()
        }
    });
    $(".galImage").bind("mouseout", function () {
        clearTimeout(t)
    });
    $("#galleft #pane2 .jspVerticalBar").show();
    if ($("#seeAllSubjectId").length > 0) {
        var b = false;
        $("#galleft #pane2").css("height", "155px");
        $("#galleft #pane2 .jspVerticalBar").hide();
        $(document).on("click", "#seeAllSubjectId .seeMore", function () {
            $(this).hide();
            $("#seeAllSubjectId .seeLess").show();
            $("#galleft #pane2").css("height", "355px");
            $("#galleft #pane2 .jspVerticalBar").show()
        });
        $(document).on("click", "#seeAllSubjectId .seeLess", function () {
            $(this).hide();
            $("#seeAllSubjectId .seeMore").show();
            $("#galleft #pane2").css("height", "155px");
            $("#galleft #pane2 .jspVerticalBar").hide();
            $("#galleft #pane2 .jspPane").css("top", "0px");
            $("#galleft #pane2 .jspDrag").css("top", "0px")
        });
        $(document).on("mouseover", "#seeAllSubjectId .seeMore, #seeAllSubjectId .seeLess", function () {
            $(this).css("color", "#EF9223")
        });
        $(document).on("mouseout", "#seeAllSubjectId .seeMore, #seeAllSubjectId .seeLess", function () {
            $(this).css("color", "#888888")
        })
    }
    window.setTimeout("countfadeout();", 5);
    trace("Gallery page=> Runnning promo content");
    judyGalleryEntryPoint("b")
});
function countfadeout() {
    if ($("#gal-usergalleries-counter").html().trim()) {
        var a = $("#gal-usergalleries-counter").text();
        $("#gal-usergalleries-count").fadeOut(5, function () {
            $("#gal-usergalleries-count").text(a).fadeIn(3000)
        })
    }
}
function showImageZoom() {
    $("#gal-zoom").fadeIn("fast");
    clearTimeout(t)
}
function hideImageZoom() {
    $("#gal-zoom").hide();
    $("#type-tooltip").hide()
}
function setSelectedSortBy(a) {
    $(".gal-sortby").each(function () {
        if ($(this).attr("id") == a) {
            $(".gal-sortby").css({backgroundColor: ""});
            $(this).css({backgroundColor: "#000000"})
        }
    })
}
function DisplayTooltipText(b) {
    var a;
    switch (b) {
        case"Art Print":
            a = "Expertly produced reproduction using high-quality paper and printing.";
            break;
        case"Giclee Print":
            a = "A high precision printing process delivered on heavy-weight, watercolor paper.";
            break;
        case"Photographic Print":
            a = "Exceptional detail and color captured on heavy-weight photographic paper.";
            break;
        case"Serigraph":
            a = "Silk screening produces a bold, vibrant art print of shop_images quality.";
            break;
        case"Wall Tapestry":
            a = "Textile art created with cotton or blended yarns on Jacquard looms.";
            break;
        case"Stetched Canvas Transfer":
            a = "Artist-grade cotton canvas wrapped on stretcher bars with hand-painted edges.";
            break;
        case"Hand Painted Art":
            a = "A handpainted original created by the same artist from start to finish.";
            break;
        case"Limited Edition":
            a = "Often signed and/or numbered, prints certified in a limited one-time printing. ";
            break;
        default:
            a = ""
    }
    return a
}
function displayTypeTooltip(a) {
    var b = $("#type-tooltip");
    b.hide();
    var f = a.text();
    var g = DisplayTooltipText(f);
    $("#type-tooltip-title").text(f);
    $("#type-tooltip-text").text(g);
    var d = a.offset();
    var e = d.top - b.height() - 10;
    if ($.browser.msie && parseInt($.browser.version, 10) < 9) {
        var e = d.top - b.height() - 2;
        $("div#type-tooltip-arrow").addClass("tooltip-arrow-ie8")
    }
    var c = d.left;
    b.css("top", e).css("left", c) - 5;
    t = setTimeout("showTypeTooltip()", 500)
}
function showTypeTooltip() {
    clearTimeout(t);
    $("#type-tooltip").show()
}
function hideTypeTooltip() {
    clearTimeout(t);
    $("#type-tooltip").hide()
}
$(".gal-vsearch-hdr .gal-l-nav-pad").hover(function () {
    $("#vsimg").removeClass("vsimg");
    $("#vstext").removeClass("vstext");
    $("#vsimg").addClass("vsimghover");
    $("#vstext").addClass("vstexthover")
}, function () {
    $("#vstext").removeClass("vstexthover");
    $("#vsimg").removeClass("vsimghover");
    $("#vstext").addClass("vstext");
    $("#vsimg").addClass("vsimg")
});
$(".clrallbtn, .leftnav-clrall").hover(function () {
    $(".clrallbtn").addClass("hover");
    $(".leftnav-clrall").addClass("catlnkover")
}, function () {
    $(".clrallbtn").removeClass("hover");
    $(".leftnav-clrall").removeClass("catlnkover");
    $(".leftnav-clrall").addClass("catlnk")
});
$(".leftnav-clrsel-header").hover(function () {
    $(this).siblings().addClass("hover");
    $(this).addClass("catlnkover")
}, function () {
    $(this).siblings().removeClass("hover");
    $(this).removeClass("catlnkover");
    $(this).addClass("catlnk")
});
$(".clrsel-btn").hover(function () {
    $(this).addClass("hover");
    $(this).siblings().addClass("catlnkover")
}, function () {
    $(this).removeClass("hover");
    $(this).siblings().removeClass("catlnkover");
    $(this).siblings().addClass("catlnk")
});
$(".Next, .previous").hover(function () {
    $(".Next").addClass("hover");
    $(".previous").addClass("hover")
}, function () {
    $(".Next").removeClass("hover");
    $(".previous").removeClass("hover")
});
$(".catlnk").click(function () {
    var b = "Gallery Static Filter";
    var a = $(this).text();
    a = a.replace(/^\s+|\s+$/g, "");
    var c = "";
    var c = $(this).parent().parent().parent().prev(".leftnav-header-Accordian").text().trim().toLowerCase();
    if (c == "product types" || c == "sizes" || c == "shapes" || c == "prices") {
        siteWideGA.trackEventWithGroupCategory(b, c, a)
    }
    if ($(this).parent().parent().parent().prev("div").hasClass("leftnav-subtitle-noindent") || $(this).parent().parent().parent().prev("div").hasClass("leftnav-subtitle")) {
        c = $(this).parent().parent().parent().prev().prev().prev(".leftnav-header-Accordian").text().trim().toLowerCase();
        if (c == "product types") {
            siteWideGA.trackEventWithGroupCategory(b, c, a)
        }
    }
});
$(".cat").click(function () {
    var b = "Gallery Static Filter";
    var a = $(this).text();
    a = a.replace(/^\s+|\s+$/g, "");
    var c = "";
    c = $(this).parent().parent().parent().prev(".leftnav-header-Accordian").text().trim().toLowerCase();
    if (c == "") {
        c = $(this).parent().parent().parent().prev().prev(".leftnav-header-Accordian").text().trim().toLowerCase()
    }
    if (c == "product types" || c == "art styles" || c == "shop by room" || c == "art medium") {
        siteWideGA.trackEventWithGroupCategory(b, c, a)
    }
    if ($(this).parent().parent().parent().prev("div").hasClass("leftnav-subtitle-noindent") || $(this).parent().parent().parent().prev("div").hasClass("leftnav-subtitle")) {
        c = $(this).parent().parent().parent().prev().prev().prev(".leftnav-header-Accordian").text().trim().toLowerCase();
        if (c == "") {
            c = $(this).parent().parent().parent().prev().prev(".leftnav-header-Accordian").text().trim().toLowerCase()
        }
        if (c == "product types" || c == "art styles" || c == "shop by room" || c == "art medium") {
            siteWideGA.trackEventWithGroupCategory(b, c, a)
        }
    }
});
$(".mod_clr_swtch").click(function () {
    var b = "Gallery Static Filter";
    var a = $(this).attr("title");
    var c = "";
    var c = $(this).parent().parent().prev().prev(".leftnav-header-Accordian").text();
    siteWideGA.trackEventWithGroupCategory(b, c, a)
});
$(document).bind("click", function (b) {
    var a = $(b.target);
    if ((a.attr("id") != "popupplaceholder") && a.parents("#popupplaceholder").length <= 0) {
        $("#popupplaceholder").hide()
    }
});
$(".galgoback").hover(function () {
    $(this).addClass("hover")
}, function () {
    $(this).removeClass("hover")
});
if ($("#gobackmain").length > 0) {
    var noresultstext = '<span class="gCustomFont gal-header-text" style="line-height:60px; ">NO RESULTS FOUND</span>';
    $("#didyoumeanc").append(noresultstext)
}
function CallSaveToGalleryMenu(a) {
    saveMenuFlag = true;
    MyGalleriesCore.getModel().setSelectedImageObject(a);
    var b = $(".gal-save-to-gallery");
    MyGalleriesCore.getModel().saveMenuPosition.left = b.offset().left;
    MyGalleriesCore.getModel().saveMenuPosition.top = b.offset().top;
    MyGalleriesCore.getModel().saveMenuPosition.width = b.width();
    MyGalleriesCore.getModel().saveMenuSourceClick = "gal-save-to-gal";
    MyGalleriesCore.getModel().saveDefaultGalleryTitle = "Saved";
    MyGalleriesCore.externalSaveToGalleryMenu(MyGalleriesCore.constants.GALLERY_PAGE)
}
function CallSaveToGalleryMenuRoom(a, d) {
    if ($("#SaveMenuContainer").length > 0) {
        $(document).off("#SaveMenuContainer");
        $("#SaveMenuContainer").unbind("click");
        $("#SaveMenuContainer").remove()
    }
    var c = $("#gal-galleryname").text();
    saveMenuFlag = true;
    MyGalleriesCore.getModel().setSelectedImageObject(a);
    var b = $(d).parent();
    MyGalleriesCore.getModel().saveMenuPosition.left = b.offset().left - 40;
    MyGalleriesCore.getModel().saveMenuPosition.top = b.offset().top - 15;
    MyGalleriesCore.getModel().saveMenuPosition.width = b.width();
    MyGalleriesCore.getModel().saveMenuSourceClick = "gal-save-to-gal-room";
    MyGalleriesCore.getModel().saveDefaultGalleryTitle = c;
    MyGalleriesCore.externalSaveToGalleryMenu(MyGalleriesCore.constants.GALLERY_PAGE)
}
function showJudyPopup() {
    $("html").addClass("showIPadExperience");
    var c = new com.art.core.tracking.GoogleAnalytics("Judy");
    var b = '<div class="ipad_takeover"><div class="modal"><div class="wrapper"><span class="title">Try the new look and feel</span><span class="description">Great news! We\'ve created a new and improved experience for iPad. Find art using advanced color search, browse bigger images and swipe your way through galleries with more ease than ever.</span><a href="#" data-tracking-shop_images-experience="iPad Experience" class="btn btn_yes">iPad Experience</a><a href="#" data-tracking-shop_images-experience="Classic Experience" class="btn btn_no">Classic Experience</a></div></div></div>';
    $(".PageContainer").append(b);
    _gaq.push(["t1._setCustomVar", 5, "iPad Experience Lightbox", "Impression", 1]);
    c.trackGAWithCategoryNonInteraction("Judy", "iPad Partial Takeover - Load", "");
    var a = $(".ipad_takeover");
    a.on("click", ".btn_yes", function (d) {
        makeJudyABChoice("1")
    });
    a.on("click", ".btn_no", function (d) {
        makeJudyABChoice("2")
    })
}
function makeJudyABChoice(a) {
    var b = $("#judyABTestName").text();
    var c = new com.art.core.tracking.GoogleAnalytics("Judy");
    cookie.setCookiePersistent("ap", "abtestname", b);
    if (a == "1") {
        cookie.setCookiePersistent("ap", "abtest1", "1");
        redirectToJudy()
    } else {
        if (a == "2") {
            $("html").removeClass("showIPadExperience");
            cookie.setCookiePersistent("ap", "abtest1", "2")
        }
    }
}
function redirectToJudy(a) {
    var e = location.href;
    if (checkIffiltersSelected()) {
        e = cleanforJudySupportedUrl(e)
    }
    if (e.indexOf("http://www.romvek.ru/search_do.asp/") > 0) {
        var c = com.art.core.utils.BrowserUtil.getQuerystringByUrl("searchstring", e);
        e = e.replace("http://www.romvek.ru/asp/search_do.asp/_/", "/discover/keyword--" + cleanSearchString(c) + "/")
    } else {
        var d = com.art.core.utils.BrowserUtil.getQuerystringByUrl("searchstring", e);
        if (d.length > 0) {
            e = "http://" + location.host + "/discover/keyword--" + cleanSearchString(d) + "/posters.html"
        } else {
            e = e.replace("http://www.romvek.ru/shop_images/", "http://www.romvek.ru/discover/");
            e = e.replace(/_p[0-9]+/, "")
        }
    }
    if (typeof(a) != "undefined" && a.urlParams.length > 0) {
        var b = "?";
        $.each(a.urlParams, function (f, g) {
            trace("param found =>" + g);
            if (f == 0) {
                b = b + g
            } else {
                b = b + "&" + g
            }
        });
        trace("Redirecting to judy with params :" + b);
        e = e + b
    } else {
        trace("No param found => redirecting to vanilla judy")
    }
    location.href = e
}
function cleanSearchString(a) {
    a = decodeURI(a);
    a = a.replace(/ /g, "-").toLowerCase();
    a = a.trim().replace(/[^\w\s\u00C0-\u00FF-]/gi, "");
    a = encodeURI(a);
    return a
}
function judyABTest() {
    if (isEligibleforJudy()) {
        var b = cookie.getCookieDictionary("ap", "abtest1") == "1" ? true : false;
        var a = $("#judyABTestName").text();
        var c = cookie.getCookieDictionary("ap", "abtestname") == a ? true : false;
        var d = true;
        if (a == "") {
            c = true
        }
        if (a == "off") {
            return
        }
        if (a == "on") {
            redirectToJudy();
            return
        }
        if (typeof wtJudySetiPadPopup == "function") {
            d = wtJudySetiPadPopup()
        }
        if (!d || (cookie.getCookieDictionary("ap", "abtest1") == "2" && c)) {
            console.log("*ipad already chose legacy experience");
            return false
        } else {
            if (!b && !c) {
                showJudyPopup()
            } else {
                if (b && c) {
                    console.log("*ipad first time on shop_images page for this session");
                    updateGalleryLinksForJudy()
                }
            }
        }
    }
}
$(document).ready(function () {
});
function checkIffiltersSelected() {
    var h = false;
    var b = location.href;
    var d = b.split("http://www.romvek.ru/");
    var a = d[4];
    trace(a);
    var c = a.indexOf("id--");
    var e = a.substring(c + 4);
    trace(e);
    var f = e.split("-");
    trace(f);
    if (f.length > 1) {
        _.each(f, function (j) {
            var k = j.substring(0, 1);
            if (k === "e" || k === "g" || k === "h" || k === "f") {
                h = true
            }
        })
    } else {
        if (f.length == 1) {
            var g = f[0].substring(0, 1);
            if (g == "e" || g == "g" || g == "h" || g == "f") {
                h = true
            }
        } else {
            h = false
        }
    }
    return h
}
function cleanforJudySupportedUrl(e) {
    var b = location.href;
    var a = "";
    var d = b.split("http://www.romvek.ru/");
    var c = d[0] + "//" + d[1] + "/" + d[2] + "/" + d[3] + "/" + buildCatIdsExcludingFiltersNotSupported() + "/" + d[5];
    return c
}
function buildCatIdsExcludingFiltersNotSupported() {
    var c = location.href;
    var a = "";
    var e = c.split("http://www.romvek.ru/");
    var b = e[4];
    trace(b);
    var d = b.indexOf("id--");
    var g = b.substring(d + 4);
    trace(g);
    var h = g.split("-");
    trace(h);
    if (h.length > 1) {
        var f = false;
        a = "";
        _.each(h, function (k) {
            var l = k.substring(0, 1);
            if (l === "b" || l === "c" || l === "d" || l === "a") {
                a += k + "-";
                f = true
            }
        });
        a = "id--" + a;
        a = a.substring(0, a.length - 1);
        trace("final string" + a);
        if (!f) {
            a = "id--0"
        }
    } else {
        if (h.length == 1) {
            a = "";
            var j = h[0].substring(0, 1);
            if (j == "e" || j == "g" || j == "h" || j == "f") {
                a = "id--0"
            }
        } else {
            a = "id--0"
        }
    }
    return a
}
function showConfirmationDialog() {
    var c = "<div class='vMessage'> If you switch to Visual Mode, all of your filter selections will be cleared.</div>";
    var d = {title: "A NEW WAY TO BROWSE ART"};
    var b = new com.art.core.components.BaseModal("modal", 520, "#FFFFFF", true, false, d);
    b.setContents(c);
    var a = new com.art.core.components.LightBox("lb", "body", 0.5);
    a.setLightBoxZIndex(1000);
    a.show();
    $("body").append(b.render(a.getLightBoxZIndex() + 1));
    b.registerButton("id_continue", com.art.core.components.ArtButton.ART_ORANGE, "SWITCH TO VISUAL MODE", function (e) {
        siteWideGA.trackEventWithGroupCategory("Judy", "Toggle Go to Gemini", "	Filter Alert LB - Switch to Gemini");
        cookie.setCookiePersistent("ap", "abtest1", "1");
        cookie.setCookiePersistent("ap", "tgo", "v");
        redirectToJudy()
    }, com.art.core.components.ArtButton.ARROW_RIGHT);
    b.registerButton("id_cancel", com.art.core.components.ArtButton.ART_BLUE, "STAY IN CLASSIC MODE", function () {
        siteWideGA.trackEventWithGroupCategory("Judy", "Toggle Go to Gemini", "Filter Alert LB - Stay in Classic");
        b.close();
        a.close()
    });
    b.registerCallback(com.art.core.components.BaseModal.CLOSE_CLICKED, function () {
        siteWideGA.trackEventWithGroupCategory("Judy", "Toggle Go to Gemini", "Filter Alert LB - Close");
        a.close()
    });
    b.registerEvents()
}
function showToggleifOn() {
    if (_.isUndefined($serverSideConfig)) {
        $(".VDiscoveryContainer").hide();
        return
    }
    if ($serverSideConfig.enableToggle === "true" && !com.art.core.utils.BusinessUtil.isMobile()) {
        $(".VDiscoveryContainer").show()
    } else {
        $(".VDiscoveryContainer").hide()
    }
};
var isVSALoaded = false;
var VSjsFilepath = "";
/*
$(document).ready(function () {
    VSjsFilepath = MyGalleriesEnvironmentVariables.cdnPath + "/" + MyGalleriesEnvironmentVariables.versionNumber + "/applications/visualSearch/js/VisualSearchApplication.js";
    var a = {
        intro: {
            title: "",
            continueButton: "CONTINUE",
            bodyHeader: "Provide us with an image you like",
            body: '<div class="VSIntroRadioContainer"><span class="VSradio1"></span><input type="hidden" name="radiovs" value="upload"/><div id="IntroRadioTxt1" class="IntroRadioTxt">Upload a file from my computer</div><div class="clear"></div><span class="VSradio2"></span><input type="hidden" name="radiovs" value="link"/><div id="IntroRadioTxt2" class="IntroRadioTxt">Enter a link to the image I like</div></div>',
            contentRight: ""
        },
        upload: {
            title: "Upload File",
            continueButton: "UPLOAD & CONTINUE",
            bodyHeader: "",
            body: "<span class='uploadtxt'>Upload  a file from my computer</span>",
            contentRight: "<div><p>&bull; We accept the following formats: jpeg, jpg, png, or gif.<p>&bull; Please select an image between 25KB and 10MB in size.</p></div>"
        },
        link: {
            title: "Enter Link",
            continueButton: "UPLOAD & CONTINUE",
            bodyHeader: "",
            body: "Please enter the link to the image you like",
            contentRight: "<div><p>&bull; The link you provide must end in jpeg, jpg, png, or gif.</p><p> &bull; File size must be greater than 25KB.</p></div>"
        }
    };
    $("#vs-upload,#vs-upload2,#vs-upload3,#vs-upload4,#vs-upload5,#vs-upload6").bind("click", function () {
        if (!isVSALoaded) {
            $.getScript(VSjsFilepath, function () {
                VisualSearchApplication_upload(VisualSearchCore);
                isVSALoaded = true
            })
        } else {
            VisualSearchApplication_upload(VisualSearchCore)
        }
    });
    $("#vs-wizard, #vs-wizard2, #vs-wizard3, #vs-wizard4, #vs-wizard5, #vs-wizard6").bind("click", function () {
        if (!isVSALoaded) {
            $.getScript(VSjsFilepath, function () {
                VisualSearchApplication_Inspire();
                isVSALoaded = true
            })
        } else {
            VisualSearchApplication_Inspire()
        }
    });
    $("#vs-wizardStep3").bind("click", function () {
        if (!isVSALoaded) {
            $.getScript(VSjsFilepath, function () {
                VisualSearchApplication_step3();
                isVSALoaded = true
            })
        } else {
            VisualSearchApplication_step3()
        }
    });
    $("#vs-wizard-lp, #vs-wizard-lp2, #vs-wizard-lp3, #vs-wizard-lp4, #vs-wizard-lp5, #vs-wizard-lp6").bind("click", function () {
        if (!isVSALoaded) {
            $.getScript(VSjsFilepath, function () {
                VisualSearchApplication_VsWizardLp();
                isVSALoaded = true
            })
        } else {
            VisualSearchApplication_VsWizardLp()
        }
    });
    $("#vs-get-images").bind("click", function () {
        if (!isVSALoaded) {
            $.getScript(VSjsFilepath, function () {
                VisualSearchApplication_VsGetImages();
                isVSALoaded = true
            })
        } else {
            VisualSearchApplication_VsGetImages()
        }
    });
    VisualSearchApplication_upload = function () {
        VisualSearchCore.init(visualSearchCoreEnvironment);
        var b = $(this).attr("id") == "vs-upload" ? "VSexactsearchLN" : "VSexactsearchblock";
        VisualSearchCore.GATrackEvent(b);
        VisualSearchCore.registerModule(new _art_.visualSearch.modules.ModuleLightbox({target: "body"}, VisualSearchCore));
        VisualSearchCore.registerModule(new _art_.visualSearch.modules.ModuleUpload({
            target: "body",
            content: a
        }, VisualSearchCore));
        VisualSearchCore.registerModule(new _art_.visualSearch.modules.ModuleCurrentSearch({
            target: "body",
            content: {}
        }, VisualSearchCore));
        VisualSearchCore.registerModule(new _art_.visualSearch.modules.ModuleSearchResults({
            target: ".VSResultsContentRight",
            content: {}
        }, VisualSearchCore));
        VisualSearchCore.startModule("ModuleLightbox");
        VisualSearchCore.startModule("ModuleUpload");
        VisualSearchCore.getModule("ModuleUpload").updateContent("intro")
    };
    VisualSearchApplication_Inspire = function () {
        VisualSearchCore.init(visualSearchCoreEnvironment);
        var b = $(this).attr("id") == "vs-wizard" ? "VS HP left nav Inspire me" : "VS HP block Inspire me";
        VisualSearchCore.GATrackEvent(b);
        VisualSearchCore.registerModule(new _art_.visualSearch.modules.ModuleLightbox({target: "body"}, VisualSearchCore));
        VisualSearchCore.registerModule(new _art_.visualSearch.modules.ModuleMultipleItemSearch({target: "body"}, VisualSearchCore));
        VisualSearchCore.registerModule(new _art_.visualSearch.modules.ModuleWizard({target: "body"}, VisualSearchCore));
        VisualSearchCore.registerModule(new _art_.visualSearch.modules.ModuleWizardIntro({target: "body"}, VisualSearchCore));
        VisualSearchCore.startModule("ModuleLightbox");
        VisualSearchCore.startModule("ModuleWizardIntro")
    };
    VisualSearchApplication_step3 = function () {
        VisualSearchCore.registerModule(new _art_.visualSearch.modules.ModuleLightbox({target: "body"}, VisualSearchCore));
        VisualSearchCore.registerModule(new _art_.visualSearch.modules.ModuleMultipleItemSearchResults({target: "body"}, VisualSearchCore));
        VisualSearchCore.registerModule(new _art_.visualSearch.modules.ModuleWizard({target: "body"}, VisualSearchCore));
        VisualSearchCore.startModule("ModuleLightbox");
        VisualSearchCore.startModule("ModuleMultipleItemSearchResults")
    };
    VisualSearchApplication_VsWizardLp = function () {
        VisualSearchCore.init(visualSearchCoreEnvironment);
        VisualSearchCore.registerModule(new _art_.visualSearch.modules.ModuleLightbox({target: "body"}, VisualSearchCore));
        VisualSearchCore.registerModule(new _art_.visualSearch.modules.ModuleMultipleItemSearch({target: "body"}, VisualSearchCore));
        VisualSearchCore.registerModule(new _art_.visualSearch.modules.ModuleWizard({target: "body"}, VisualSearchCore));
        VisualSearchCore.startModule("ModuleLightbox");
        VisualSearchCore.startModule("ModuleWizard")
    };
    VisualSearchApplication_VsGetImages = function () {
        VisualSearchCore.init(visualSearchCoreEnvironment);
        var b = new com.art.core.vos.ImageVO();
        b.getTestCatalogImage();
        VisualSearchCore.registerModule(new _art_.visualSearch.modules.ModuleLightbox({target: "body"}, VisualSearchCore));
        VisualSearchCore.registerModule(new _art_.visualSearch.modules.ModuleCurrentSearch({
            target: "body",
            content: {}
        }, VisualSearchCore));
        VisualSearchCore.registerModule(new _art_.visualSearch.modules.ModuleSearchResults({
            target: ".VSResultsContentRight",
            content: {}
        }, VisualSearchCore));
        VisualSearchCore.startModule("ModuleLightbox");
        VisualSearchCore.sendNotification(new _art_.core.Note(VisualSearchCore.events.SEARCH_FROM_CATALOG, {pivotImage: b}, "vo"))
    }
});*/