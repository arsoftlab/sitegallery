/*
 * jQzoom Evolution Library v2.3  - Javascript Image magnifier
 * http://www.mind-projects.it
 *
 * Copyright 2011, Engineer Marco Renzi
 * Licensed under the BSD license.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the organization nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * Date: 03 May 2011 22:16:00
 *
 * Date: 27 Apr 2012
 * Note: Modified the zoom event to trigger only for mouseenter (i.e removed the mouseover binding). Added checkedOnce to verify whether user entered intentionally to zoom.
 * Details: Please check SCRIPTS\jquery.jqzoom-core.js changes: This is packed version of that file.
 */
eval(function (h, b, d, g, f, i) {
    f = function (a) {
        return (a < b ? "" : f(parseInt(a / b))) + ((a = a % b) > 35 ? String.fromCharCode(a + 29) : a.toString(36))
    };
    if (!"".replace(/^/, String)) {
        while (d--) {
            i[f(d)] = g[d] || f(d)
        }
        g = [function (a) {
            return i[a]
        }];
        f = function () {
            return "\\w+"
        };
        d = 1
    }
    while (d--) {
        if (g[d]) {
            h = h.replace(new RegExp("\\b" + f(d) + "\\b", "g"), g[d])
        }
    }
    return h
}("9 1p=-1;9 1C=-1;9 1D;9 1E;9 25;9 26;9 27=F;$(1v).2r(8(e){A(1p<0){1p=e.1F;1C=e.1G}});(8($){9 v=($.1H.2s&&$.1H.2t<7);9 w=$(1v.1I);9 y=$(y);9 z=F;$.3m.15=8(b){G 5.28(8(){9 a=5.3n.3o();A(a=='a'){Q 15(5,b)}})};15=8(g,h){9 j=2u;j=$(g).1J(\"15\");A(j)G j;9 k=5;9 l=$.1K({},$.15.2v,h||{});k.3p=g;g.1q=$(g).R('1q');g.1L=F;g.3q=F;g.1w=F;g.1h=F;g.1c={};g.2w=2u;g.16={};g.1M=F;$(g).D({'3r-1i':'1x','3s-3t':'1x'});9 m=$(\"3u:3v(0)\",g);g.V=$(g).R('V');g.29=m.R('V');9 n=($.1y(g.V).Y>0)?g.V:g.29;9 p=Q 2x(m);9 q=Q 2y();9 r=Q 2z();9 s=Q 2A();9 t=Q 2B();$(g).1N('2C',8(e){e.2D();G F});9 u=['2a','1d','1j','1r'];A($.3w($.1y(l.H),u)<0){l.H='2a'}$.1K(k,{2b:8(){A($(\".L\",g).Y==0){g.L=$('<Z/>').1O('L');m.3x(g.L)}A(l.H=='1j'){l.17=p.w;l.18=p.h}A($(\".2c\",g).Y==0){q.S()}A($(\".2d\",g).Y==0){r.S()}A($(\".2E\",g).Y==0){t.S()}A(l.2e||l.H=='1d'||l.1P){k.1Q()}k.2F()},2F:8(){A(l.H=='1d'){$(\".L\",g).3y(8(){g.1M=13});$(\".L\",g).3z(8(){g.1M=F});1v.1I.3A=8(){G F};$(\".L\",g).D({1R:'1s'});$(\".2c\",g).D({1R:'3B'})}A(l.H=='1j'){$(\".1S\",g).D({1R:'3C'})}$(\".L\",g).1N('3D',8(a){3E{1D=$('#1T').14().B;1E=$('#1T').14().C;25=1D+$('#1T').O();26=1E+$('#1T').11();A(1p<0||(1p>1D&&1p<25)&&(1C>1E&&1C<26)){A(!27){27=13;G F}}}3F(e){}m.R('V','');$(g).R('V','');g.1L=13;p.1t();A(g.1h){k.2f(a)}1k{k.1Q()}});$(\".L\",g).1N('3G',8(a){k.2G()});$(\".L\",g).1N('2r',8(e){A(e.1F>p.E.r||e.1F<p.E.l||e.1G<p.E.t||e.1G>p.E.b){q.1U();G F}g.1L=13;A(g.1h&&!$('.2d',g).3H(':2H')){k.2f(e)}A(g.1h&&(l.H!='1d'||(l.H=='1d'&&g.1M))){q.1l(e)}});9 c=Q 2I();9 i=0;9 d=Q 2I();d=$('a').3I(8(){9 a=Q 3J(\"3K[\\\\s]*:[\\\\s]*'\"+$.1y(g.1q)+\"'\",\"i\");9 b=$(5).R('1q');A(a.3L(b)){G 5}});A(d.Y>0){9 f=d.3M(0,1);d.3N(f)}d.28(8(){A(l.2e){9 a=$.1K({},1V(\"(\"+$.1y($(5).R('1q'))+\")\"));c[i]=Q 2g();c[i].1e=a.1z;i++}$(5).2C(8(e){A($(5).3O('2h')){G F}d.28(8(){$(5).3P('2h')});e.2D();k.2J(5);G F})})},1Q:8(){A(g.1h==F&&g.1w==F){9 a=$(g).R('2K');g.1w=13;s.2L(a)}},2f:8(e){3Q(g.2w);q.T();r.T()},2G:8(e){1W(l.H){1u'1d':W;1s:m.R('V',g.29);$(g).R('V',g.V);A(l.1P){q.1U()}1k{r.P();q.P()}W}g.1L=F},2J:8(a){g.1w=F;g.1h=F;9 b=Q 3R();b=$.1K({},1V(\"(\"+$.1y($(a).R('1q'))+\")\"));A(b.1X&&b.1z){9 c=b.1X;9 d=b.1z;$(a).1O('2h');$(g).R('2K',d);m.R('1e',c);q.P();r.P();k.1Q()}1k{2i('2M :: 2N 2O 1Y 1z 2P 1X.');2j'2M :: 2N 2O 1Y 1z 2P 1X.';}G F}});A(m[0].3S){p.1t();A($(\".L\",g).Y==0)k.2b()}8 2x(c){9 d=5;5.6=c[0];5.2Q=8(){9 a=0;a=c.D('2k-C-O');M='';9 b=0;b=c.D('2k-B-O');K='';A(a){1Y(i=0;i<3;i++){9 x=[];x=a.1Z(i,1);A(2R(x)==F){M=M+''+a.1Z(i,1)}1k{W}}}A(b){1Y(i=0;i<3;i++){A(!2R(b.1Z(i,1))){K=K+b.1Z(i,1)}1k{W}}}d.M=(M.Y>0)?1V(M):0;d.K=(K.Y>0)?1V(K):0};5.1t=8(){d.2Q();d.w=c.O();d.h=c.11();d.1m=c.3T();d.1f=c.3U();d.E=c.14();d.E.l=c.14().B+d.K;d.E.t=c.14().C+d.M;d.E.r=d.w+d.E.l;d.E.b=d.h+d.E.t;d.2S=c.14().B+d.1m;d.3V=c.14().C+d.1f};5.6.2T=8(){2i('20 21 22 X.');2j'20 21 22 X.';};5.6.2U=8(){d.1t();A($(\".L\",g).Y==0)k.2b()};G d};8 2B(){9 a=5;5.S=8(){5.6=$('<Z/>').1O('2E').D('2l','2V').2W(l.2X);$('.L',g).S(5.6)};5.T=8(){5.6.C=(p.1f-5.6.11())/2;5.6.B=(p.1m-5.6.O())/2;5.6.D({C:5.6.C,B:5.6.B,12:'19',2l:'2H'})};5.P=8(){5.6.D('2l','2V')};G 5}8 2y(){9 d=5;5.6=$('<Z/>').1O('2c');5.S=8(){$('.L',g).S($(5.6).P());A(l.H=='1r'){5.X=Q 2g();5.X.1e=p.6.1e;$(5.6).2m().S(5.X)}};5.2Y=8(){5.6.w=(23((l.17)/g.1c.x)>p.w)?p.w:(23(l.17/g.1c.x));5.6.h=(23((l.18)/g.1c.y)>p.h)?p.h:(23(l.18/g.1c.y));5.6.C=(p.1f-5.6.h-2)/2;5.6.B=(p.1m-5.6.w-2)/2;5.6.D({C:0,B:0,O:5.6.w+'I',11:5.6.h+'I',12:'19',1g:'1x',2n:1+'I'});A(l.H=='1r'){5.X.1e=p.6.1e;$(5.6).D({'2o':1});$(5.X).D({12:'19',1g:'1A',B:-(5.6.B+1-p.K)+'I',C:-(5.6.C+1-p.M)+'I'})}};5.1U=8(){5.6.C=(p.1f-5.6.h-2)/2;5.6.B=(p.1m-5.6.w-2)/2;5.6.D({C:5.6.C,B:5.6.B});A(l.H=='1r'){$(5.X).D({12:'19',1g:'1A',B:-(5.6.B+1-p.K)+'I',C:-(5.6.C+1-p.M)+'I'})}s.1l()};5.1l=8(e){g.16.x=e.1F;g.16.y=e.1G;9 b=0;9 c=0;8 2Z(a){G g.16.x-(a.w)/2<p.E.l}8 30(a){G g.16.x+(a.w)/2>p.E.r}8 31(a){G g.16.y-(a.h)/2<p.E.t}8 32(a){G g.16.y+(a.h)/2>p.E.b}b=g.16.x+p.K-p.E.l-(5.6.w+2)/2;c=g.16.y+p.M-p.E.t-(5.6.h+2)/2;A(2Z(5.6)){b=p.K-1}1k A(30(5.6)){b=p.w+p.K-5.6.w-1}A(31(5.6)){c=p.M-1}1k A(32(5.6)){c=p.h+p.M-5.6.h-1}5.6.B=b;5.6.C=c;5.6.D({'B':b+'I','C':c+'I'});A(l.H=='1r'){A($.1H.2s&&$.1H.2t>7){$(5.6).2m().S(5.X)}$(5.X).D({12:'19',1g:'1A',B:-(5.6.B+1-p.K)+'I',C:-(5.6.C+1-p.M)+'I'})}s.1l()};5.P=8(){m.D({'2o':1});5.6.P()};5.T=8(){A(l.H!='1j'&&(l.33||l.H=='1d')){5.6.T()}A(l.H=='1r'){m.D({'2o':l.34})}};5.2p=8(){9 o={};o.B=d.6.B;o.C=d.6.C;G o};G 5};8 2z(){9 b=5;5.6=$(\"<Z 1B='2d'><Z 1B='1S'><Z 1B='24'></Z><Z 1B='2q'></Z></Z></Z>\");5.U=$('<35 1B=\"3W\" 1e=\"3X:\\'\\';\" 3Y=\"0\" 3Z=\"0\" 40=\"36\" 41=\"42\" 43=\"0\" ></35>');5.1l=8(){5.6.1n=0;5.6.1o=0;A(l.H!='1j'){1W(l.12){1u\"B\":5.6.1n=(p.E.l-p.K-J.N(l.1a)-l.17>0)?(0-l.17-J.N(l.1a)):(p.1m+J.N(l.1a));5.6.1o=J.N(l.1b);W;1u\"C\":5.6.1n=J.N(l.1a);5.6.1o=(p.E.t-p.M-J.N(l.1b)-l.18>0)?(0-l.18-J.N(l.1b)):(p.1f+J.N(l.1b));W;1u\"36\":5.6.1n=J.N(l.1a);5.6.1o=(p.E.t-p.M+p.1f+J.N(l.1b)+l.18<37.11)?(p.1f+J.N(l.1b)):(0-l.18-J.N(l.1b));W;1s:5.6.1n=(p.2S+J.N(l.1a)+l.17<37.O)?(p.1m+J.N(l.1a)):(0-l.17-J.N(l.1a));5.6.1o=J.N(l.1b);W}}5.6.D({'B':5.6.1n+'I','C':5.6.1o+'I'});G 5};5.S=8(){$('.L',g).S(5.6);5.6.D({12:'19',1g:'1x',38:44});A(l.H=='1j'){5.6.D({1R:'1s'});9 a=(p.K==0)?1:p.K;$('.1S',5.6).D({2n:a+'I'})}$('.1S',5.6).D({O:J.39(l.17)+'I',2n:a+'I'});$('.2q',5.6).D({O:'3a%',11:J.39(l.18)+'I'});$('.24',5.6).D({O:'3a%',12:'19'});$('.24',5.6).P();A(l.V&&n.Y>0){$('.24',5.6).2W(n).T()}b.1l()};5.P=8(){1W(l.3b){1u'45':5.6.46(l.3c,8(){});W;1s:5.6.P();W}5.U.P()};5.T=8(){1W(l.3d){1u'47':5.6.3e();5.6.3e(l.3f,8(){});W;1s:5.6.T();W}A(v&&l.H!='1j'){5.U.O=5.6.O();5.U.11=5.6.11();5.U.B=5.6.1n;5.U.C=5.6.1o;5.U.D({1g:'1A',12:\"19\",B:5.U.B,C:5.U.C,38:48,O:5.U.O+'I',11:5.U.11+'I'});$('.L',g).S(5.U);5.U.T()}}};8 2A(){9 c=5;5.6=Q 2g();5.2L=8(a){t.T();5.49=a;5.6.1i.12='19';5.6.1i.2k='3g';5.6.1i.1g='1x';5.6.1i.B='-4a';5.6.1i.C='3g';1v.1I.4b(5.6);5.6.1e=a};5.1t=8(){9 a=$(5.6);9 b={};5.6.1i.1g='1A';c.w=a.O();c.h=a.11();c.E=a.14();c.E.l=a.14().B;c.E.t=a.14().C;c.E.r=c.w+c.E.l;c.E.b=c.h+c.E.t;b.x=(c.w/p.w);b.y=(c.h/p.h);g.1c=b;1v.1I.4c(5.6);$('.2q',g).2m().S(5.6);q.2Y()};5.6.2T=8(){2i('20 21 22 3h 3i X.');2j'20 21 22 3h 3i X.';};5.6.2U=8(){c.1t();t.P();g.1w=F;g.1h=13;A(l.H=='1d'||l.1P){q.T();r.T();q.1U()}};5.1l=8(){9 a=-g.1c.x*(q.2p().B-p.K+1);9 b=-g.1c.y*(q.2p().C-p.M+1);$(5.6).D({'B':a+'I','C':b+'I'})};G 5};$(g).1J(\"15\",k)};$.15={2v:{H:'2a',17:3j,18:3j,1a:10,1b:0,12:\"4d\",2e:13,2X:'4e 4f',V:13,33:13,34:0.4,1P:F,3d:'T',3b:'P',3f:'4g',3c:'4h'},3k:8(a){9 b=$(a).1J('15');b.3k();G F},3l:8(a){9 b=$(a).1J('15');b.3l();G F},4i:8(a){z=13},4j:8(a){z=F}}})(4k);", 62, 269, "|||||this|node||function|var|||||||||||||||||||||||||||if|left|top|css|pos|false|return|zoomType|px|Math|bleft|zoomPad|btop|abs|width|hide|new|attr|append|show|ieframe|title|break|image|length|div||height|position|true|offset|jqzoom|mousepos|zoomWidth|zoomHeight|absolute|xOffset|yOffset|scale|drag|src|oh|display|largeimageloaded|style|innerzoom|else|setposition|ow|leftpos|toppos|firstX|rel|reverse|default|fetchdata|case|document|largeimageloading|none|trim|largeimage|block|class|firstY|mainImageX|mainImageY|pageX|pageY|browser|body|data|extend|zoom_active|mouseDown|bind|addClass|alwaysOn|load|cursor|zoomWrapper|MainImage|setcenter|eval|switch|smallimage|for|substr|Problems|while|loading|parseInt|zoomWrapperTitle|mainImageX2|mainImageY2|checkedOnce|each|imagetitle|standard|create|zoomPup|zoomWindow|preloadImages|activate|Image|zoomThumbActive|alert|throw|border|visibility|empty|borderWidth|opacity|getoffset|zoomWrapperImage|mousemove|msie|version|null|defaults|timer|Smallimage|Lens|Stage|Largeimage|Loader|click|preventDefault|zoomPreload|init|deactivate|visible|Array|swapimage|href|loadimage|ERROR|Missing|parameter|or|findborder|isNaN|rightlimit|onerror|onload|hidden|html|preloadText|setdimensions|overleft|overright|overtop|overbottom|lens|imageOpacity|iframe|bottom|screen|zIndex|round|100|hideEffect|fadeoutSpeed|showEffect|fadeIn|fadeinSpeed|0px|the|big|300|disable|enable|fn|nodeName|toLowerCase|el|zoom_disabled|outline|text|decoration|img|eq|inArray|wrap|mousedown|mouseup|ondragstart|move|crosshair|mouseenter|try|catch|mouseleave|is|filter|RegExp|gallery|test|splice|push|hasClass|removeClass|clearTimeout|Object|complete|outerWidth|outerHeight|bottomlimit|zoomIframe|javascript|marginwidth|marginheight|align|scrolling|no|frameborder|5001|fadeout|fadeOut|fadein|99|url|5000px|appendChild|removeChild|right|Loading|zoom|slow|2000|disableAll|enableAll|jQuery".split("|"), 0, {}));
jQuery.ui || (function (l) {
    var r = l.fn.remove, m = l.browser.mozilla && (parseFloat(l.browser.version) < 1.9);
    l.ui = {
        version: "1.7.2",
        plugin: {
            add: function (b, c, e) {
                var d = l.ui[b].prototype;
                for (var a in e) {
                    d.plugins[a] = d.plugins[a] || [];
                    d.plugins[a].push([c, e[a]])
                }
            }, call: function (a, c, b) {
                var e = a.plugins[c];
                if (!e || !a.element[0].parentNode) {
                    return
                }
                for (var d = 0; d < e.length; d++) {
                    if (a.options[e[d][0]]) {
                        e[d][1].apply(a.element, b)
                    }
                }
            }
        },
        contains: function (b, a) {
            return document.compareDocumentPosition ? b.compareDocumentPosition(a) & 16 : b !== a && b.contains(a)
        },
        hasScroll: function (d, b) {
            if (l(d).css("overflow") == "hidden") {
                return false
            }
            var a = (b && b == "left") ? "scrollLeft" : "scrollTop", c = false;
            if (d[a] > 0) {
                return true
            }
            d[a] = 1;
            c = (d[a] > 0);
            d[a] = 0;
            return c
        },
        isOverAxis: function (b, a, c) {
            return (b > a) && (b < (a + c))
        },
        isOver: function (f, b, e, d, a, c) {
            return l.ui.isOverAxis(f, e, a) && l.ui.isOverAxis(b, d, c)
        },
        keyCode: {
            BACKSPACE: 8,
            CAPS_LOCK: 20,
            COMMA: 188,
            CONTROL: 17,
            DELETE: 46,
            DOWN: 40,
            END: 35,
            ENTER: 13,
            ESCAPE: 27,
            HOME: 36,
            INSERT: 45,
            LEFT: 37,
            NUMPAD_ADD: 107,
            NUMPAD_DECIMAL: 110,
            NUMPAD_DIVIDE: 111,
            NUMPAD_ENTER: 108,
            NUMPAD_MULTIPLY: 106,
            NUMPAD_SUBTRACT: 109,
            PAGE_DOWN: 34,
            PAGE_UP: 33,
            PERIOD: 190,
            RIGHT: 39,
            SHIFT: 16,
            SPACE: 32,
            TAB: 9,
            UP: 38
        }
    };
    if (m) {
        var o = l.attr, n = l.fn.removeAttr, q = "http://www.w3.org/2005/07/aaa", j = /^aria-/, k = /^wairole:/;
        l.attr = function (b, a, c) {
            var d = c !== undefined;
            return (a == "role" ? (d ? o.call(this, b, a, "wairole:" + c) : (o.apply(this, arguments) || "").replace(k, "")) : (j.test(a) ? (d ? b.setAttributeNS(q, a.replace(j, "aaa:"), c) : o.call(this, b, a.replace(j, "aaa:"))) : o.apply(this, arguments)))
        };
        l.fn.removeAttr = function (a) {
            return (j.test(a) ? this.each(function () {
                this.removeAttributeNS(q, a.replace(j, ""))
            }) : n.call(this, a))
        }
    }
    l.fn.extend({
        remove: function () {
            l("*", this).add(this).each(function () {
                l(this).triggerHandler("remove")
            });
            return r.apply(this, arguments)
        }, enableSelection: function () {
            return this.attr("unselectable", "off").css("MozUserSelect", "").unbind("selectstart.ui")
        }, disableSelection: function () {
            return this.attr("unselectable", "on").css("MozUserSelect", "none").bind("selectstart.ui", function () {
                return false
            })
        }, scrollParent: function () {
            var a;
            if ((l.browser.msie && (/(static|relative)/).test(this.css("position"))) || (/absolute/).test(this.css("position"))) {
                a = this.parents().filter(function () {
                    return (/(relative|absolute|fixed)/).test(l.curCSS(this, "position", 1)) && (/(auto|scroll)/).test(l.curCSS(this, "overflow", 1) + l.curCSS(this, "overflow-y", 1) + l.curCSS(this, "overflow-x", 1))
                }).eq(0)
            } else {
                a = this.parents().filter(function () {
                    return (/(auto|scroll)/).test(l.curCSS(this, "overflow", 1) + l.curCSS(this, "overflow-y", 1) + l.curCSS(this, "overflow-x", 1))
                }).eq(0)
            }
            return (/fixed/).test(this.css("position")) || !a.length ? l(document) : a
        }
    });
    l.extend(l.expr[":"], {
        data: function (c, b, a) {
            return !!l.data(c, a[3])
        }, focusable: function (b) {
            var c = b.nodeName.toLowerCase(), a = l.attr(b, "tabindex");
            return (/input|select|textarea|button|object/.test(c) ? !b.disabled : "a" == c || "area" == c ? b.href || !isNaN(a) : !isNaN(a)) && !l(b)["area" == c ? "parents" : "closest"](":hidden").length
        }, tabbable: function (b) {
            var a = l.attr(b, "tabindex");
            return (isNaN(a) || a >= 0) && l(b).is(":focusable")
        }
    });
    function p(d, e, f, c) {
        function b(h) {
            var g = l[d][e][h] || [];
            return (typeof g == "string" ? g.split(/,?\s+/) : g)
        }

        var a = b("getter");
        if (c.length == 1 && typeof c[0] == "string") {
            a = a.concat(b("getterSetter"))
        }
        return (l.inArray(f, a) != -1)
    }

    l.widget = function (b, a) {
        var c = b.split(".")[0];
        b = b.split(".")[1];
        l.fn[b] = function (g) {
            var e = (typeof g == "string"), f = Array.prototype.slice.call(arguments, 1);
            if (e && g.substring(0, 1) == "_") {
                return this
            }
            if (e && p(c, b, g, f)) {
                var d = l.data(this[0], b);
                return (d ? d[g].apply(d, f) : undefined)
            }
            return this.each(function () {
                var h = l.data(this, b);
                (!h && !e && l.data(this, b, new l[c][b](this, g))._init());
                (h && e && l.isFunction(h[g]) && h[g].apply(h, f))
            })
        };
        l[c] = l[c] || {};
        l[c][b] = function (f, e) {
            var d = this;
            this.namespace = c;
            this.widgetName = b;
            this.widgetEventPrefix = l[c][b].eventPrefix || b;
            this.widgetBaseClass = c + "-" + b;
            this.options = l.extend({}, l.widget.defaults, l[c][b].defaults, l.metadata && l.metadata.get(f)[b], e);
            this.element = l(f).bind("setData." + b, function (h, g, i) {
                if (h.target == f) {
                    return d._setData(g, i)
                }
            }).bind("getData." + b, function (h, g) {
                if (h.target == f) {
                    return d._getData(g)
                }
            }).bind("remove", function () {
                return d.destroy()
            })
        };
        l[c][b].prototype = l.extend({}, l.widget.prototype, a);
        l[c][b].getterSetter = "option"
    };
    l.widget.prototype = {
        _init: function () {
        }, destroy: function () {
            this.element.removeData(this.widgetName).removeClass(this.widgetBaseClass + "-disabled " + this.namespace + "-state-disabled").removeAttr("aria-disabled")
        }, option: function (c, d) {
            var b = c, a = this;
            if (typeof c == "string") {
                if (d === undefined) {
                    return this._getData(c)
                }
                b = {};
                b[c] = d
            }
            l.each(b, function (e, f) {
                a._setData(e, f)
            })
        }, _getData: function (a) {
            return this.options[a]
        }, _setData: function (a, b) {
            this.options[a] = b;
            if (a == "disabled") {
                this.element[b ? "addClass" : "removeClass"](this.widgetBaseClass + "-disabled " + this.namespace + "-state-disabled").attr("aria-disabled", b)
            }
        }, enable: function () {
            this._setData("disabled", false)
        }, disable: function () {
            this._setData("disabled", true)
        }, _trigger: function (c, d, e) {
            var g = this.options[c], a = (c == this.widgetEventPrefix ? c : this.widgetEventPrefix + c);
            d = l.Event(d);
            d.type = a;
            if (d.originalEvent) {
                for (var b = l.event.props.length, f; b;) {
                    f = l.event.props[--b];
                    d[f] = d.originalEvent[f]
                }
            }
            this.element.trigger(d, e);
            return !(l.isFunction(g) && g.call(this.element[0], d, e) === false || d.isDefaultPrevented())
        }
    };
    l.widget.defaults = {disabled: false};
    l.ui.mouse = {
        _mouseInit: function () {
            var a = this;
            this.element.bind("mousedown." + this.widgetName, function (b) {
                return a._mouseDown(b)
            }).bind("click." + this.widgetName, function (b) {
                if (a._preventClickEvent) {
                    a._preventClickEvent = false;
                    b.stopImmediatePropagation();
                    return false
                }
            });
            if (l.browser.msie) {
                this._mouseUnselectable = this.element.attr("unselectable");
                this.element.attr("unselectable", "on")
            }
            this.started = false
        }, _mouseDestroy: function () {
            this.element.unbind("." + this.widgetName);
            (l.browser.msie && this.element.attr("unselectable", this._mouseUnselectable))
        }, _mouseDown: function (c) {
            c.originalEvent = c.originalEvent || {};
            if (c.originalEvent.mouseHandled) {
                return
            }
            (this._mouseStarted && this._mouseUp(c));
            this._mouseDownEvent = c;
            var b = this, d = (c.which == 1), a = (typeof this.options.cancel == "string" ? l(c.target).parents().add(c.target).filter(this.options.cancel).length : false);
            if (!d || a || !this._mouseCapture(c)) {
                return true
            }
            this.mouseDelayMet = !this.options.delay;
            if (!this.mouseDelayMet) {
                this._mouseDelayTimer = setTimeout(function () {
                    b.mouseDelayMet = true
                }, this.options.delay)
            }
            if (this._mouseDistanceMet(c) && this._mouseDelayMet(c)) {
                this._mouseStarted = (this._mouseStart(c) !== false);
                if (!this._mouseStarted) {
                    c.preventDefault();
                    return true
                }
            }
            this._mouseMoveDelegate = function (e) {
                return b._mouseMove(e)
            };
            this._mouseUpDelegate = function (e) {
                return b._mouseUp(e)
            };
            l(document).bind("mousemove." + this.widgetName, this._mouseMoveDelegate).bind("mouseup." + this.widgetName, this._mouseUpDelegate);
            (l.browser.safari || c.preventDefault());
            c.originalEvent.mouseHandled = true;
            return true
        }, _mouseMove: function (a) {
            if (l.browser.msie && !a.button) {
                return this._mouseUp(a)
            }
            if (this._mouseStarted) {
                this._mouseDrag(a);
                return a.preventDefault()
            }
            if (this._mouseDistanceMet(a) && this._mouseDelayMet(a)) {
                this._mouseStarted = (this._mouseStart(this._mouseDownEvent, a) !== false);
                (this._mouseStarted ? this._mouseDrag(a) : this._mouseUp(a))
            }
            return !this._mouseStarted
        }, _mouseUp: function (a) {
            l(document).unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate);
            if (this._mouseStarted) {
                this._mouseStarted = false;
                this._preventClickEvent = (a.target == this._mouseDownEvent.target);
                this._mouseStop(a)
            }
            return false
        }, _mouseDistanceMet: function (a) {
            return (Math.max(Math.abs(this._mouseDownEvent.pageX - a.pageX), Math.abs(this._mouseDownEvent.pageY - a.pageY)) >= this.options.distance)
        }, _mouseDelayMet: function (a) {
            return this.mouseDelayMet
        }, _mouseStart: function (a) {
        }, _mouseDrag: function (a) {
        }, _mouseStop: function (a) {
        }, _mouseCapture: function (a) {
            return true
        }
    };
    l.ui.mouse.defaults = {cancel: null, distance: 1, delay: 0}
})(jQuery);
(function (b) {
    b.widget("ui.draggable", b.extend({}, b.ui.mouse, {
        _init: function () {
            if (this.options.helper == "original" && !(/^(?:r|a|f)/).test(this.element.css("position"))) {
                this.element[0].style.position = "relative"
            }
            (this.options.addClasses && this.element.addClass("ui-draggable"));
            (this.options.disabled && this.element.addClass("ui-draggable-disabled"));
            this._mouseInit()
        }, destroy: function () {
            if (!this.element.data("draggable")) {
                return
            }
            this.element.removeData("draggable").unbind(".draggable").removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled");
            this._mouseDestroy()
        }, _mouseCapture: function (a) {
            var d = this.options;
            if (this.helper || d.disabled || b(a.target).is(".ui-resizable-handle")) {
                return false
            }
            this.handle = this._getHandle(a);
            if (!this.handle) {
                return false
            }
            return true
        }, _mouseStart: function (a) {
            var d = this.options;
            this.helper = this._createHelper(a);
            this._cacheHelperProportions();
            if (b.ui.ddmanager) {
                b.ui.ddmanager.current = this
            }
            this._cacheMargins();
            this.cssPosition = this.helper.css("position");
            this.scrollParent = this.helper.scrollParent();
            this.offset = this.element.offset();
            this.offset = {top: this.offset.top - this.margins.top, left: this.offset.left - this.margins.left};
            b.extend(this.offset, {
                click: {left: a.pageX - this.offset.left, top: a.pageY - this.offset.top},
                parent: this._getParentOffset(),
                relative: this._getRelativeOffset()
            });
            this.originalPosition = this._generatePosition(a);
            this.originalPageX = a.pageX;
            this.originalPageY = a.pageY;
            if (d.cursorAt) {
                this._adjustOffsetFromHelper(d.cursorAt)
            }
            if (d.containment) {
                this._setContainment()
            }
            this._trigger("start", a);
            this._cacheHelperProportions();
            if (b.ui.ddmanager && !d.dropBehaviour) {
                b.ui.ddmanager.prepareOffsets(this, a)
            }
            this.helper.addClass("ui-draggable-dragging");
            this._mouseDrag(a, true);
            return true
        }, _mouseDrag: function (a, f) {
            this.position = this._generatePosition(a);
            this.positionAbs = this._convertPositionTo("absolute");
            if (!f) {
                var e = this._uiHash();
                this._trigger("drag", a, e);
                this.position = e.position
            }
            if (!this.options.axis || this.options.axis != "y") {
                this.helper[0].style.left = this.position.left + "px"
            }
            if (!this.options.axis || this.options.axis != "x") {
                this.helper[0].style.top = this.position.top + "px"
            }
            if (b.ui.ddmanager) {
                b.ui.ddmanager.drag(this, a)
            }
            return false
        }, _mouseStop: function (e) {
            var f = false;
            if (b.ui.ddmanager && !this.options.dropBehaviour) {
                f = b.ui.ddmanager.drop(this, e)
            }
            if (this.dropped) {
                f = this.dropped;
                this.dropped = false
            }
            if ((this.options.revert == "invalid" && !f) || (this.options.revert == "valid" && f) || this.options.revert === true || (b.isFunction(this.options.revert) && this.options.revert.call(this.element, f))) {
                var a = this;
                b(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function () {
                    a._trigger("stop", e);
                    a._clear()
                })
            } else {
                this._trigger("stop", e);
                this._clear()
            }
            return false
        }, _getHandle: function (a) {
            var d = !this.options.handle || !b(this.options.handle, this.element).length ? true : false;
            b(this.options.handle, this.element).find("*").andSelf().each(function () {
                if (this == a.target) {
                    d = true
                }
            });
            return d
        }, _createHelper: function (e) {
            var f = this.options;
            var a = b.isFunction(f.helper) ? b(f.helper.apply(this.element[0], [e])) : (f.helper == "clone" ? this.element.clone() : this.element);
            if (!a.parents("body").length) {
                a.appendTo((f.appendTo == "parent" ? this.element[0].parentNode : f.appendTo))
            }
            if (a[0] != this.element[0] && !(/(fixed|absolute)/).test(a.css("position"))) {
                a.css("position", "absolute")
            }
            return a
        }, _adjustOffsetFromHelper: function (a) {
            if (a.left != undefined) {
                this.offset.click.left = a.left + this.margins.left
            }
            if (a.right != undefined) {
                this.offset.click.left = this.helperProportions.width - a.right + this.margins.left
            }
            if (a.top != undefined) {
                this.offset.click.top = a.top + this.margins.top
            }
            if (a.bottom != undefined) {
                this.offset.click.top = this.helperProportions.height - a.bottom + this.margins.top
            }
        }, _getParentOffset: function () {
            this.offsetParent = this.helper.offsetParent();
            var a = this.offsetParent.offset();
            if (this.cssPosition == "absolute" && this.scrollParent[0] != document && b.ui.contains(this.scrollParent[0], this.offsetParent[0])) {
                a.left += this.scrollParent.scrollLeft();
                a.top += this.scrollParent.scrollTop()
            }
            if ((this.offsetParent[0] == document.body) || (this.offsetParent[0].tagName && this.offsetParent[0].tagName.toLowerCase() == "html" && b.browser.msie)) {
                a = {top: 0, left: 0}
            }
            return {
                top: a.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
                left: a.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
            }
        }, _getRelativeOffset: function () {
            if (this.cssPosition == "relative") {
                var a = this.element.position();
                return {
                    top: a.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(),
                    left: a.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft()
                }
            } else {
                return {top: 0, left: 0}
            }
        }, _cacheMargins: function () {
            this.margins = {
                left: (parseInt(this.element.css("marginLeft"), 10) || 0),
                top: (parseInt(this.element.css("marginTop"), 10) || 0)
            }
        }, _cacheHelperProportions: function () {
            this.helperProportions = {width: this.helper.outerWidth(), height: this.helper.outerHeight()}
        }, _setContainment: function () {
            var h = this.options;
            if (h.containment == "parent") {
                h.containment = this.helper[0].parentNode
            }
            if (h.containment == "document" || h.containment == "window") {
                this.containment = [0 - this.offset.relative.left - this.offset.parent.left, 0 - this.offset.relative.top - this.offset.parent.top, b(h.containment == "document" ? document : window).width() - this.helperProportions.width - this.margins.left, (b(h.containment == "document" ? document : window).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]
            }
            if (!(/^(document|window|parent)$/).test(h.containment) && h.containment.constructor != Array) {
                var f = b(h.containment)[0];
                if (!f) {
                    return
                }
                var g = b(h.containment).offset();
                var a = (b(f).css("overflow") != "hidden");
                this.containment = [g.left + (parseInt(b(f).css("borderLeftWidth"), 10) || 0) + (parseInt(b(f).css("paddingLeft"), 10) || 0) - this.margins.left, g.top + (parseInt(b(f).css("borderTopWidth"), 10) || 0) + (parseInt(b(f).css("paddingTop"), 10) || 0) - this.margins.top, g.left + (a ? Math.max(f.scrollWidth, f.offsetWidth) : f.offsetWidth) - (parseInt(b(f).css("borderLeftWidth"), 10) || 0) - (parseInt(b(f).css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left, g.top + (a ? Math.max(f.scrollHeight, f.offsetHeight) : f.offsetHeight) - (parseInt(b(f).css("borderTopWidth"), 10) || 0) - (parseInt(b(f).css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top]
            } else {
                if (h.containment.constructor == Array) {
                    this.containment = h.containment
                }
            }
        }, _convertPositionTo: function (j, l) {
            if (!l) {
                l = this.position
            }
            var d = j == "absolute" ? 1 : -1;
            var i = this.options, a = this.cssPosition == "absolute" && !(this.scrollParent[0] != document && b.ui.contains(this.scrollParent[0], this.offsetParent[0])) ? this.offsetParent : this.scrollParent, k = (/(html|body)/i).test(a[0].tagName);
            return {
                top: (l.top + this.offset.relative.top * d + this.offset.parent.top * d - (b.browser.safari && this.cssPosition == "fixed" ? 0 : (this.cssPosition == "fixed" ? -this.scrollParent.scrollTop() : (k ? 0 : a.scrollTop())) * d)),
                left: (l.left + this.offset.relative.left * d + this.offset.parent.left * d - (b.browser.safari && this.cssPosition == "fixed" ? 0 : (this.cssPosition == "fixed" ? -this.scrollParent.scrollLeft() : k ? 0 : a.scrollLeft()) * d))
            }
        }, _generatePosition: function (l) {
            var o = this.options, a = this.cssPosition == "absolute" && !(this.scrollParent[0] != document && b.ui.contains(this.scrollParent[0], this.offsetParent[0])) ? this.offsetParent : this.scrollParent, p = (/(html|body)/i).test(a[0].tagName);
            if (this.cssPosition == "relative" && !(this.scrollParent[0] != document && this.scrollParent[0] != this.offsetParent[0])) {
                this.offset.relative = this._getRelativeOffset()
            }
            var k = l.pageX;
            var j = l.pageY;
            if (this.originalPosition) {
                if (this.containment) {
                    if (l.pageX - this.offset.click.left < this.containment[0]) {
                        k = this.containment[0] + this.offset.click.left
                    }
                    if (l.pageY - this.offset.click.top < this.containment[1]) {
                        j = this.containment[1] + this.offset.click.top
                    }
                    if (l.pageX - this.offset.click.left > this.containment[2]) {
                        k = this.containment[2] + this.offset.click.left
                    }
                    if (l.pageY - this.offset.click.top > this.containment[3]) {
                        j = this.containment[3] + this.offset.click.top
                    }
                }
                if (o.grid) {
                    var n = this.originalPageY + Math.round((j - this.originalPageY) / o.grid[1]) * o.grid[1];
                    j = this.containment ? (!(n - this.offset.click.top < this.containment[1] || n - this.offset.click.top > this.containment[3]) ? n : (!(n - this.offset.click.top < this.containment[1]) ? n - o.grid[1] : n + o.grid[1])) : n;
                    var m = this.originalPageX + Math.round((k - this.originalPageX) / o.grid[0]) * o.grid[0];
                    k = this.containment ? (!(m - this.offset.click.left < this.containment[0] || m - this.offset.click.left > this.containment[2]) ? m : (!(m - this.offset.click.left < this.containment[0]) ? m - o.grid[0] : m + o.grid[0])) : m
                }
            }
            return {
                top: (j - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + (b.browser.safari && this.cssPosition == "fixed" ? 0 : (this.cssPosition == "fixed" ? -this.scrollParent.scrollTop() : (p ? 0 : a.scrollTop())))),
                left: (k - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + (b.browser.safari && this.cssPosition == "fixed" ? 0 : (this.cssPosition == "fixed" ? -this.scrollParent.scrollLeft() : p ? 0 : a.scrollLeft())))
            }
        }, _clear: function () {
            this.helper.removeClass("ui-draggable-dragging");
            if (this.helper[0] != this.element[0] && !this.cancelHelperRemoval) {
                this.helper.remove()
            }
            this.helper = null;
            this.cancelHelperRemoval = false
        }, _trigger: function (a, e, f) {
            f = f || this._uiHash();
            b.ui.plugin.call(this, a, [e, f]);
            if (a == "drag") {
                this.positionAbs = this._convertPositionTo("absolute")
            }
            return b.widget.prototype._trigger.call(this, a, e, f)
        }, plugins: {}, _uiHash: function (a) {
            return {
                helper: this.helper,
                position: this.position,
                absolutePosition: this.positionAbs,
                offset: this.positionAbs
            }
        }
    }));
    b.extend(b.ui.draggable, {
        version: "1.7.2",
        eventPrefix: "drag",
        defaults: {
            addClasses: true,
            appendTo: "parent",
            axis: false,
            cancel: ":input,option",
            connectToSortable: false,
            containment: false,
            cursor: "auto",
            cursorAt: false,
            delay: 0,
            distance: 1,
            grid: false,
            handle: false,
            helper: "original",
            iframeFix: false,
            opacity: false,
            refreshPositions: false,
            revert: false,
            revertDuration: 500,
            scope: "default",
            scroll: true,
            scrollSensitivity: 20,
            scrollSpeed: 20,
            snap: false,
            snapMode: "both",
            snapTolerance: 20,
            stack: false,
            zIndex: false
        }
    });
    b.ui.plugin.add("draggable", "connectToSortable", {
        start: function (g, i) {
            var h = b(this).data("draggable"), j = h.options, a = b.extend({}, i, {item: h.element});
            h.sortables = [];
            b(j.connectToSortable).each(function () {
                var c = b.data(this, "sortable");
                if (c && !c.options.disabled) {
                    h.sortables.push({instance: c, shouldRevert: c.options.revert});
                    c._refreshItems();
                    c._trigger("activate", g, a)
                }
            })
        }, stop: function (f, h) {
            var g = b(this).data("draggable"), a = b.extend({}, h, {item: g.element});
            b.each(g.sortables, function () {
                if (this.instance.isOver) {
                    this.instance.isOver = 0;
                    g.cancelHelperRemoval = true;
                    this.instance.cancelHelperRemoval = false;
                    if (this.shouldRevert) {
                        this.instance.options.revert = true
                    }
                    this.instance._mouseStop(f);
                    this.instance.options.helper = this.instance.options._helper;
                    if (g.options.helper == "original") {
                        this.instance.currentItem.css({top: "auto", left: "auto"})
                    }
                } else {
                    this.instance.cancelHelperRemoval = false;
                    this.instance._trigger("deactivate", f, a)
                }
            })
        }, drag: function (g, j) {
            var i = b(this).data("draggable"), a = this;
            var h = function (e) {
                var s = this.offset.click.top, r = this.offset.click.left;
                var c = this.positionAbs.top, o = this.positionAbs.left;
                var f = e.height, q = e.width;
                var t = e.top, d = e.left;
                return b.ui.isOver(c + s, o + r, t, d, f, q)
            };
            b.each(i.sortables, function (c) {
                this.instance.positionAbs = i.positionAbs;
                this.instance.helperProportions = i.helperProportions;
                this.instance.offset.click = i.offset.click;
                if (this.instance._intersectsWith(this.instance.containerCache)) {
                    if (!this.instance.isOver) {
                        this.instance.isOver = 1;
                        this.instance.currentItem = b(a).clone().appendTo(this.instance.element).data("sortable-item", true);
                        this.instance.options._helper = this.instance.options.helper;
                        this.instance.options.helper = function () {
                            return j.helper[0]
                        };
                        g.target = this.instance.currentItem[0];
                        this.instance._mouseCapture(g, true);
                        this.instance._mouseStart(g, true, true);
                        this.instance.offset.click.top = i.offset.click.top;
                        this.instance.offset.click.left = i.offset.click.left;
                        this.instance.offset.parent.left -= i.offset.parent.left - this.instance.offset.parent.left;
                        this.instance.offset.parent.top -= i.offset.parent.top - this.instance.offset.parent.top;
                        i._trigger("toSortable", g);
                        i.dropped = this.instance.element;
                        i.currentItem = i.element;
                        this.instance.fromOutside = i
                    }
                    if (this.instance.currentItem) {
                        this.instance._mouseDrag(g)
                    }
                } else {
                    if (this.instance.isOver) {
                        this.instance.isOver = 0;
                        this.instance.cancelHelperRemoval = true;
                        this.instance.options.revert = false;
                        this.instance._trigger("out", g, this.instance._uiHash(this.instance));
                        this.instance._mouseStop(g, true);
                        this.instance.options.helper = this.instance.options._helper;
                        this.instance.currentItem.remove();
                        if (this.instance.placeholder) {
                            this.instance.placeholder.remove()
                        }
                        i._trigger("fromSortable", g);
                        i.dropped = false
                    }
                }
            })
        }
    });
    b.ui.plugin.add("draggable", "cursor", {
        start: function (f, g) {
            var a = b("body"), h = b(this).data("draggable").options;
            if (a.css("cursor")) {
                h._cursor = a.css("cursor")
            }
            a.css("cursor", h.cursor)
        }, stop: function (a, e) {
            var f = b(this).data("draggable").options;
            if (f._cursor) {
                b("body").css("cursor", f._cursor)
            }
        }
    });
    b.ui.plugin.add("draggable", "iframeFix", {
        start: function (a, e) {
            var f = b(this).data("draggable").options;
            b(f.iframeFix === true ? "iframe" : f.iframeFix).each(function () {
                b('<div class="ui-draggable-iframeFix" style="background: #fff;"></div>').css({
                    width: this.offsetWidth + "px",
                    height: this.offsetHeight + "px",
                    position: "absolute",
                    opacity: "0.001",
                    zIndex: 1000
                }).css(b(this).offset()).appendTo("body")
            })
        }, stop: function (a, d) {
            b("div.ui-draggable-iframeFix").each(function () {
                this.parentNode.removeChild(this)
            })
        }
    });
    b.ui.plugin.add("draggable", "opacity", {
        start: function (f, g) {
            var a = b(g.helper), h = b(this).data("draggable").options;
            if (a.css("opacity")) {
                h._opacity = a.css("opacity")
            }
            a.css("opacity", h.opacity)
        }, stop: function (a, e) {
            var f = b(this).data("draggable").options;
            if (f._opacity) {
                b(e.helper).css("opacity", f._opacity)
            }
        }
    });
    b.ui.plugin.add("draggable", "scroll", {
        start: function (e, f) {
            var a = b(this).data("draggable");
            if (a.scrollParent[0] != document && a.scrollParent[0].tagName != "HTML") {
                a.overflowOffset = a.scrollParent.offset()
            }
        }, drag: function (h, i) {
            var g = b(this).data("draggable"), j = g.options, a = false;
            if (g.scrollParent[0] != document && g.scrollParent[0].tagName != "HTML") {
                if (!j.axis || j.axis != "x") {
                    if ((g.overflowOffset.top + g.scrollParent[0].offsetHeight) - h.pageY < j.scrollSensitivity) {
                        g.scrollParent[0].scrollTop = a = g.scrollParent[0].scrollTop + j.scrollSpeed
                    } else {
                        if (h.pageY - g.overflowOffset.top < j.scrollSensitivity) {
                            g.scrollParent[0].scrollTop = a = g.scrollParent[0].scrollTop - j.scrollSpeed
                        }
                    }
                }
                if (!j.axis || j.axis != "y") {
                    if ((g.overflowOffset.left + g.scrollParent[0].offsetWidth) - h.pageX < j.scrollSensitivity) {
                        g.scrollParent[0].scrollLeft = a = g.scrollParent[0].scrollLeft + j.scrollSpeed
                    } else {
                        if (h.pageX - g.overflowOffset.left < j.scrollSensitivity) {
                            g.scrollParent[0].scrollLeft = a = g.scrollParent[0].scrollLeft - j.scrollSpeed
                        }
                    }
                }
            } else {
                if (!j.axis || j.axis != "x") {
                    if (h.pageY - b(document).scrollTop() < j.scrollSensitivity) {
                        a = b(document).scrollTop(b(document).scrollTop() - j.scrollSpeed)
                    } else {
                        if (b(window).height() - (h.pageY - b(document).scrollTop()) < j.scrollSensitivity) {
                            a = b(document).scrollTop(b(document).scrollTop() + j.scrollSpeed)
                        }
                    }
                }
                if (!j.axis || j.axis != "y") {
                    if (h.pageX - b(document).scrollLeft() < j.scrollSensitivity) {
                        a = b(document).scrollLeft(b(document).scrollLeft() - j.scrollSpeed)
                    } else {
                        if (b(window).width() - (h.pageX - b(document).scrollLeft()) < j.scrollSensitivity) {
                            a = b(document).scrollLeft(b(document).scrollLeft() + j.scrollSpeed)
                        }
                    }
                }
            }
            if (a !== false && b.ui.ddmanager && !j.dropBehaviour) {
                b.ui.ddmanager.prepareOffsets(g, h)
            }
        }
    });
    b.ui.plugin.add("draggable", "snap", {
        start: function (f, g) {
            var a = b(this).data("draggable"), h = a.options;
            a.snapElements = [];
            b(h.snap.constructor != String ? (h.snap.items || ":data(draggable)") : h.snap).each(function () {
                var d = b(this);
                var c = d.offset();
                if (this != a.element[0]) {
                    a.snapElements.push({
                        item: this,
                        width: d.outerWidth(),
                        height: d.outerHeight(),
                        top: c.top,
                        left: c.left
                    })
                }
            })
        }, drag: function (H, E) {
            var o = b(this).data("draggable"), F = o.options;
            var L = F.snapTolerance;
            var K = E.offset.left, J = K + o.helperProportions.width, l = E.offset.top, i = l + o.helperProportions.height;
            for (var I = o.snapElements.length - 1; I >= 0; I--) {
                var G = o.snapElements[I].left, D = G + o.snapElements[I].width, C = o.snapElements[I].top, a = C + o.snapElements[I].height;
                if (!((G - L < K && K < D + L && C - L < l && l < a + L) || (G - L < K && K < D + L && C - L < i && i < a + L) || (G - L < J && J < D + L && C - L < l && l < a + L) || (G - L < J && J < D + L && C - L < i && i < a + L))) {
                    if (o.snapElements[I].snapping) {
                        (o.options.snap.release && o.options.snap.release.call(o.element, H, b.extend(o._uiHash(), {snapItem: o.snapElements[I].item})))
                    }
                    o.snapElements[I].snapping = false;
                    continue
                }
                if (F.snapMode != "inner") {
                    var d = Math.abs(C - i) <= L;
                    var M = Math.abs(a - l) <= L;
                    var t = Math.abs(G - J) <= L;
                    var B = Math.abs(D - K) <= L;
                    if (d) {
                        E.position.top = o._convertPositionTo("relative", {
                                top: C - o.helperProportions.height,
                                left: 0
                            }).top - o.margins.top
                    }
                    if (M) {
                        E.position.top = o._convertPositionTo("relative", {top: a, left: 0}).top - o.margins.top
                    }
                    if (t) {
                        E.position.left = o._convertPositionTo("relative", {
                                top: 0,
                                left: G - o.helperProportions.width
                            }).left - o.margins.left
                    }
                    if (B) {
                        E.position.left = o._convertPositionTo("relative", {top: 0, left: D}).left - o.margins.left
                    }
                }
                var r = (d || M || t || B);
                if (F.snapMode != "outer") {
                    var d = Math.abs(C - l) <= L;
                    var M = Math.abs(a - i) <= L;
                    var t = Math.abs(G - K) <= L;
                    var B = Math.abs(D - J) <= L;
                    if (d) {
                        E.position.top = o._convertPositionTo("relative", {top: C, left: 0}).top - o.margins.top
                    }
                    if (M) {
                        E.position.top = o._convertPositionTo("relative", {
                                top: a - o.helperProportions.height,
                                left: 0
                            }).top - o.margins.top
                    }
                    if (t) {
                        E.position.left = o._convertPositionTo("relative", {top: 0, left: G}).left - o.margins.left
                    }
                    if (B) {
                        E.position.left = o._convertPositionTo("relative", {
                                top: 0,
                                left: D - o.helperProportions.width
                            }).left - o.margins.left
                    }
                }
                if (!o.snapElements[I].snapping && (d || M || t || B || r)) {
                    (o.options.snap.snap && o.options.snap.snap.call(o.element, H, b.extend(o._uiHash(), {snapItem: o.snapElements[I].item})))
                }
                o.snapElements[I].snapping = (d || M || t || B || r)
            }
        }
    });
    b.ui.plugin.add("draggable", "stack", {
        start: function (a, f) {
            var h = b(this).data("draggable").options;
            var g = b.makeArray(b(h.stack.group)).sort(function (d, c) {
                return (parseInt(b(d).css("zIndex"), 10) || h.stack.min) - (parseInt(b(c).css("zIndex"), 10) || h.stack.min)
            });
            b(g).each(function (c) {
                this.style.zIndex = h.stack.min + c
            });
            this[0].style.zIndex = h.stack.min + g.length
        }
    });
    b.ui.plugin.add("draggable", "zIndex", {
        start: function (f, g) {
            var a = b(g.helper), h = b(this).data("draggable").options;
            if (a.css("zIndex")) {
                h._zIndex = a.css("zIndex")
            }
            a.css("zIndex", h.zIndex)
        }, stop: function (a, e) {
            var f = b(this).data("draggable").options;
            if (f._zIndex) {
                b(e.helper).css("zIndex", f._zIndex)
            }
        }
    })
})(jQuery);
(function (f) {
    var e = {
        dragStart: "start.draggable",
        drag: "drag.draggable",
        dragStop: "stop.draggable",
        maxHeight: "maxHeight.resizable",
        minHeight: "minHeight.resizable",
        maxWidth: "maxWidth.resizable",
        minWidth: "minWidth.resizable",
        resizeStart: "start.resizable",
        resize: "drag.resizable",
        resizeStop: "stop.resizable"
    }, d = "ui-dialog ui-widget ui-widget-content ui-corner-all ";
    f.widget("ui.dialog", {
        _init: function () {
            this.originalTitle = this.element.attr("title");
            var s = this, t = this.options, q = t.title || this.originalTitle || "&nbsp;", b = f.ui.dialog.getTitleId(this.element), r = (this.uiDialog = f("<div/>")).appendTo(document.body).hide().addClass(d + t.dialogClass).css({
                position: "absolute",
                overflow: "hidden",
                zIndex: t.zIndex
            }).attr("tabIndex", -1).css("outline", 0).keydown(function (g) {
                (t.closeOnEscape && g.keyCode && g.keyCode == f.ui.keyCode.ESCAPE && s.close(g))
            }).attr({role: "dialog", "aria-labelledby": b}).mousedown(function (g) {
                s.moveToTop(false, g)
            }), n = this.element.show().removeAttr("title").addClass("ui-dialog-content ui-widget-content").appendTo(r), c = (this.uiDialogTitlebar = f("<div></div>")).addClass("ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix").prependTo(r), p = f('<a href="#"/>').addClass("ui-dialog-titlebar-close ui-corner-all").attr("role", "button").hover(function () {
                p.addClass("ui-state-hover")
            }, function () {
                p.removeClass("ui-state-hover")
            }).focus(function () {
                p.addClass("ui-state-focus")
            }).blur(function () {
                p.removeClass("ui-state-focus")
            }).mousedown(function (g) {
                g.stopPropagation()
            }).click(function (g) {
                s.close(g);
                return false
            }).appendTo(c), o = (this.uiDialogTitlebarCloseText = f("<span/>")).addClass("ui-icon ui-icon-closethick").text(t.closeText).appendTo(p), a = f("<span/>").addClass("ui-dialog-title").attr("id", b).html(q).prependTo(c);
            c.find("*").add(c).disableSelection();
            (t.draggable && f.fn.draggable && this._makeDraggable());
            (t.resizable && f.fn.resizable && this._makeResizable());
            this._createButtons(t.buttons);
            this._isOpen = false;
            (t.bgiframe && f.fn.bgiframe && r.bgiframe());
            (t.autoOpen && this.open())
        }, destroy: function () {
            (this.overlay && this.overlay.destroy());
            this.uiDialog.hide();
            this.element.unbind(".dialog").removeData("dialog").removeClass("ui-dialog-content ui-widget-content").hide().appendTo("body");
            this.uiDialog.remove();
            (this.originalTitle && this.element.attr("title", this.originalTitle))
        }, close: function (c) {
            var a = this;
            if (false === a._trigger("beforeclose", c)) {
                return
            }
            (a.overlay && a.overlay.destroy());
            a.uiDialog.unbind("keypress.ui-dialog");
            (a.options.hide ? a.uiDialog.hide(a.options.hide, function () {
                a._trigger("close", c)
            }) : a.uiDialog.hide() && a._trigger("close", c));
            f.ui.dialog.overlay.resize();
            a._isOpen = false;
            if (a.options.modal) {
                var b = 0;
                f(".ui-dialog").each(function () {
                    if (this != a.uiDialog[0]) {
                        b = Math.max(b, f(this).css("z-index"))
                    }
                });
                f.ui.dialog.maxZ = b
            }
        }, isOpen: function () {
            return this._isOpen
        }, moveToTop: function (c, b) {
            if ((this.options.modal && !c) || (!this.options.stack && !this.options.modal)) {
                return this._trigger("focus", b)
            }
            if (this.options.zIndex > f.ui.dialog.maxZ) {
                f.ui.dialog.maxZ = this.options.zIndex
            }
            (this.overlay && this.overlay.$el.css("z-index", f.ui.dialog.overlay.maxZ = ++f.ui.dialog.maxZ));
            var a = {scrollTop: this.element.attr("scrollTop"), scrollLeft: this.element.attr("scrollLeft")};
            this.uiDialog.css("z-index", ++f.ui.dialog.maxZ);
            this.element.attr(a);
            this._trigger("focus", b)
        }, open: function () {
            if (this._isOpen) {
                return
            }
            var b = this.options, a = this.uiDialog;
            this.overlay = b.modal ? new f.ui.dialog.overlay(this) : null;
            (a.next().length && a.appendTo("body"));
            this._size();
            this._position(b.position);
            a.show(b.show);
            this.moveToTop(true);
            (b.modal && a.bind("keypress.ui-dialog", function (k) {
                if (k.keyCode != f.ui.keyCode.TAB) {
                    return
                }
                var j = f(":tabbable", this), l = j.filter(":first")[0], c = j.filter(":last")[0];
                if (k.target == c && !k.shiftKey) {
                    setTimeout(function () {
                        l.focus()
                    }, 1)
                } else {
                    if (k.target == l && k.shiftKey) {
                        setTimeout(function () {
                            c.focus()
                        }, 1)
                    }
                }
            }));
            f([]).add(a.find(".ui-dialog-content :tabbable:first")).add(a.find(".ui-dialog-buttonpane :tabbable:first")).add(a).filter(":first").focus();
            this._trigger("open");
            this._isOpen = true
        }, _createButtons: function (h) {
            var c = this, a = false, b = f("<div></div>").addClass("ui-dialog-buttonpane ui-widget-content ui-helper-clearfix");
            this.uiDialog.find(".ui-dialog-buttonpane").remove();
            (typeof h == "object" && h !== null && f.each(h, function () {
                return !(a = true)
            }));
            if (a) {
                f.each(h, function (g, j) {
                    f('<button type="button"></button>').addClass("ui-state-default ui-corner-all").text(g).click(function () {
                        j.apply(c.element[0], arguments)
                    }).hover(function () {
                        f(this).addClass("ui-state-hover")
                    }, function () {
                        f(this).removeClass("ui-state-hover")
                    }).focus(function () {
                        f(this).addClass("ui-state-focus")
                    }).blur(function () {
                        f(this).removeClass("ui-state-focus")
                    }).appendTo(b)
                });
                b.appendTo(this.uiDialog)
            }
        }, _makeDraggable: function () {
            var a = this, c = this.options, b;
            this.uiDialog.draggable({
                cancel: ".ui-dialog-content",
                handle: ".ui-dialog-titlebar",
                containment: "document",
                start: function () {
                    b = c.height;
                    f(this).height(f(this).height()).addClass("ui-dialog-dragging");
                    (c.dragStart && c.dragStart.apply(a.element[0], arguments))
                },
                drag: function () {
                    (c.drag && c.drag.apply(a.element[0], arguments))
                },
                stop: function () {
                    f(this).removeClass("ui-dialog-dragging").height(b);
                    (c.dragStop && c.dragStop.apply(a.element[0], arguments));
                    f.ui.dialog.overlay.resize()
                }
            })
        }, _makeResizable: function (h) {
            h = (h === undefined ? this.options.resizable : h);
            var a = this, c = this.options, b = typeof h == "string" ? h : "n,e,s,w,se,sw,ne,nw";
            this.uiDialog.resizable({
                cancel: ".ui-dialog-content",
                alsoResize: this.element,
                maxWidth: c.maxWidth,
                maxHeight: c.maxHeight,
                minWidth: c.minWidth,
                minHeight: c.minHeight,
                start: function () {
                    f(this).addClass("ui-dialog-resizing");
                    (c.resizeStart && c.resizeStart.apply(a.element[0], arguments))
                },
                resize: function () {
                    (c.resize && c.resize.apply(a.element[0], arguments))
                },
                handles: b,
                stop: function () {
                    f(this).removeClass("ui-dialog-resizing");
                    c.height = f(this).height();
                    c.width = f(this).width();
                    (c.resizeStop && c.resizeStop.apply(a.element[0], arguments));
                    f.ui.dialog.overlay.resize()
                }
            }).find(".ui-resizable-se").addClass("ui-icon ui-icon-grip-diagonal-se")
        }, _position: function (l) {
            var b = f(window), c = f(document), j = c.scrollTop(), a = c.scrollLeft(), k = j;
            if (f.inArray(l, ["center", "top", "right", "bottom", "left"]) >= 0) {
                l = [l == "right" || l == "left" ? l : "center", l == "top" || l == "bottom" ? l : "middle"]
            }
            if (l.constructor != Array) {
                l = ["center", "middle"]
            }
            if (l[0].constructor == Number) {
                a += l[0]
            } else {
                switch (l[0]) {
                    case"left":
                        a += 0;
                        break;
                    case"right":
                        a += b.width() - this.uiDialog.outerWidth();
                        break;
                    default:
                    case"center":
                        a += (b.width() - this.uiDialog.outerWidth()) / 2
                }
            }
            if (l[1].constructor == Number) {
                j += l[1]
            } else {
                switch (l[1]) {
                    case"top":
                        j += 0;
                        break;
                    case"bottom":
                        j += b.height() - this.uiDialog.outerHeight();
                        break;
                    default:
                    case"middle":
                        j += (b.height() - this.uiDialog.outerHeight()) / 2
                }
            }
            j = Math.max(j, k);
            this.uiDialog.css({top: j, left: a})
        }, _setData: function (b, c) {
            (e[b] && this.uiDialog.data(e[b], c));
            switch (b) {
                case"buttons":
                    this._createButtons(c);
                    break;
                case"closeText":
                    this.uiDialogTitlebarCloseText.text(c);
                    break;
                case"dialogClass":
                    this.uiDialog.removeClass(this.options.dialogClass).addClass(d + c);
                    break;
                case"draggable":
                    (c ? this._makeDraggable() : this.uiDialog.draggable("destroy"));
                    break;
                case"height":
                    this.uiDialog.height(c);
                    break;
                case"position":
                    this._position(c);
                    break;
                case"resizable":
                    var a = this.uiDialog, h = this.uiDialog.is(":data(resizable)");
                    (h && !c && a.resizable("destroy"));
                    (h && typeof c == "string" && a.resizable("option", "handles", c));
                    (h || this._makeResizable(c));
                    break;
                case"title":
                    f(".ui-dialog-title", this.uiDialogTitlebar).html(c || "&nbsp;");
                    break;
                case"width":
                    this.uiDialog.width(c);
                    break
            }
            f.widget.prototype._setData.apply(this, arguments)
        }, _size: function () {
            var b = this.options;
            this.element.css({height: 0, minHeight: 0, width: "auto"});
            var a = this.uiDialog.css({height: "auto", width: b.width}).height();
            this.element.css({
                minHeight: Math.max(b.minHeight - a, 0),
                height: b.height == "auto" ? "auto" : Math.max(b.height - a, 0)
            })
        }
    });
    f.extend(f.ui.dialog, {
        version: "1.7.2",
        defaults: {
            autoOpen: true,
            bgiframe: false,
            buttons: {},
            closeOnEscape: true,
            closeText: "close",
            dialogClass: "",
            draggable: true,
            hide: null,
            height: "auto",
            maxHeight: false,
            maxWidth: false,
            minHeight: 150,
            minWidth: 150,
            modal: false,
            position: "center",
            resizable: true,
            show: null,
            stack: true,
            title: "",
            width: 300,
            zIndex: 1000
        },
        getter: "isOpen",
        uuid: 0,
        maxZ: 0,
        getTitleId: function (a) {
            return "ui-dialog-title-" + (a.attr("id") || ++this.uuid)
        },
        overlay: function (a) {
            this.$el = f.ui.dialog.overlay.create(a)
        }
    });
    f.extend(f.ui.dialog.overlay, {
        instances: [],
        maxZ: 0,
        events: f.map("focus,mousedown,mouseup,keydown,keypress,click".split(","), function (a) {
            return a + ".dialog-overlay"
        }).join(" "),
        create: function (b) {
            if (this.instances.length === 0) {
                setTimeout(function () {
                    if (f.ui.dialog.overlay.instances.length) {
                        f(document).bind(f.ui.dialog.overlay.events, function (c) {
                            var h = f(c.target).parents(".ui-dialog").css("zIndex") || 0;
                            return (h > f.ui.dialog.overlay.maxZ)
                        })
                    }
                }, 1);
                f(document).bind("keydown.dialog-overlay", function (c) {
                    (b.options.closeOnEscape && c.keyCode && c.keyCode == f.ui.keyCode.ESCAPE && b.close(c))
                });
                f(window).bind("resize.dialog-overlay", f.ui.dialog.overlay.resize)
            }
            var a = f("<div></div>").appendTo(document.body).addClass("ui-widget-overlay").css({
                width: this.width(),
                height: this.height()
            });
            (b.options.bgiframe && f.fn.bgiframe && a.bgiframe());
            this.instances.push(a);
            return a
        },
        destroy: function (a) {
            this.instances.splice(f.inArray(this.instances, a), 1);
            if (this.instances.length === 0) {
                f([document, window]).unbind(".dialog-overlay")
            }
            a.remove();
            var b = 0;
            f.each(this.instances, function () {
                b = Math.max(b, this.css("z-index"))
            });
            this.maxZ = b
        },
        height: function () {
            if (f.browser.msie && f.browser.version < 7) {
                var b = Math.max(document.documentElement.scrollHeight, document.body.scrollHeight);
                var a = Math.max(document.documentElement.offsetHeight, document.body.offsetHeight);
                if (b < a) {
                    return f(window).height() + "px"
                } else {
                    return b + "px"
                }
            } else {
                return f(document).height() + "px"
            }
        },
        width: function () {
            if (f.browser.msie && f.browser.version < 7) {
                var a = Math.max(document.documentElement.scrollWidth, document.body.scrollWidth);
                var b = Math.max(document.documentElement.offsetWidth, document.body.offsetWidth);
                if (a < b) {
                    return f(window).width() + "px"
                } else {
                    return a + "px"
                }
            } else {
                return f(document).width() + "px"
            }
        },
        resize: function () {
            var a = f([]);
            f.each(f.ui.dialog.overlay.instances, function () {
                a = a.add(this)
            });
            a.css({width: 0, height: 0}).css({width: f.ui.dialog.overlay.width(), height: f.ui.dialog.overlay.height()})
        }
    });
    f.extend(f.ui.dialog.overlay.prototype, {
        destroy: function () {
            f.ui.dialog.overlay.destroy(this.$el)
        }
    })
})(jQuery);
var popup, displayBoxPos, isWorking, divobj, req;
isWorking = false;
function GetPopupDivByName() {
    var a = document.getElementById("popupplaceholder");
    if (a == null) {
        a = document.body.insertBefore(document.createElement("div"), document.body.firstChild);
        a.style.position = "absolute";
        a.style.display = "block";
        a.style.zIndex = 100;
        a.id = "popupplaceholder"
    }
    return a
}
function CallbackReload(b) {
    if (isWorking) {
        return
    }
    isWorking = true;
    var a = b.indexOf("/asp/");
    if (a > 0) {
        b = b.substring(a)
    }
    req = com.art.core.utils.BrowserUtil.createXMLHTTPObject();
    req.open("GET", b + "&rnd=" + Math.random(), true);
    req.onreadystatechange = ReloadPage;
    req.send(null);
    return true
}
function ReloadPage() {
    if (req.readyState == 4) {
        var b = window.location.toString();
        var a = b.length;
        if (b.substring(a - 1) == "#") {
            b = b.substring(0, a - 1)
        }
        window.location = b
    }
}
function PopupCallback(b, a) {
    if (isWorking) {
        return
    }
    isWorking = true;
    req = com.art.core.utils.BrowserUtil.createXMLHTTPObject();
    var c = "/asp/pop_up/popup_callback.asp?tmpl=" + b + a + "&rnd=" + Math.random();
    req.open("GET", c, true);
    req.onreadystatechange = CallbackResponse;
    req.send(null);
    return true
}
function CallbackResponse() {
    if (req.readyState == 4) {
        var response = eval("(" + req.responseText + ")");
        var template = response.jsonRecord.Template.replace(/&quot;/g, '"');
        popup = GetPopupDivByName();
        if (popup != null) {
            popup.innerHTML = template;
            popup.style.top = (displayBoxPos[1] + 70) + "px";
            popup.style.left = (displayBoxPos[0] - 70) + "px";
            if (displayBoxPos[0] < 100) {
                popup.style.left = "100px"
            }
        }
        isWorking = false;
        divobj.style.cursor = "default";
        setFocus("emailaddress");
        setFocus("password")
    }
}
function setFocus(a) {
    var b = document.getElementById(a);
    if (b) {
        b.focus()
    }
}
function findElementPos(c) {
    var a = 0;
    var b = 0;
    if (c.offsetLeft == 0 || c.offsetTop == 0) {
        if (c.offsetParent) {
            a = c.offsetLeft;
            b = c.offsetTop;
            while (c = c.offsetParent) {
                a += c.offsetLeft;
                b += c.offsetTop
            }
        }
    } else {
        a = c.offsetLeft;
        b = c.offsetTop
    }
    return [a, b]
}
var XMLHttpFactories = [function () {
    return new XMLHttpRequest()
}, function () {
    return new ActiveXObject("Msxml2.XMLHTTP")
}, function () {
    return new ActiveXObject("Msxml3.XMLHTTP")
}, function () {
    return new ActiveXObject("Microsoft.XMLHTTP")
}];
var browser = new com.art.core.utils.BrowserUtil.Browser();
var dragObj = new Object();
dragObj.zIndex = 0;
var x, y;
function dragStart(b, c) {
    var a;
    if (c) {
        dragObj.elNode = document.getElementById(c)
    } else {
        if (browser.isIE) {
            dragObj.elNode = window.event.srcElement
        }
        if (browser.isNS) {
            dragObj.elNode = b.target
        }
        if (dragObj.elNode.nodeType == 3) {
            dragObj.elNode = dragObj.elNode.parentNode
        }
    }
    findMousePos(b);
    dragObj.cursorStartX = x;
    dragObj.cursorStartY = y;
    dragObj.elStartLeft = parseInt(dragObj.elNode.style.left, 10);
    dragObj.elStartTop = parseInt(dragObj.elNode.style.top, 10);
    if (isNaN(dragObj.elStartLeft)) {
        dragObj.elStartLeft = 0
    }
    if (isNaN(dragObj.elStartTop)) {
        dragObj.elStartTop = 0
    }
    dragObj.elNode.style.zIndex = ++dragObj.zIndex;
    if (browser.isIE) {
        document.attachEvent("onmousemove", dragGo);
        document.attachEvent("onmouseup", dragStop);
        window.event.cancelBubble = true;
        window.event.returnValue = false
    }
    if (browser.isNS) {
        document.addEventListener("mousemove", dragGo, true);
        document.addEventListener("mouseup", dragStop, true);
        b.preventDefault()
    }
}
function dragGo(a) {
    findMousePos(a);
    dragObj.elNode.style.left = (dragObj.elStartLeft + x - dragObj.cursorStartX) + "px";
    dragObj.elNode.style.top = (dragObj.elStartTop + y - dragObj.cursorStartY) + "px";
    if (browser.isIE) {
        window.event.cancelBubble = true;
        window.event.returnValue = false
    }
    if (browser.isNS) {
        a.preventDefault()
    }
}
function dragStop(a) {
    dragObj.elNode = null;
    if (browser.isIE) {
        document.detachEvent("onmousemove", dragGo);
        document.detachEvent("onmouseup", dragStop)
    }
    if (browser.isNS) {
        document.removeEventListener("mousemove", dragGo, true);
        document.removeEventListener("mouseup", dragStop, true)
    }
}
function findMousePos(a) {
    if (browser.isIE) {
        x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
        y = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop
    }
    if (browser.isNS) {
        x = a.clientX + window.scrollX;
        y = a.clientY + window.scrollY
    }
};
function SifPop(b, a, c, d) {
    this.divobj = b;
    b.style.cursor = "wait";
    displayBoxPos = findElementPos(document.getElementById(a));
    PopupCallback("SeeItFramed", "&pid=" + c + "&spid=" + d)
}
function CartPop(c, b, d, f, a, e) {
    var g = c.title;
    this.divobj = c;
    c.style.cursor = "wait";
    displayBoxPos = findElementPos(document.getElementById(b));
    PopupCallback("AddToCart", "&pid=" + d + "&spid=" + f + "&cid=" + a + "&scid=" + escape(e))
}
function GalPop(b, a, c, d) {
    this.divobj = b;
    b.style.cursor = "wait";
    displayBoxPos = findElementPos(document.getElementById(a));
    PopupCallback("AddItem", "&disp=Gallery&pid=" + c + "&spid=" + d + "&title=" + GetTitle(b))
}
function WLPop(b, a, c, d) {
    this.divobj = b;
    b.style.cursor = "wait";
    displayBoxPos = findElementPos(document.getElementById(a));
    PopupCallback("AddItem", "&disp=Wish%20List&pid=" + c + "&spid=" + d + "&title=" + GetTitle(b))
}
function GetTitle(a) {
    var b = a.title;
    if (b.startsWith("Add ")) {
        b = b.substring(4)
    }
    if (b.endsWith(" to Gallery")) {
        b = b.substring(0, b.length - 11)
    }
    if (b.endsWith(" to Wishlist")) {
        b = b.substring(0, b.length - 12)
    }
    return b
}
String.prototype.startsWith = function (a) {
    return (this.match("^" + a) == a)
};
String.prototype.endsWith = function (a) {
    return (this.match(a + "$") == a)
};
function doGalleryPopUP(c, b, g, d, f, a, e) {
    this.divobj = c;
    c.style.cursor = "wait";
    displayBoxPos = findElementPos(document.getElementById(b));
    PopupCallback("AddItem", "&disp=Gallery&pid=" + d + "&spid=" + f + "&apnum=" + a + "&podconfigid=" + e + "&title=" + g)
}
function doWLPopUP(c, b, g, d, f, a, e) {
    this.divobj = c;
    c.style.cursor = "wait";
    displayBoxPos = findElementPos(document.getElementById(b));
    PopupCallback("AddItem", "&disp=Wish%20List&pid=" + d + "&spid=" + f + "&apnum=" + a + "&podconfigid=" + e + "&title=" + g)
}
function doDeleteItem(a, b, g, h, c, e, d, f) {
    this.divobj = a;
    a.style.cursor = "wait";
    findMousePos(b);
    displayBoxPos = [x + g, y + h];
    f = replaceDashDash(f);
    PopupCallback("DeleteItem", "&gname=" + c + "&gtype=" + d + "&title=" + e + "&url=" + escape(f))
}
function doMoveItem(b, c, j, k, g, d, f, h, e, a) {
    this.divobj = b;
    b.style.cursor = "wait";
    findMousePos(c);
    displayBoxPos = [x + j, y + k];
    PopupCallback("MoveItem", "&oid=" + g + "&galleryid=" + d + "&gt=" + f + "&startAt=" + h + "&grid=" + e + "&count=" + a)
}
function doDeleteWLLising(a, b, f, g, d, e, c) {
    this.divobj = a;
    a.style.cursor = "wait";
    findMousePos(b);
    displayBoxPos = [x + f, y + g];
    e = replaceDashDash(e);
    PopupCallback("DeleteWLListing", "&gtype=" + d + "&gname=" + c + "&url=" + escape(e))
}
function replaceDashDash(a) {
    return a = a.replace(/--/g, "@dashdash")
}
function getRadioButtonValue(a) {
    for (i = 0; i < a.length; i++) {
        if (a[i].checked) {
            return a[i].value
        }
    }
    return "not found"
}
function lightencryption(f) {
    var a = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var c = "";
    for (var b = 0; b < f.length; b++) {
        var e = Math.floor(Math.random() * a.length);
        c += f.substring(b, b + 1) + a.substring(e, e + 1)
    }
    var d = c.length / 2;
    return "&dw=" + escape(reverseString(c.substring(d, 2 * d))) + "&sp=" + escape(reverseString(c.substring(0, d)))
}
function reverseString(c) {
    c = c.replace(/%/g, "@(percent)");
    var a = c.length;
    var b = "";
    for (a; a > 0; a--) {
        b += c.substring(a - 1, a)
    }
    return b
}
function replaceSingleQuote(a) {
    return a.replace(/'/g, "@(singlequote)")
}
function removeSingleQuote(a) {
    return a.replace(/'/g, "")
}
function checkFavorites(d) {
    var a = document.getElementById("createLabel");
    var c = document.getElementById("createText");
    var b = document.getElementById("createRadio");
    if (b.checked) {
        a.innerHTML = "<br />e.g. Favorites, Living Room, For Mom <br />";
        c.style.display = "block"
    } else {
        a.innerHTML = "&nbsp;<br />Create a New " + d + "<br /><br />";
        c.style.display = "none"
    }
    document.getElementById("error1").style.display = "none";
    document.getElementById("error2").style.display = "none"
}
function HighlightRow(b) {
    var c = document.getElementsByName("radiobutton");
    if (c) {
        for (var a = 0; a < c.length; a++) {
            c[a].parentNode.parentNode.className = ""
        }
    }
    b.parentNode.parentNode.className = "pop_bldtext"
}
function launchStudio(c, e) {
    var b = document.getElementsByName("radiobutton");
    if (b) {
        for (var a = 0; a < b.length; a++) {
            if (b[a].checked) {
                var d = (b[a].value).split(":");
                GotoStudio(d[0], d[1], c, e)
            }
        }
    }
}
function launchCart(e, g, a, d) {
    var c = document.getElementsByName("radiobutton");
    if (c) {
        for (var b = 0; b < c.length; b++) {
            if (c[b].checked) {
                var f = (c[b].value).split(":");
                GotoCart(f[0], f[1], e, g, a, d, f[2])
            }
        }
    }
}
function GotoCart(c, g, e, b, a, f, d) {
    window.location = "/asp/place_order-asp/_/PDTA--" + c + "/SP--" + g + "/ID--" + a + "/posters.htm?Add_to_Cart=Y&XHN=2" + f + "&ui=" + e + "&PODConfigID=" + d
};
var slash = "/";
var selectedSize;
var ImagePath;
var ImageHeight;
var ImageWidth;
var arrImage;
var arrImageDimensions;
var selectedAPNum;
var strAPNumToBeReplaced;
var strProducttype;
var arrPODAttributes;
var bApplyShadow;
var customerZoneID;
var ISOCurrency;
var DLEPODConfigID = 0;
var DLEcanFrame;
var frameStepURL;
var cartPageURL;
var wt_Enabled = "";
var setZoomWindow = true;
var zoomSetAttempt = 0;
var cookie = new com.art.core.cookie.Cookie();
$(document).ready(function () {
    wt_Enabled = $("#wtEnabled").text();
    GetZoneAndCurrency();
    initJQZoom();
    initTabs();
    initRecommendationTabs();
    initProductPageButtons();
    setupCustomerReviews();
    var showPLAModule = $("#showPLA").val();
    if (showPLAModule != "undefined" && showPLAModule != "" && showPLAModule.toLowerCase() == "true") {
        setupPLAModule()
    }
    else {
        if (com.art.core.utils.BrowserUtil.getQueryString("mg") == "rv" || com.art.core.utils.BrowserUtil.getQueryString("mg") == "n") {
            setupBreadCrumb()
        }
    }
    executeCurrentPriceWithBelowLabelChangesWide();
    DLEPODConfigID = $(".moreSizesDD_Row_selected").find("#moreSizesDD_selected_PODConfigID").text();
    if (DLEPODConfigID != "") {
        var varDLEAPNUMOnLoad = $(".moreSizesDD_Row_selected").find("#moreSizesDD_selected_APNum").text();
        DLEcanFrame = $(".moreSizesDD_Row_selected").find("#moreSizesDD_selected_ItemFrame").text();
        setDLEPODParameters(DLEPODConfigID, varDLEAPNUMOnLoad, DLEcanFrame, false, $(".moreSizesDD_Row_selected").find("#ItemDisplayedTypeText").text())
    }
    $(document).on("mouseup", "#MainImage, .zoomPup", function (e) {
        eval($("#ZoomLink").attr("href"))
    });
    $(document).on("mouseover", ".moreSizesDD_Container_DLE, .moreSizesDD_Row, .moreSizesDD_Row_selected", function (e) {
        $(this).children().find(".product-filter-icon").removeClass("product-filter-icon").addClass("product-filter-selected")
    });
    $(document).on("mouseout", ".moreSizesDD_Container_DLE, .moreSizesDD_Row", function (e) {
        $(this).children().find(".product-filter-selected").removeClass("product-filter-selected").addClass("product-filter-icon")
    });
    $(document).on("click", ".moreSizesDD_Container_DLE", function (e) {
        var url = $(this).attr("pgurl");
        if (com.art.core.utils.BrowserUtil.getQueryString("mg") == "y") {
            if ($(".mygalmoveoptions").css("display") != "none") {
                url += "&mg=y&chgsz=y";
                url += "&gid=" + com.art.core.utils.BrowserUtil.getQueryString("gid");
                url += "&itemid=" + com.art.core.utils.BrowserUtil.getQueryString("itemid");
                url += "&apnum=" + com.art.core.utils.BrowserUtil.getQueryString("apnum");
                url += "&PODConfigID=" + com.art.core.utils.BrowserUtil.getQueryString("PODConfigID")
            }
        }
        try {
            localStorage.setItem("roomView_preserveDLE", "true")
        } catch (e) {
        }
        location.href = url
    });
    $(document).on("click", ".moreSizesDD_Row", function (e) {
        DisplayMainAndZoomImage(currentRow);
        ApplyShadowToMainImage(canvasStretch, bApplyShadow, customerZoneID);
        PopulateSelectedSizeAttributes(currentRow);
    });
    function zoom() {
        if ($("#ZoomLink").length > 0) {
            var url = $("#zoomURL").text();
            var posMXW = url.indexOf("MXW");
            var posMXH = url.indexOf("MXH");
            hiResHref = $("#MainImage").attr("src");
            var pos = hiResHref.indexOf("MXW");
            if (pos > 1) {
                hiResHref = hiResHref.replace(hiResHref.substr(pos, hiResHref.indexOf("MXH") - pos), url.substr(url.indexOf("MXW"), url.indexOf("MXH") - url.indexOf("MXW")));
                pos = hiResHref.indexOf("MXH");
                hiResHref = hiResHref.replace(hiResHref.substr(pos, hiResHref.indexOf("QLT") - pos), url.substr(url.indexOf("MXH"), url.indexOf("QLT") - url.indexOf("MXH")))
            }
            hiResHref = hiResHref.replace("watermarker/-", "watermarker/");
            replaceProductImage($("#MainImage").attr("src"), hiResHref)
        }
    }

    $(document).on("click", "#prd-add-to-cart", function (e) {
        var ReplacedCartHTML = "";
        var CartLink = $("#hdAddToCart").html();
        var selectedPODConfigID = $(".moreSizesDD_Row_selected").find("#moreSizesDD_selected_PODConfigID").text();
        var strZoneProductID = $(".moreSizesDD_Row_selected").find("#moreSizesDD_selected_zoneProductID").text();
        CartLink = CartLink.replace(/&amp;/g, "&");
        if ($.trim(selectedPODConfigID) == "") {
            ReplacedCartHTML = CartLink
        } else {
            if ($.trim(selectedPODConfigID) == "") {
                selectedPODConfigID = 0
            }
            var selectedSpecID = "sp--" + strZoneProductID.substr(strZoneProductID.length - 1, strZoneProductID.length);
            var selectedProductID = "pdta--" + strZoneProductID.substr(0, strZoneProductID.length - 1);
            var strPODConfigID = "PODConfigID=" + selectedPODConfigID;
            var strProductIDToBeReplaced = GetParameterToBeReplaced(CartLink, "pdta--", "/");
            var strSpecIDToBeReplaced = GetParameterToBeReplaced(CartLink, "sp--", "/");
            var strPODConfigIDToBeReplaced = GetParameterToBeReplaced(CartLink, "podconfigid=", "&");
            ReplacedCartHTML = replaceString(replaceString(replaceString(CartLink, strPODConfigIDToBeReplaced, strPODConfigID), strProductIDToBeReplaced, selectedProductID), strSpecIDToBeReplaced, selectedSpecID)
        }
        if ($("#hdAddToCartFrameStep").length <= 0 || $("#prd-is-frameit").text().toLowerCase() == "hiddenclass" || $(".cf_framecontainer").length <= 0) {
            linkForm(ReplacedCartHTML)
        } else {
            cartPageURL = ReplacedCartHTML;
            ReplacedCartHTML = ReplacedCartHTML.replace("place_order.asp", "include/addtocart.asp");
            frameStepURL = $("#hdAddToCartFrameStep").text();
            var cartIID = com.art.core.utils.BrowserUtil.getQueryString("iid");
            if ($.trim(selectedPODConfigID) == "") {
                makeAjaxCallToAddToCartProductPage(ReplacedCartHTML, cartIID)
            } else {
                if ($.trim(selectedPODConfigID) == "") {
                    selectedPODConfigID = 0
                }
                var selectedRow, PODConfigID, APNum;
                selectedRow = $(".moreSizesDD_Row_selected");
                if (selectedRow.html() == null) {
                    PODConfigID = $("#hdItemPODConfigID").text();
                    APNum = $("#hdItemAPNum").text()
                } else {
                    PODConfigID = selectedRow.find("#moreSizesDD_selected_PODConfigID").text();
                    APNum = selectedRow.find("#moreSizesDD_selected_APNum").text()
                }
                productID = $(".moreSizesDD_Row_selected").find("#moreSizesDD_selected_zoneProductID").text();
                specID = $("#hdItemSpecID").text();
                productID = productID.split(specID);
                productID = productID[0];
                frameStepURL = "/frameStep/?Add_to_Cart=Y&pd=" + productID + "&sp=" + specID + "&APNum=" + APNum + "&PODConfigID=" + PODConfigID;
                makeAjaxCallToAddToCartProductPage(ReplacedCartHTML, cartIID)
            }
        }
        if (wt_Enabled == "true") {
            if ($("#frameSku").text() == "") {
            }
        }
    });
    if (com.art.core.utils.BrowserUtil.isiOSDevice()) {
        $(".frame_it_bttn").removeAttr("onclick")
    }
    $(".frame_it_bttn").bind("click", function (event) {
        if (com.art.core.utils.BrowserUtil.isiOSDevice()) {
            event.preventDefault();
            if ($("#myImageFlowParent").length > 0) {
                $("html, body").animate({scrollTop: $("#myImageFlowParent").offset().top - 10}, 500)
            }
        } else {
            return true
        }
    });
    $(document).on("click", "#dfe-as-popup-header", function () {
        $("#dfeModule").removeClass("dfe-as-popup");
        $("#dfe-as-popup-header").remove();
        $("#dfeModule").find(".moduleHeader").show();
        $("#dfe-modal").remove()
    });
    $(document).on("click", ".moreSizesDD_Row_selected", function (e) {
        var moreCustomOptions = $("#moreCustom1");
        if (moreCustomOptions.attr("class") != "") {
            moreCustomOptions.attr("style", "")
        }
    });
    $(document).on("click", "#linkToEmailFriend", function (e) {
        var selectedPODConfigID, selectedRow, selectedZoneProductID, selectedProductID, ReplacedPODConfigIDLink, selectedSpecID;
        var strProductIDToBeReplaced, strSpecIDToBeReplaced, strPODConfigIDToBeReplaced;
        var EmailPopUpLink = $("#hdmailaFriendURL").text();
        selectedRow = $(".moreSizesDD_Row_selected");
        selectedPODConfigID = selectedRow.find("#moreSizesDD_selected_PODConfigID").text();
        if (selectedPODConfigID != "") {
            strProductIDToBeReplaced = GetParameterToBeReplaced(EmailPopUpLink, "pd--", "/");
            strSpecIDToBeReplaced = GetParameterToBeReplaced(EmailPopUpLink, "sp--", "/");
            strPODConfigIDToBeReplaced = GetParameterToBeReplaced(EmailPopUpLink, "PODConfigID=", "&");
            selectedZoneProductID = selectedRow.find("#moreSizesDD_selected_zoneProductID").text();
            selectedSpecID = "sp--" + selectedZoneProductID.substr(selectedZoneProductID.length - 1, selectedZoneProductID.length);
            selectedProductID = "pd--" + selectedZoneProductID.substr(0, selectedZoneProductID.length - 1);
            selectedPODConfigID = "PODConfigID=" + selectedPODConfigID;
            ReplacedPODConfigIDLink = replaceString(replaceString(replaceString(EmailPopUpLink, strPODConfigIDToBeReplaced, selectedPODConfigID), strProductIDToBeReplaced, selectedProductID), strSpecIDToBeReplaced, selectedSpecID)
        } else {
            ReplacedPODConfigIDLink = EmailPopUpLink
        }
        eval(ReplacedPODConfigIDLink)
    });
    var $includedText = $("#AdditionalText");
    includedText = $includedText.text().toLowerCase();
    if (includedText.indexOf("rolling stone") > 1 && includedText.indexOf("subscription") > 1) {
        var rsClickEvent = "RollingStoneDetailsWindow('12','$6.48')";
        rollingStoneOfferLink = '<div><a href="#" onclick="' + rsClickEvent + '">Offer and rebate details</a></div>';
        $includedText.append(rollingStoneOfferLink)
    }
    function DisplayMainAndZoomImage(currentRow) {
        ImagePath = currentRow.find("#moreSizesDD_selected_ImageFileAttr").html();
        canCanvasText = currentRow.find("#moreSizesDD_selected_canCanvasStretch").text();
        canCanvas = false;
        if (canCanvasText != "") {
            canCanvasText = canCanvasText.toUpperCase();
            if (canCanvasText == "TRUE") {
                canCanvas = true
            }
        }
        arrImage = currentRow.find("#moreSizesDD_selected_ImageFileAttr").html().split("@");
        arrImageDimensions = arrImage[1].split("_");
        ImageWidth = arrImageDimensions[0];
        ImageHeight = arrImageDimensions[1];
        $("#MainImage").attr("height", ImageHeight);
        $("#MainImage").attr("width", ImageWidth);
        var smallImage = arrImage[0].replace(/&amp;/g, "&");
        if (canCanvas && smallImage.toLowerCase().indexOf("/crop/") > 0) {
            var zw, zh;
            if (typeof window.innerWidth != "undefined") {
                zw = window.innerWidth * 0.92;
                zh = window.innerHeight * 0.92
            } else {
                if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
                    zw = document.documentElement.clientWidth * 0.92;
                    zh = document.documentElement.clientHeight * 0.92
                } else {
                    zw = $(window).width() * 0.92;
                    zh = $(window).height() * 0.92
                }
            }
            var bigImage = smallImage.substring(0, smallImage.indexOf("&maxw="));
            bigImage = bigImage + "&maxw=" + Math.round(zw) + "&maxh=" + Math.round(zh);
            replaceProductImage(smallImage, bigImage)
        } else {
            if ($("#ZoomLink").length > 0) {
                var hiResHref = $("#zoomURL").text();
                if (useSuperZoom) {
                    var key = "watermarker/";
                    var u = hiResHref;
                    var u2 = (u.indexOf(key) > -1) ? u.substr(u.indexOf(key) + key.length) : u;
                    hiResHref = getCropperUrl(u2)
                }
                hiResHref = hiResHref.replace("watermarker/-", "watermarker/");
                replaceProductImage(arrImage[0].replace(/&amp;/g, "&"), hiResHref)
            } else {
                $("#MainImage").attr("src", arrImage[0].replace(/&amp;/g, "&"))
            }
        }
        $("#ZoomLink").attr("href", arrImage[2]);
        formatMainImage()
    }

    function applySelectedRowStyle(currentRow, previousSelectedRow) {
        currentRow.find(".moreSizesDD_arrow #imgIndicator").attr("src", "http://cache1.artprintimages.com/images/pub/productPage/right_arrow.gif");
        previousSelectedRow.find(".moreSizesDD_arrow #imgIndicator").attr("src", "http://cache1.artprintimages.com/images/misc/blank.gif");
        previousSelectedRow.addClass("moreSizesDD_Row").removeClass("moreSizesDD_Row_selected");
        currentRow.addClass("moreSizesDD_Row_selected").removeClass("moreSizesDD_Row");
        $(".moreSizesDD_selected_Cell").addClass("moreSizesDD_Cell").removeClass("moreSizesDD_selected_Cell");
        currentRow.find(".moreSizesDD_Cell").addClass("moreSizesDD_selected_Cell").removeClass("moreSizesDD_Cell");
        $(".moreSizesDD_selected_Price").addClass("moreSizesDD_price").removeClass("moreSizesDD_selected_Price");
        currentRow.find(".moreSizesDD_price").addClass("moreSizesDD_selected_Price").removeClass("moreSizesDD_price");
        $(".moreSizesDD_selected_ships_Cell").addClass("moreSizesDD_ships_Cell").removeClass("moreSizesDD_selected_ships_Cell");
        currentRow.find(".moreSizesDD_ships_Cell").addClass("moreSizesDD_selected_ships_Cell").removeClass("moreSizesDD_ships_Cell");
        $(".moreSizesDD_Row").children().find(".product-filter-selected").removeClass("product-filter-selected").addClass("product-filter-icon");
        $(".moreSizesDD_Container_DLE").children().find(".product-filter-selected").removeClass("product-filter-selected").addClass("product-filter-icon")
    }

    function DisplayPrice(sPriceDetails) {
        var aPriceDetails, PriceTitle, PriceValue;
        aPriceDetails = sPriceDetails.split("@");
        PriceTitle = aPriceDetails[0];
        PriceValue = aPriceDetails[1];
        $("#prd-price-titles").html(PriceTitle);
        $("#prd-price-value").html(PriceValue)
    }

    function GetZoneAndCurrency() {
        var LocDetails;
        LocDetails = ($("#hdLocalizationDetails").text().split("@"));
        customerZoneID = LocDetails[0];
        ISOCurrency = LocDetails[1]
    }

    function DisplayOtherServiceOptions(APNum, PODConfigID, ZoneProductID) {
        var output;
        $.ajax({
            type: "POST",
            url: "/asp/include/ValueAddedServicesHelper.asp",
            ContentType: "application/x-www-form-urlencoded",
            data: "PODAPNum=" + APNum + "&PODItemConfigAPNum=" + PODConfigID + "&ZoneID=" + customerZoneID + "&ISOCurrencyCode=" + ISOCurrency + "&isAjax=Y",
            async: false,
            success: function (msg) {
                output = msg
            }
        });
        if ($.trim(output) != "") {
            FetchServiceOptionValues(output);
            ReplacePODConfigZoneProductIDInServiceURL(PODConfigID, ZoneProductID)
        }
    }

    function FetchServiceOptionValues(output) {
        var serviceOptions, canCanvasTransfer, canMount, canLaminate, aServiceOptions;
        serviceOptions = output;
        aServiceOptions = serviceOptions.split("@");
        canCanvasTransfer = aServiceOptions[0];
        canMount = aServiceOptions[1];
        canLaminate = aServiceOptions[2];
        showHideServiceOptions(canCanvasTransfer, canMount, canLaminate)
    }

    function showHideServiceOptions(canCanvasTransfer, canMount, canLaminate) {
        if (canCanvasTransfer > 0 || canMount > 0 || canLaminate > 0) {
            $("#prd-additional-services").show()
        } else {
            $("#prd-additional-services").hide()
        }
        if (canCanvasTransfer == 0) {
            $("#divImgCanvasTransfer").removeClass("displayClass").addClass("hiddenClass");
            $("#divImgCanvasTransfer").children().removeClass("displayClass").addClass("hiddenClass");
            $("#divImgCanvasTransfer").find("*").removeClass("displayClass").addClass("hiddenClass")
        } else {
            $("#divImgCanvasTransfer").removeClass("hiddenClass").addClass("displayClass");
            $("#divImgCanvasTransfer").children().removeClass("hiddenClass").addClass("displayClass");
            $("#divImgCanvasTransfer").find("*").removeClass("hiddenClass").addClass("displayClass")
        }
        if (canMount == 0) {
            $("#divImgMounting").removeClass("displayClass").addClass("hiddenClass");
            $("#divImgMounting").children().removeClass("displayClass").addClass("hiddenClass");
            $("#divImgMounting").find("*").removeClass("displayClass").addClass("hiddenClass")
        } else {
            $("#divImgMounting").removeClass("hiddenClass").addClass("displayClass");
            $("#divImgMounting").children().removeClass("hiddenClass").addClass("displayClass");
            $("#divImgMounting").find("*").removeClass("hiddenClass").addClass("displayClass")
        }
        if (canLaminate == 0) {
            $("#divImgLaminating").removeClass("displayClass").addClass("hiddenClass");
            $("#divImgLaminating").children().removeClass("displayClass").addClass("hiddenClass");
            $("#divImgLaminating").find("*").removeClass("displayClass").addClass("hiddenClass")
        } else {
            $("#divImgLaminating").removeClass("hiddenClass").addClass("displayClass");
            $("#divImgLaminating").children().removeClass("hiddenClass").addClass("displayClass");
            $("#divImgLaminating").find("*").removeClass("hiddenClass").addClass("displayClass")
        }
        serviceButtonWide()
    }

    function ReplacePODConfigZoneProductIDInServiceURL(PODConfigID, ZoneProductID) {
        var strToBeReplacedPODConfigID;
        var selectedPODConfigID;
        var selectedProductID;
        var strToBeReplacedProductID;
        var strToBeReplacedSpecID;
        var selectedSpecID;
        selectedPODConfigID = "PODConfigID=" + PODConfigID;
        strToBeReplacedPODConfigID = GetParameterToBeReplaced($("#divImgMounting a").attr("href"), "PODConfigID=", "&");
        selectedProductID = "pd--" + ZoneProductID.substr(0, ZoneProductID.length - 1);
        strToBeReplacedProductID = GetParameterToBeReplaced($("#divImgMounting a").attr("href"), "pd--", "/");
        selectedSpecID = "sp--" + ZoneProductID.substr(ZoneProductID.length - 1, ZoneProductID.length).toLowerCase();
        strToBeReplacedSpecID = GetParameterToBeReplaced($("#divImgMounting a").attr("href"), "sp--", "/");
        $("#divImgCanvasTransfer a").attr("href", replaceString($("#divImgCanvasTransfer a").attr("href"), strToBeReplacedPODConfigID, selectedPODConfigID));
        $("#divImgMounting a").attr("href", replaceString($("#divImgMounting a").attr("href"), strToBeReplacedPODConfigID, selectedPODConfigID));
        $("#divImgLaminating a").attr("href", replaceString($("#divImgLaminating a").attr("href"), strToBeReplacedPODConfigID, selectedPODConfigID));
        $("#divImgCanvasTransfer a").attr("href", replaceString($("#divImgCanvasTransfer a").attr("href"), strToBeReplacedProductID, selectedProductID));
        $("#divImgMounting a").attr("href", replaceString($("#divImgMounting a").attr("href"), strToBeReplacedProductID, selectedProductID));
        $("#divImgLaminating a").attr("href", replaceString($("#divImgLaminating a").attr("href"), strToBeReplacedProductID, selectedProductID));
        $("#divImgCanvasTransfer a").attr("href", replaceString($("#divImgCanvasTransfer a").attr("href"), strToBeReplacedSpecID, selectedSpecID));
        $("#divImgMounting a").attr("href", replaceString($("#divImgMounting a").attr("href"), strToBeReplacedSpecID, selectedSpecID));
        $("#divImgLaminating a").attr("href", replaceString($("#divImgLaminating a").attr("href"), strToBeReplacedSpecID, selectedSpecID))
    }

    function replaceString(str, strToBeReplaced, newStr) {
        var reg, result;
        if (str != null) {
            reg = new RegExp(strToBeReplaced, "gi");
            result = str.replace(reg, newStr)
        }
        return result
    }

    function ApplyShadowToMainImage(canvasStretch, applyShadow, customerZoneID) {
        if (canvasStretch.toLowerCase() == "true") {
            $("#MainImage").removeClass("shadow");
            $("#MainImage").css("filter", "");
            $(".zoomPup").find("img").removeClass("shadow");
            if (customerZoneID == 3) {
                $("#wrap1").removeClass("wrap1").addClass("canvaswrap1");
                $("#wrap2").removeClass("wrap2").addClass("canvaswrap2");
                $("#wrap3").removeClass("wrap3").addClass("canvaswrap3")
            } else {
                $("#wrap1").removeClass("wrap1").addClass("canvaswhitewrap1");
                $("#wrap2").removeClass("wrap2").addClass("canvaswhitewrap2");
                $("#wrap3").removeClass("wrap3").addClass("canvaswhitewrap3")
            }
        } else {
            if (applyShadow == 0) {
                $("#MainImage").removeClass("shadow");
                $("#MainImage").css("filter", "");
                $(".zoomPup").find("img").removeClass("shadow");
                if (customerZoneID == 3) {
                    $("#wrap1").removeClass("wrap1").removeClass("canvaswrap1");
                    $("#wrap2").removeClass("wrap2").removeClass("canvaswrap2");
                    $("#wrap3").removeClass("wrap3").removeClass("canvaswrap3")
                } else {
                    $("#wrap1").removeClass("wrap1").removeClass("canvaswhitewrap1");
                    $("#wrap2").removeClass("wrap2").removeClass("canvaswhitewrap2");
                    $("#wrap3").removeClass("wrap3").removeClass("canvaswhitewrap3")
                }
            } else {
                $("#MainImage").addClass("shadow");
                $(".zoomPup").find("img").addClass("shadow");
                if (customerZoneID == 3) {
                    $("#wrap1").removeClass("canvaswrap1");
                    $("#wrap2").removeClass("canvaswrap2");
                    $("#wrap3").removeClass("canvaswrap3")
                } else {
                    $("#wrap1").removeClass("canvaswhitewrap1");
                    $("#wrap2").removeClass("canvaswhitewrap2");
                    $("#wrap3").removeClass("canvaswhitewrap3")
                }
            }
        }
    }

    function showFramingOptions(showFraming, APNum, PODConfigID, currentRow) {
        var bShowFrame, strAPNumToBeReplaced, selectedAPNum, strPODConfigIDtoBeReplaced, selectedPODconfigID;
        var FrameItButton = $(".cf_framecontainer");
        var customFrameLink = $(".frame_it_bttn");
        var GenericCustomFraming = $("#GenericCustomFraming");
        bShowFrame = false;
        if (showFraming.toLowerCase() == "true") {
            bShowFrame = true
        }
        if (bShowFrame) {
            FrameItButton.show();
            $("#prd-is-frameit").text("displayClass");
            customFrameLink.removeClass("hiddenClass").addClass("displayClass");
            strAPNumToBeReplaced = GetParameterToBeReplaced(GenericCustomFraming.html(), "APNum=", "&");
            selectedAPNum = "apnum=" + APNum;
            strPODConfigIDtoBeReplaced = GetParameterToBeReplaced(GenericCustomFraming.html(), "PODConfigID=", "&amp;");
            selectedPODconfigID = "PODConfigID=" + PODConfigID;
            $("#DLELink").show()
        } else {
            $("#DLELink").hide();
            FrameItButton.hide();
            $("#prd-is-frameit").text("hiddenClass");
            customFrameLink.removeClass("displayClass").addClass("hiddenClass")
        }
    }

    function GetParameterToBeReplaced(strHTML, str, delimiter) {
        var HTML, loc1, loc2, sub1;
        if (strHTML != null) {
            HTML = strHTML.toLowerCase();
            loc1 = HTML.indexOf(str.toLowerCase());
            loc2 = HTML.indexOf(delimiter, loc1);
            sub1 = HTML.substring(loc1, loc2)
        }
        return sub1
    }

    function formatMainImage() {
        $("#MainImageContainer").css("margin-left", 0);
        var iw = $("#MainImage").attr("width");
        var cw = $(".prd-hero-module").width();
        var mw = (cw - iw) / 2 + 5;
        $("#MainImageContainer").css("margin-left", mw + "px");
        $("#MainImageContainer").css("width", iw + "px")
    }

    $(document).on("click", "#divAddtoGallery", function (e) {
        SetDataForMyGalleries();
        var placeholderGallery = "";
        var moveToGallery = $(this).hasClass("mygalmovetogallerycontainer");
        var copyToGallery = $(this).hasClass("mygalcopytogallerycontainer");
        var obj = $(".prd-save-to-gallery");
        var prdWT = $("#prd-id-wt-state").text()
    });


    $(document).on("click", "#divAddtoWishList", function (e) {
        var PODConfigID, APNum, Title, ProductID, SpecID, selectedRow, ZoneProductID;
        selectedRow = $(".moreSizesDD_Row_selected");
        Title = $("#hdFormattedTitle").text();
        if (selectedRow.html() == null) {
            PODConfigID = $("#hdItemPODConfigID").text();
            APNum = $("#hdItemAPNum").text();
            ZoneProductID = $("#ZoneProductID").text()
        } else {
            PODConfigID = selectedRow.find("#moreSizesDD_selected_PODConfigID").text();
            APNum = selectedRow.find("#moreSizesDD_selected_APNum").text();
            ZoneProductID = selectedRow.find("#moreSizesDD_selected_zoneProductID").text()
        }
        ProductID = ZoneProductID.substr(0, ZoneProductID.length - 1);
        SpecID = ZoneProductID.substr(ZoneProductID.length - 1, ZoneProductID.length);
        doWLPopUP(this, "prd-cart-details-module", Title, ProductID, SpecID, APNum, PODConfigID)
    });
    var $popupContainer = $("#inPageFS");
    var gCloseText = "close";
    $popupContainer.dialog({modal: true, autoOpen: false, width: "900"});
    $popupContainer.dialog("option", "height", "725");
    $popupContainer.dialog("option", "title", gCloseText);
    $popupContainer.dialog("option", "position", ["center", "top"]);
    $popupContainer.dialog("open");
    $(".ui-widget").css("margin-top", "10px");
    $(".ui-dialog-content").css("min-height", "0");
    $(".ui-widget").css("height", "725px");
    $(document).on("click", ".ui-dialog-titlebar", function () {
        $("#inPageFS").dialog("close")
    });
    $(document).on("mouseenter", ".plaImage", function () {
        $("#plaZoomContainer").hide();
        var imgSrc = $(this).attr("src");
        imgSrc = imgSrc + "?w=200&h=183";
        var offset = $(this).offset();
        theItemTop = offset.top + parseInt($(this).css("padding-top"));
        var carouselOffset = $("#PLACarousel").offset();
        theItemLeft = offset.left;
        theItemHeight = $(this).height();
        theItemWidth = $(this).width();
        var hMax = 183;
        var wMax = 200;
        var aspectRatio = parseInt(theItemWidth) / parseInt(theItemHeight);
        if (aspectRatio >= 1) {
            zoomW = wMax;
            zoomH = hMax / aspectRatio
        } else {
            zoomW = wMax * aspectRatio;
            zoomH = hMax
        }
        topAdj = (zoomH - theItemHeight) / 2;
        leftAdj = (zoomW - theItemWidth) / 2;
        newTop = theItemTop - topAdj;
        newLeft = theItemLeft - leftAdj;
        newTop = (carouselOffset.top + (parseInt($("#PLACarousel").height()) - zoomH) / 2);
        var itemURL = $(this).parents(".plaItemContainer").find(".plaLink").text();
        $("#plaZoomContainer").data("itemURL", itemURL);
        $("#plaZoomContainer").css("top", newTop).css("left", newLeft);
        $("#plaZoom").attr("src", imgSrc);
        $("#plaZoomContainer").fadeIn()
    });
    $(document).on("mouseleave", ".plaImage", function () {
        plaT = setTimeout(function () {
            $("#plaZoom").attr("src", "http://cache1.artprintimages.com/images/misc/blank.gif");
            $("#plaZoomContainer").hide()
        }, 250)
    });
    $(document).on("mouseenter", ".plaTitle,#plaExploreLink,#plaViewMore", function () {
        $(this).addClass("hover")
    });
    $(document).on("mouseleave", ".plaTitle,#plaExploreLink,#plaViewMore", function () {
        $(this).removeClass("hover")
    });
    $(document).on("click", ".plaItemContainer", function () {
        var itemURL = $(this).find(".plaLink").text();
        location.href = itemURL
    });
    $(document).on("mouseleave", "#plaZoomContainer", function () {
        $("#plaZoom").attr("src", "http://cache1.artprintimages.com/images/misc/blank.gif");
        $(this).hide()
    });
    $(document).on("mouseenter", "#plaZoomContainer", function () {
        clearTimeout(plaT);
        $(this).show()
    });
    $(document).on("click", "#plaZoomContainer", function () {
        location.href = $(this).data("itemURL")
    });
    $(document).on("click", "#plaExploreLink,#plaViewMore", function () {
        location.href = $("#plaExploreLink").data("itemURL")
    });
    $(document).on("click", "#prd-tab-about-print", function () {
        if (!$(this).hasClass("selected")) {
            $(this).addClass("selected");
            $("#prd-tab-about-artist").removeClass("selected");
            $(".prd-tab-about-print").show();
            $(".prd-tab-about-artist").hide()
        }
    });
    $(document).on("click", "#prd-tab-about-artist", function () {
        if (!$(this).hasClass("selected")) {
            $(this).addClass("selected");
            $("#prd-tab-about-print").removeClass("selected");
            $(".prd-tab-about-artist").show();
            $(".prd-tab-about-print").hide()
        }
    });
    $(document).on("click", "#prd-tab-you-like", function () {
        if (!$(this).hasClass("selected")) {
            $(this).addClass("selected");
            $("#prd-tab-related-cat").removeClass("selected");
            $(".prd-tab-you-like").show();
            $(".prd-tab-related-cat").hide()
        }
    });
    $(document).on("click", "#prd-tab-related-cat", function () {
        if (!$(this).hasClass("selected")) {
            $(this).addClass("selected");
            $("#prd-tab-you-like").removeClass("selected");
            $(".prd-tab-related-cat").show();
            $(".prd-tab-you-like").hide()
        }
    })
});
function getValueInQS(b, c) {
    var a = new RegExp("[?&]" + b + "=([^&]*)").exec(c);
    return a && decodeURIComponent(a[1].replace(/\+/g, " "))
}
function updateHiResImage(b) {
    var e = $("#MainImage").attr("src");
    if (e.indexOf("/img/metal/") >= 0) {
        var c = b.split("?");
        var f = "?" + c[1];
        var g = getValueInQS("w", f);
        var a = getValueInQS("h", f);
        var d = "w=" + g + "&h=" + a;
        if (e.indexOf("?") >= 0) {
            d = "&" + d
        } else {
            d = "?" + d
        }
        b = e + d
    }
    return b
}

function setDLEPODParameters(e, a, b, c, d) {
}
function makeAjaxCallToAddToCartProductPage(a, b) {
    var d = a;
    var c = frameStepURL;
    if (com.art.core.utils.BrowserUtil.isMobile()) {
        c = cartPageURL
    }
    $.ajax({
        url: d, processData: false, cache: false, type: "GET", dataType: "text", success: function (e) {
            if ((e.indexOf("added#") > -1)) {
                theStatusSplit = e.split("#");
                theCartCount = theStatusSplit[1];
                cartItemId = theStatusSplit[2];
                c += "&cartCount=" + theCartCount;
                if (b && b.length > 0) {
                    c += "&IID=" + b
                } else {
                    c += "&IID=" + cartItemId
                }
                location.href = c
            } else {
            }
        }, error: function (f, e) {
        }
    })
}
function MouseEvent(a) {
    this.x = a.pageX;
    this.y = a.pageY
}
function ShowHideToolTipInfo(a) {
    var d = $("tooltipText");
    if ($("#tooltipText").length) {
        if (a == true) {
            var c = $("#prd-cart-details-module").offset();
            var b = (c.left) + 10;
            var f = $("#AutoTranslatedText").offset();
            var e = (f.top) + 50;
            $("#tooltipText").css({left: b + "px", top: e + "px"});
            $("#tooltipText").show()
        } else {
            $("#tooltipText").hide()
        }
    }
}
$(document).on("mouseover", ".LearnMoreBlock", function (a) {
    $(".LearnMoreBlock").css("background-color", "#0072BC")
});
$(document).on("mouseout", ".LearnMoreBlock", function (a) {
    $(".LearnMoreBlock").css("background-color", "#3F4D5B")
});
$(document).on("click", ".P2AModule", function (a) {
    trackGAEvent("_trackPageview", "/p2a-cta/product-page");
    setTimeout(function () {
        location.href = "/photostoart"
    }, 200);
    return false
});
function LoadZoomImage(a) {
    var c = getCropperUrl(a);
    c = updateHiResImage(c);
    var b = new com.art.core.utils.SuperZoom("productSZ", 340);
    b.showWithWaterMark(c);

}
function initTabs() {
    $("ul.prd-tabs li:not('.selected,.disabled')").on({
        mouseover: function () {
            $(this).css("backgroundColor", "#000000");
            $(this).css("border", "1px solid #000000");
            $(this).css("border-bottom", "1px solid #000000");
            $(this).css("color", "#FFFFFF")
        }, mouseout: function () {
            $(this).css("backgroundColor", "#F1F1F1");
            $(this).css("border", "1px solid #C6C6C6");
            $(this).css("border-bottom", "1px solid #F1F1F1");
            $(this).css("color", "#888888")
        }, click: function () {
            $(this).siblings().removeClass("selected");
            $(this).siblings().css("backgroundColor", "#F1F1F1");
            $(this).siblings().css("color", "#888888");
            $(this).css("backgroundColor", "#FFFFFF");
            $(this).css("border", "1px solid #C6C6C6");
            $(this).css("border-bottom", "1px solid #FFFFFF");
            $(this).css("color", "#000000");
            $(this).addClass("selected");
            var a = $(this).attr("id");
            a = "." + a;
            thisTabId = "#" + $(this).closest("div").attr("id");
            $(thisTabId + " .prd-tab-content > div").hide().parent().find(a).fadeIn("fast")
        }
    })
}
function initRecommendationTabs() {
    $("#pdp .cs-name").on({
        mouseenter: function () {
            if (!$(this).hasClass("cs-name-hover")) {
                $(this).addClass("cs-name-hover")
            }
        }, mouseleave: function () {
            if ($(this).hasClass("cs-name-hover")) {
                $(this).removeClass("cs-name-hover")
            }
        }
    });
    setTimeout("handleTabDisplay()", 1100)
}
function handleTabDisplay() {
    if (isATGRecoExist()) {
        if ($("#prd-tab-you-like").length > 0) {
            if (!$("#prd-tab-you-like").hasClass("disabled")) {
                $("#prd-tab-you-like").click()
            }
        }
    } else {
        if (!$("#prd-tab-you-like").hasClass("disabled")) {
            $("#prd-tab-you-like").addClass("disabled")
        }
        if ($("#prd-tab-related-cat").length > 0) {
            if (!$("#prd-tab-related-cat").hasClass("disabled")) {
                $("#prd-tab-related-cat").click()
            }
        }
    }
    if (($("#prd-tab-you-like").length <= 0 || $("#prd-tab-you-like").hasClass("disabled")) && ($("#prd-tab-related-cat").length <= 0 || $("#prd-tab-related-cat").hasClass("disabled"))) {
        if ($("#prd-recommendation-module").length > 0) {
            $("#prd-recommendation-module").hide()
        }
    }
}
function initProductPageButtons() {
    $(document).on("mouseover", ".frame_it_bttn", function () {
        if (!$(this).hasClass("frame_it_bttn_hover")) {
            $(this).addClass("frame_it_bttn_hover")
        }
    });
    $(document).on("mouseout", ".frame_it_bttn", function () {
        if ($(this).hasClass("frame_it_bttn_hover")) {
            $(this).removeClass("frame_it_bttn_hover")
        }
    });
    $(document).on("mouseover", ".add_to_cart_bttn", function () {
        if (!$(this).hasClass("add_to_cart_bttn_hover")) {
            $(this).addClass("add_to_cart_bttn_hover")
        }
    });
    $(document).on("mouseout", ".add_to_cart_bttn", function () {
        if ($(this).hasClass("add_to_cart_bttn_hover")) {
            $(this).removeClass("add_to_cart_bttn_hover")
        }
    });
    $(document).on("mouseover", ".save_to_gal_icon", function () {
        if (!$(this).hasClass("save_to_gal_icon_hover")) {
            $(this).addClass("save_to_gal_icon_hover")
        }
    });
    $(document).on("mouseout", ".save_to_gal_icon", function () {
        if ($(this).hasClass("save_to_gal_icon_hover")) {
            $(this).removeClass("save_to_gal_icon_hover")
        }
    });
    $(document).on("mouseover", ".mygalremoveicon, .mygalremovecontainer", function () {
        if (!$(".mygalremoveicon").hasClass("mygalremoveicon_hover")) {
            $(".mygalremoveicon").addClass("mygalremoveicon_hover")
        }
    });
    $(document).on("mouseout", ".mygalremoveicon, .mygalremovecontainer", function () {
        if ($(".mygalremoveicon").hasClass("mygalremoveicon_hover")) {
            $(".mygalremoveicon").removeClass("mygalremoveicon_hover")
        }
    });
    $(document).on("mouseover", ".mygalmovetogalleryicon, .mygalmovetogallerycontainer", function () {
        if (!$(".mygalmovetogalleryicon").hasClass("mygalmovetogalleryicon_hover")) {
            $(".mygalmovetogalleryicon").addClass("mygalmovetogalleryicon_hover")
        }
    });
    $(document).on("mouseout", ".mygalmovetogalleryicon, .mygalmovetogallerycontainer", function () {
        if ($(".mygalmovetogalleryicon").hasClass("mygalmovetogalleryicon_hover")) {
            $(".mygalmovetogalleryicon").removeClass("mygalmovetogalleryicon_hover")
        }
    });
    $(document).on("mouseover", ".mygalusername, .mygalgalleryname", function () {
        if (!$(this).hasClass("brdlnkover")) {
            $(this).addClass("brdlnkover")
        }
    });
    $(document).on("mouseout", ".mygalusername, .mygalgalleryname", function () {
        if ($(this).hasClass("brdlnkover")) {
            $(this).removeClass("brdlnkover")
        }
    });
    $(document).on("mouseover", ".mygalusername, .mygalusericon", function () {
        if (!$(".mygalusericon").hasClass("mygalusericonhover")) {
            $(".mygalusericon").addClass("mygalusericonhover")
        }
        if (!$(".mygalusername").hasClass("brdlnkover")) {
            $(".mygalusername").addClass("brdlnkover")
        }
    });
    $(document).on("mouseout", ".mygalusername, .mygalusericon", function () {
        if ($(".mygalusericon").hasClass("mygalusericonhover")) {
            $(".mygalusericon").removeClass("mygalusericonhover")
        }
        if ($(".mygalusername").hasClass("brdlnkover")) {
            $(".mygalusername").removeClass("brdlnkover")
        }
    });

}
function getSelectedApNum() {
    var a, b;
    b = $(".moreSizesDD_Row_selected");
    if (b.html() == null) {
        a = $("#hdItemAPNum").text()
    } else {
        a = b.find("#moreSizesDD_selected_APNum").text()
    }
    return a
}
function setupCustomerReviews() {
    $(".pr-submit-button, .pr-cancel-button").on({
        mouseenter: function () {
            if (!$(this).hasClass("pr-button-hover")) {
                $(this).addClass("pr-button-hover")
            }
        }, mouseleave: function () {
            if ($(this).hasClass("pr-button-hover")) {
                $(this).removeClass("pr-button-hover")
            }
        }
    })
}
function initJQZoom() {
    if ($("#ZoomLink").length > 0 && !com.art.core.utils.BrowserUtil.isiOSDevice()) {
        var a = $("#zoomURL").text();
        if (useSuperZoom) {
            var b = "watermarker/";
            var c = a;
            var d = (c.indexOf(b) > -1) ? c.substr(c.indexOf(b) + b.length) : c;
            a = getCropperUrl(d)
        }
        a = a.replace("watermarker/-", "watermarker/");
        a = updateHiResImage(a);
        $("#MainImage").wrap('<a href="' + a + '" class="jqzoom" title=""></a>');
        bindJQZoom()
    }
}
function bindJQZoom() {
    if (!com.art.core.utils.BrowserUtil.isiOSDevice()) {
        $(".jqzoom").jqzoom({
            zoomType: "reverse",
            lens: true,
            preloadImages: false,
            alwaysOn: false,
            zoomWidth: 200,
            zoomHeight: 200,
            xOffset: 24,
            yOffset: 0,
            position: "right",
            imageOpacity: 0.5,
            title: false,
            showEffect: "show",
            hideEffect: "hide",
            fadeinSpeed: "slow",
            fadeoutSpeed: "slow"
        });
        setZoomWindow = true;
        zoomSetAttempt = 0;
        $(document).on("mouseenter mouseover", ".zoomPad", function () {
            if (setZoomWindow) {
                var a = $("#MainImage");
                var d = $("#prd-title-module").offset();
                var c = a.offset();
                var b = $(".zoomPup").find("img");
                mainImageWidth = a.attr("width");
                mainImageHeight = a.attr("height");
                zwLeft = d.left - c.left;
                zwLeft = zwLeft - 6;
                if (a.hasClass("shadow")) {
                    b.addClass("shadow")
                } else {
                    b.removeClass("shadow")
                }
                b.width(mainImageWidth);
                b.height(mainImageHeight);
                $(".zoomWindow").css("left", zwLeft + "px");
                setTimeout("adjustZoomWindowSize()", 500);
                setZoomWindow = false
            }
        })
    }
}
function adjustZoomWindowSize() {
    if (zoomSetAttempt < 100) {
        zoomSetAttempt = zoomSetAttempt + 1;
        var b = $(".zoomWrapperImage").find("img");
        if (b.length > 0) {
            if (b.height() > 0 && b.width() > 0) {
                var f = 498;
                var a = $("#MainImage");
                var d = $("#prd-title-module").offset();
                var e = $("#prd-title-module").width();
                var c = a.offset();
                if (b.height() < 498) {
                    $(".zoomWrapperImage").height(b.height())
                }
                if (b.width() < 498) {
                    $(".zoomWrapper").width(b.width());
                    f = b.width()
                }
                zwLeft = d.left - c.left;
                zwLeft = zwLeft + 492;
                zwLeft = zwLeft - f;
                $(".zoomWindow").css("left", zwLeft + "px")
            } else {
                setTimeout("adjustZoomWindowSize()", 500)
            }
        } else {
            setTimeout("adjustZoomWindowSize()", 500)
        }
    }
}
function replaceProductImage(c, b) {
    b = updateHiResImage(b);
    if (!com.art.core.utils.BrowserUtil.isiOSDevice()) {
        var a = $(".zoomPad").find("#MainImage").clone();
        $(".zoomWindow").remove();
        $(".zoomPup").remove();
        $(".zoomPad").remove();
        $(".jqzoom").remove();
        a.attr("src", c);
        $("#MainImageShadow").html(a);
        $("#MainImage").wrap('<a href="' + b + '" class="jqzoom" title=""></a>');
        bindJQZoom()
    } else {
        $("#MainImage").attr("src", c)
    }
}
if (com.art.core.utils.BrowserUtil.isiOSDevice()) {
    $("#DLELink").hide()
}
function loadHouzzButton() {
    var a = "";
    a += '<a class="houzz-share-button" data-url="' + window.location + '" data-hzid="' + $("#hdnHouzzid").text() + '"  data-title="' + $("#hdnProdTitle").text() + '" data-img="' + $("#MainImage").attr("src") + '" data-desc="' + $("#hdnProdTitle").text() + '" data-category="Prints And Posters" data-showcount="0" href="#"></a>';
    a += '<script>(function (d, s, id) { if (!d.getElementById(id)) { var js = d.createElement(s); js.id = id; js.async = true; js.src = "//platform.houzz.com/js/widgets.js?" + (new Date().getTime()); var ss = d.getElementsByTagName(s)[0]; ss.parentNode.insertBefore(js, ss); } })(document, "script", "houzzwidget-js");</script>';
    $("#divhouzz").html(a);
    $(".prd-ratings").css("width", "500px !important");
    $("#gigya-sharebar").css("float", "left !important");
    $("#divhouzz").show()
}
function loadGigyaShareBar() {
    if ($("#showGigyaShareBar").text().toLowerCase() == "true") {
        try {
            console.log("VS_1::Loading GIGYA ShareBar during first mousemove")
        } catch (a) {
        }
        initGigyaShareBar();
        $("#gigya-sharebar").show()
    } else {
        try {
            console.log("VS_1::No GIGYA ShareBar detected during first mousemove")
        } catch (a) {
        }
    }
}
function loadSocialButtons() {
    var b = "";
    b += '<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pub=artdotcom"></script>';
    b += '<div id="prd-addthis-block"><a class="addthis_button_pinterest" pi:pinit:media="' + $("#prd-pinit-img").text() + '" pi:pinit:description="' + $("#prd-pinit-url").text() + '"></a><a class="addthis_button_tweet" tw:count="none"></a><a class="addthis_button_google_plusone" g:plusone:size="medium" g:plusone:count="false" g:plusone:annotation="none"></a><div id="prd-fb-like" class="prd-fb-like"></div></div>';
    if ($("#prd-show-social-block").text().toLowerCase() == "true") {
        try {
            console.log("KK_1::Loading Social Block during first mousemove")
        } catch (a) {
        }
        try {
            console.log(b)
        } catch (a) {
        }
        $("#prd-bookmark-block").html(b);
        initSocialButtons();
        $("#prd-bookmark-block").show()
    } else {
        try {
            console.log("KK_1::No Social Block detected during first mousemove")
        } catch (a) {
        }
    }
}
function loadSocialComments() {
    if ($("#prd-show-fb-comments").text().toLowerCase() == "true") {
        try {
            console.log("KK_1::Loading FB Comments during first mousemove")
        } catch (a) {
        }
        $("#prd-fb-comments").append('<div id="prd-facebook-comments"></div>');
        initSocialComments();
        $("#prd-fb-comments").show()
    } else {
        try {
            console.log("KK_1::No FB Comments detected during first mousemove")
        } catch (a) {
        }
    }
}
if (com.art.core.utils.BrowserUtil.isiOSDevice()) {
    var ua = navigator.userAgent, eventTrigger = (ua.match(/iPad/i)) ? "touchstart" : "mousemove";
    $(document).one(eventTrigger, function (a) {

        if (isAuthTokenExists()) {

        }
        loadBrightCoveJs();
        var b = setInterval(function () {
            if (!com.art.core.utils.ObjectUtil.isNullOrEmpty(window.artgigya) && MyGalleriesEnvironmentVariables != undefined) {
                clearInterval(b);
                if ($("#prd-gigya-plugin").text().toLowerCase() == "true") {
                    loadGigyaShareBar();
                    loadGigyaComments()
                } else {
                    loadSocialButtons();
                    loadSocialComments()
                }
            }
        }, 500)
    })
} else {
    $(document).one("mousemove", function (a) {

        if (isAuthTokenExists()) {

        }
        loadBrightCoveJs();
        var b = setInterval(function () {
            if (!com.art.core.utils.ObjectUtil.isNullOrEmpty(window.artgigya) && MyGalleriesEnvironmentVariables != undefined) {
                clearInterval(b);
                if ($("#prd-gigya-plugin").text().toLowerCase() == "true") {
                    loadGigyaShareBar();
                    loadGigyaComments()
                } else {
                    loadSocialButtons();
                    loadSocialComments()
                }
            }
        }, 500)
    })
}
var loadBrightCoveJsLoaded = false;
function loadBrightCoveJs() {
    if (!loadBrightCoveJsLoaded) {
        $.getScript("http://admin.brightcove.com/js/BrightcoveExperiences.js", function () {
            loadBrightCoveJsLoaded = true
        })
    }
}
function executeCurrentPriceWithBelowLabelChangesNarrow() {
    orangeLabel()
}
function executeCurrentPriceWithBelowLabelChangesWide() {
    serviceButtonWide()
}
function executeNewPriceWithBelowLabelChangesNarrow() {
    orangeLabel();
    priceChange()
}
function executeNewPriceWithBelowLabelChangesWide() {
    orangeLabel();
    priceChange();
    serviceButtonWide()
}
function executeNewPriceWithAboveLabelChangesNarrow() {
    orangeLabel();
    priceChange();
    saveToGalleryLabel()
}
function executeNewPriceWithAboveLabelChangesWide() {
    orangeLabel();
    priceChange();
    saveToGalleryLabel();
    serviceButtonWide()
}
function orangeLabel() {
    $(".save_to_gal_icon,.save_to_gal_icon_hover").css("color", "#F07E23");
    $(".save_to_gal_icon, .save_to_gal_icon_hover").css("width", "130px");
    $(".prd-save-to-gallery").css("font-size", "14px");
    $(".save_to_gal_icon").addClass("save_to_gal_icon_orange");
    $(document).on("mouseover", ".save_to_gal_icon", function () {
        $(".save_to_gal_icon").removeClass("save_to_gal_icon_hover");
        $(".save_to_gal_icon").removeClass("save_to_gal_icon_orange");
        if (!$(this).hasClass("save_to_gal_icon_orange_hover")) {
            $(this).addClass("save_to_gal_icon_orange_hover")
        }
    });
    $(document).on("mouseout", ".save_to_gal_icon", function () {
        $(".save_to_gal_icon").removeClass("save_to_gal_icon_hover");
        $(".save_to_gal_icon").addClass("save_to_gal_icon_orange");
        if ($(this).hasClass("save_to_gal_icon_orange_hover")) {
            $(this).removeClass("save_to_gal_icon_orange_hover")
        }
    })
}
function serviceButtonWide() {
    var a = 0, b = "";
    if ($("#divImgMounting").hasClass("displayClass")) {
        a += 1
    }
    if ($("#divImgLaminating").hasClass("displayClass")) {
        a += 1
    }
    if ($("#divImgCanvasTransfer").hasClass("displayClass")) {
        a += 1
    }
    if (a == 1) {
        b = "222px"
    } else {
        if (a == 2) {
            b = "110px"
        } else {
            if (a == 3) {
                b = "72px"
            }
        }
    }
    $("#A1, #A2, #A4").css("width", b)
}
function priceChange() {
    $("#prd-price-titles").hide();
    $("#prd-price-value").css("margin-top", "0px");
    $(".percentageSaved, .listPrice, .salePrice, .finalPrice").addClass("newPriceCI")
}
function saveToGalleryLabel() {
    $(".prd-save-to-gallery").css("margin-top", "0px");
    $(".prd-save-to-gallery").css("margin-bottom", "13px");
    $(".prd-save-to-gallery").css("padding", "3px");
    $(".prd-save-to-gallery").insertBefore($(".prd-cart-buttons"))
}


function callAPIwithLeafCategoryID(a, b, h, c, e, d, g) {
    var f = a + "/jsonp/CatalogItemSearch?apiKey=" + b + "&sessionId=" + h + "&searchText=&categoryIdList=" + c + "&artistCategoryId=0&numberOfRecords=17&pageNumber=1&sortBy=popularity&sortDirection=Down&maxThumbnailImageWidth=80,maxThumbnailImageHeight=80";
    $.ajax({
        type: "GET", url: f, cache: false, async: false, dataType: "jsonp", success: function (l) {
            var j, k;
            var m = false;
            if (l.OperationResponse.ResponseCode == "200" && l.ResultSummary != null && l.Items != null) {
                j = l.Items;
                k = l.ResultSummary.TotalRecordCount;
                m = true
            }
            if (m == true) {
                getPLACarousel(j, k, e, true, d, g)
            }
        }, error: function (k, j) {
        }
    })
}
function getPLACarousel(m, z, d, l, n, D) {
    var q = "";
    var r = z;
    var C = m;
    if (z > 17) {
        r = 17
    }
    if (z > 0) {
        for (var k = 0; k < r; k++) {
            var f = 0;
            var e = 0;
            var t = C[k].ImageInformation.SmallImage.Dimensions.Width;
            var s = C[k].ImageInformation.SmallImage.Dimensions.Height;
            var o = 115;
            var F = 0, j = 0, E = 5, p = 0;
            var a = t / s;
            if (a > 1) {
                F = o;
                j = o / a
            } else {
                F = o * a;
                j = o - 5
            }
            E = o - j;
            E = Math.floor(E);
            p = (144 - F) / 2;
            p = Math.floor(p);
            f = Math.floor(F);
            e = Math.floor(j);
            var u = C[k].ItemPrice.DisplayPrice;
            var v = C[k].ItemAttributes.Title;
            v = v.replace(/'/g, "'");
            var g = v;
            if (v.length > 24) {
                v = v.substring(0, 23);
                v = v + "..."
            }
            var A = C[k].ItemAttributes.ProductPageUrl;
            var b = C[k].ImageInformation.GenericImageUrl;
            b = b.replace("-M-", "-S-");
            q += '<li> <div class="plaItemContainer">';
            q += '<div class="plaImgContainer"><img class="plaImage" style="padding-top:' + E + 'px;" width="' + f + '" height="' + e + '" src="' + b + '" class="imageItem shadow" /></div>';
            q += '<div class="plaText"><span class="hidden plaLink" style="display:none;">' + A + "?isPLAItem=true&plaSearchText=" + D + '</span><div class="link"><span class="plaTitle link" title="' + g + '">' + v + '</span></div><div class="plaPrice">' + u + "</div></div>";
            q += "</div></li>"
        }
        var B;
        if (l) {
            B = "/gallery/id--" + n + "/" + d + ".htm"
        } else {
            B = "/asp/search_do.asp/_/posters.htm?searchstring=" + d + "&SSK=" + d + "&WT.oss=" + d + "&sby=all"
        }
        $("#PLAExplore").html('<span id="plaExploreLink" class="plaExploreLink link">Explore "' + d + '" ></span>');
        $("#plaExploreLink").data("itemURL", B);
        if (z > 17) {
            q += '<li><div class="" style="margin-top:55px;text-align:center;"><span id="plaViewMore" class="plaViewMore gCustomFont link">View More ></span></div></li>';
            r = r + 1
        }
        $("#ulPLACarousel").html(q);
        $("#ulPLACarousel").jcarousel({size: r, visible: 6, scroll: 6, initCallback: pla_initCallback});
        $("#PLACarousel").css("visibility", "visible");
        if (z == 6) {
            var c = $(".jcarousel-next").attr("class");
            c += "jcarousel-next-disabled";
            $(".jcarousel-next").attr("class", c)
        }
    }
}
function pla_initCallback(a, b) {
    $("#PLACarousel .jcarousel-next-horizontal").removeClass("jcarousel-next");
    $("#PLACarousel .jcarousel-prev-horizontal").removeClass("jcarousel-prev")
}

var isIE = (navigator.appVersion.indexOf("MSIE") != -1) ? true : false;
var isWin = (navigator.appVersion.toLowerCase().indexOf("win") != -1) ? true : false;
var isOpera = (navigator.userAgent.indexOf("Opera") != -1) ? true : false;
function ControlVersion() {
    var c;
    var a;
    var b;
    try {
        a = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");
        c = a.GetVariable("$version")
    } catch (b) {
    }
    if (!c) {
        try {
            a = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");
            c = "WIN 6,0,21,0";
            a.AllowScriptAccess = "always";
            c = a.GetVariable("$version")
        } catch (b) {
        }
    }
    if (!c) {
        try {
            a = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.3");
            c = a.GetVariable("$version")
        } catch (b) {
        }
    }
    if (!c) {
        try {
            a = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.3");
            c = "WIN 3,0,18,0"
        } catch (b) {
        }
    }
    if (!c) {
        try {
            a = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
            c = "WIN 2,0,0,11"
        } catch (b) {
            c = -1
        }
    }
    return c
}
function GetSwfVer() {
    var c = -1;
    if (navigator.plugins != null && navigator.plugins.length > 0) {
        if (navigator.plugins["Shockwave Flash 2.0"] || navigator.plugins["Shockwave Flash"]) {
            var d = navigator.plugins["Shockwave Flash 2.0"] ? " 2.0" : "";
            var b = navigator.plugins["Shockwave Flash" + d].description;
            var a = b.split(" ");
            var e = a[2].split(".");
            var f = e[0];
            var g = e[1];
            var h = a[3];
            if (h == "") {
                h = a[4]
            }
            if (h[0] == "d") {
                h = h.substring(1)
            } else {
                if (h[0] == "r") {
                    h = h.substring(1);
                    if (h.indexOf("d") > 0) {
                        h = h.substring(0, h.indexOf("d"))
                    }
                } else {
                    if (h[0] == "b") {
                        h = h.substring(1)
                    }
                }
            }
            var c = f + "." + g + "." + h
        }
    } else {
        if (navigator.userAgent.toLowerCase().indexOf("webtv/2.6") != -1) {
            c = 4
        } else {
            if (navigator.userAgent.toLowerCase().indexOf("webtv/2.5") != -1) {
                c = 3
            } else {
                if (navigator.userAgent.toLowerCase().indexOf("webtv") != -1) {
                    c = 2
                } else {
                    if (isIE && isWin && !isOpera) {
                        c = ControlVersion()
                    }
                }
            }
        }
    }
    return c
}
function DetectFlashVer(a, b, c) {
    versionStr = GetSwfVer();
    if (versionStr == -1) {
        return false
    } else {
        if (versionStr != 0) {
            if (isIE && isWin && !isOpera) {
                tempArray = versionStr.split(" ");
                tempString = tempArray[1];
                versionArray = tempString.split(",")
            } else {
                versionArray = versionStr.split(".")
            }
            var d = versionArray[0];
            var e = versionArray[1];
            var f = versionArray[2];
            if (d > parseFloat(a)) {
                return true
            } else {
                if (d == parseFloat(a)) {
                    if (e > parseFloat(b)) {
                        return true
                    } else {
                        if (e == parseFloat(b)) {
                            if (f >= parseFloat(c)) {
                                return true
                            }
                        }
                    }
                }
            }
            return false
        }
    }
}
function AC_AddExtension(d, a) {
    var c = d.indexOf("?");
    if (c != -1) {
        var b = d.substring(0, c);
        if (b.length >= a.length && b.lastIndexOf(a) == (b.length - a.length)) {
            return d
        } else {
            return d.replace(/\?/, a + "?")
        }
    } else {
        if (d.length >= a.length && d.lastIndexOf(a) == (d.length - a.length)) {
            return d
        } else {
            return d + a
        }
    }
}
function AC_Generateobj(c, d, a) {
    var e = "";
    if (isIE && isWin && !isOpera) {
        e += "<object ";
        for (var b in c) {
            e += b + '="' + c[b] + '" '
        }
        e += ">";
        for (var b in d) {
            e += '<param name="' + b + '" value="' + d[b] + '" /> '
        }
        e += "</object>"
    } else {
        e += "<embed ";
        for (var b in a) {
            e += b + '="' + a[b] + '" '
        }
        e += "> </embed>"
    }
    document.write(e)
}
function AC_FL_RunContent() {
    var a = AC_GetArgs(arguments, ".swf", "movie", "clsid:d27cdb6e-ae6d-11cf-96b8-444553540000", "application/x-shockwave-flash");
    AC_Generateobj(a.objAttrs, a.params, a.embedAttrs)
}
function AC_GetArgs(a, d, h, b, f) {
    var g = new Object();
    g.embedAttrs = new Object();
    g.params = new Object();
    g.objAttrs = new Object();
    for (var e = 0; e < a.length; e = e + 2) {
        var c = a[e].toLowerCase();
        switch (c) {
            case"classid":
                break;
            case"pluginspage":
                g.embedAttrs[a[e]] = a[e + 1];
                break;
            case"src":
            case"movie":
                a[e + 1] = AC_AddExtension(a[e + 1], d);
                g.embedAttrs.src = a[e + 1];
                g.params[h] = a[e + 1];
                break;
            case"onafterupdate":
            case"onbeforeupdate":
            case"onblur":
            case"oncellchange":
            case"onclick":
            case"ondblClick":
            case"ondrag":
            case"ondragend":
            case"ondragenter":
            case"ondragleave":
            case"ondragover":
            case"ondrop":
            case"onfinish":
            case"onfocus":
            case"onhelp":
            case"onmousedown":
            case"onmouseup":
            case"onmouseover":
            case"onmousemove":
            case"onmouseout":
            case"onkeypress":
            case"onkeydown":
            case"onkeyup":
            case"onload":
            case"onlosecapture":
            case"onpropertychange":
            case"onreadystatechange":
            case"onrowsdelete":
            case"onrowenter":
            case"onrowexit":
            case"onrowsinserted":
            case"onstart":
            case"onscroll":
            case"onbeforeeditfocus":
            case"onactivate":
            case"onbeforedeactivate":
            case"ondeactivate":
            case"type":
            case"codebase":
                g.objAttrs[a[e]] = a[e + 1];
                break;
            case"id":
            case"width":
            case"height":
            case"align":
            case"vspace":
            case"hspace":
            case"class":
            case"title":
            case"accesskey":
            case"name":
            case"tabindex":
                g.embedAttrs[a[e]] = g.objAttrs[a[e]] = a[e + 1];
                break;
            default:
                g.embedAttrs[a[e]] = g.params[a[e]] = a[e + 1]
        }
    }
    g.objAttrs.classid = b;
    if (f) {
        g.embedAttrs.type = f
    }
    return g
};
var GlobalRatio;
var GlobalOffset = 0;
var pdApnum = 0;
var SessionID = 0;
var gaDelayView;
var gaDelayScroll;
var gaArrowClickEventFiring = false;
var gitemsCnt = 0;
var wt_Enabled = "false";
var wt_Curr = "";
var wt_flag = "false";
var wt_SKU = "";
function ImageFlow() {
    this.defaults = {
        aspectRatio: 1.964,
        buttons: false,
        captions: true,
        captionsTop: true,
        centerItemID: 1,
        externalImgData: null,
        imageCursor: "default",
        ImageFlowID: "imageflow",
        imageFocusM: 9,
        imageFocusMax: 4,
        imageScaling: true,
        imagesHeight: 0.67,
        imagesM: 1,
        onClick: function () {
            if (gaDFEApplicable.toLowerCase() == "true") {
                var b = $("#captionAddToCart").data("clickitem");
                setDFETrackCookie("P", "I", b.FrameSku)
            }
            if (wt_Enabled == "true") {
                if (wt_flag != "first") {
                    wt_flag = "first"
                }
            }
            document.location = this.url
        },
        onStart: null,
        opacity: false,
        opacityArray: [10, 8, 6, 4, 2],
        partialLoad: false,
        percentLandscape: 118,
        percentOther: 100,
        preloadImages: true,
        reflections: true,
        reflectionGET: "",
        reflectionP: 0.5,
        reflectionPNG: false,
        scrollbarP: 0.6,
        slider: true,
        sliderCursor: "e-resize",
        sliderWidth: 14,
        startID: 3,
        startAnimation: false,
        xStep: 150
    };
    var a = this;
    this.init = function (g) {
        var h = ["aspectRatio", "buttons", "captions", "captionsTop", "centerItemID", "externalImgData", "imageCursor", "imagesM", "ImageFlowID", "imageFocusM", "imageFocusMax", "imagesHeight", "onClick", "onStart", "opacity", "opacityArray", "percentLandscape", "percentOther", "preloadImages", "partialLoad", "reflections", "reflectionGET", "reflectionP", "reflectionPNG", "imageScaling", "scrollbarP", "slider", "sliderCursor", "sliderWidth", "startID", "startAnimation", "xStep"];
        var e = h.length;
        for (var c = 0; c < e; c++) {
            var f = h[c];
            this[f] = (g !== undefined && g[f] !== undefined) ? g[f] : a.defaults[f]
        }
        var d = document.getElementById(a.ImageFlowID);
        if (d) {
            d.style.visibility = "visible";
            this.ImageFlowDiv = d;
            if (this.createStructure()) {
                this.imagesDiv = document.getElementById(a.ImageFlowID + "_images");
                this.captionDiv = document.getElementById(a.ImageFlowID + "_caption");
                this.captionTopDiv = document.getElementById(a.ImageFlowID + "_captionTop");
                this.navigationDiv = document.getElementById(a.ImageFlowID + "_navigation");
                this.scrollbarDiv = document.getElementById(a.ImageFlowID + "_scrollbar");
                this.sliderDiv = document.getElementById(a.ImageFlowID + "_slider");
                this.buttonNextDiv = document.getElementById(a.ImageFlowID + "_next");
                this.buttonPreviousDiv = document.getElementById(a.ImageFlowID + "_previous");
                this.indexArray = [];
                this.current = 0;
                this.imageID = 0;
                this.target = 0;
                this.memTarget = 0;
                this.firstRefresh = true;
                this.firstCheck = true;
                this.readyToUse = -1;
                this.busy = false;
                if (this.slider === false) {
                    this.scrollbarDiv.style.display = "none"
                }
                var j = this.ImageFlowDiv.offsetWidth;
                var b = Math.round(j / a.aspectRatio);
                this.loadingProgress()
            }
        }
    };
    this.getPercentOther = function () {
        return 100
    };
    this.prepareStartupImgs = function () {
        if (a.externalImgData != null) {
            $("#dfeDisplayingY").text(a.externalImgData.length);
            var b = $(this.ImageFlowDiv);
            if (a.centerItemID == 0) {
                a.centerItemID = 1
            }
            var e;
            for (var c = 1; c <= a.externalImgData.length; c++) {
                var d = $("<img />");
                if (c < (a.imageFocusMax + a.imageFocusMax + 2)) {
                    d.attr("src", a.externalImgData[c - 1].FrameImageUrl)
                } else {
                    d.attr("src", "http://cache1.allpostersimages.com/images/productPage/ajax-loader.gif");
                    d.attr("mkz", a.externalImgData[c - 1].FrameImageUrl)
                }
                e = (a.externalImgData[c - 1].ImageHeight > a.externalImgData[c - 1].ImageWidth) ? 'style="Height:400px;"' : 'style="width:400px"';
                b.append(d)
            }
        }
    };
    this.createStructure = function () {
        this.prepareStartupImgs();
        var p = a.Helper.createDocumentElement("div", "images");
        var t = null;
        var r = this.ImageFlowDiv.childNodes.length;
        var B, C, e = null;
        if (a.partialLoad) {
            B = a.startID - a.imageFocusMax;
            if (B < 0) {
                B = 0
            }
            C = B + a.imageFocusMax + a.imageFocusMax + 1
        }
        var d = 0;
        for (var q = 0; q < r; q++) {
            t = this.ImageFlowDiv.childNodes[q];
            if (t && t.nodeType == 1 && t.nodeName == "IMG") {
                var o = t.cloneNode(true);
                if (a.partialLoad && (d < B || d > C)) {
                    var b = $(o);
                    if (e == null) {
                        e = b.attr("src")
                    }
                    b.data("src", b.attr("src"));
                    b.attr("src", e);
                    p.appendChild(b.get(0))
                } else {
                    p.appendChild(o)
                }
                d++
            }
        }
        var j = a.Helper.createDocumentElement("div", "caption");
        var k = a.Helper.createDocumentElement("div", "captionTop");
        j.style.display = "none";
        var v = a.Helper.createDocumentElement("div", "scrollbar");
        var w = a.Helper.createDocumentElement("div", "slider");
        v.appendChild(w);
        if (a.buttons) {
            var h = a.Helper.createDocumentElement("div", "previous", "imageFlowButtonPrev");
            var g = a.Helper.createDocumentElement("div", "next", "imageFlowButtonNext");
            v.appendChild(h);
            v.appendChild(g)
        }
        var s = a.Helper.createDocumentElement("div", "navigation");
        s.appendChild(j);
        $(this.ImageFlowDiv).parent().prepend(k);
        var c = $('<div id="scrollbarDivContainer" ></div').append(v);
        var z = false;
        if (a.ImageFlowDiv.appendChild(p) && a.ImageFlowDiv.appendChild(s)) {
            for (q = 0; q < r; q++) {
                t = this.ImageFlowDiv.childNodes[q];
                if (t && t.nodeType == 1 && t.nodeName == "IMG") {
                    this.ImageFlowDiv.removeChild(t)
                }
            }
            z = true
        }
        $("#myImageFlow_navigation").append(c);
        var f = $.trim($("#addToCart").text());
        var n = $.trim($("#prd-frameit-event").text());
        if (n == null || n == "") {
            n = "javascript:customFrameItem();"
        }
        var l = "javascript:customDFEFrameItem();";
        if ($("#prd-product-type").length > 0) {
            var u = $("#prd-product-type").text()
        }
        $("#myImageFlowParent").prepend($('<span id="coverflowToolbar"> <a id="colorPickerAnchor"><img id="colorpickericon" src="http://cache1.artprintimages.com/images/dfe/color_picker_icon.png" /></a><div class="colorPicker" id="colorPicker"> <div id="wallcolor"> <div id="colorPickerTop"><div class="title" >' + $("#gTxt_select_wall_color").text() + '</div> <a id="colorPickerClose"><img src="http://cache1.artprintimages.com/images/dfe/wall_color_close.gif"/></a></div><ul id="iconmenu" /> </div> </div> </span>'));
        if (com.art.core.utils.BrowserUtil.isiOSDevice()) {
            $("#myImageFlow_caption").append($('<div id="imgCaption" class="gCenterDiv"><div class="captionContent" id="captionContent" ><span id="captionDetail" /><span id="captionPrice"/> </div><div id="captionAddToCart" class="gCenterDiv"><div style="" class="add_to_cart_bttn dfe_add_to_cart_bttn" id="prd-add-to-cart-dfe"><div class="dfe_add_to_cart_text gCustomFont">Заказать</div></div></div></div>'))
        } else {
            if ((u.indexOf("Stretched Canvas Print") != -1) || (u.indexOf("Framed Canvas Print") != -1)) {
                $("#myImageFlow_caption").append($('<div id="imgCaption" class="gCenterDiv"><div class="captionContent" id="captionContent" ><span id="captionDetail" /><span id="captionPrice"/> </div><div id="captionAddToCart" class="gCenterDiv"><div style="" class="add_to_cart_bttn dfe_add_to_cart_bttn" id="prd-add-to-cart-dfe"><div class="dfe_add_to_cart_text gCustomFont">Заказать</div></div></div></div>'))
            } else {
                $("#myImageFlow_caption").append($('<div id="imgCaption" class="gCenterDiv"><div class="captionContent" id="captionContent" ><span id="captionDetail" /><span id="captionPrice"/> </div><div id="captionAddToCart" class="gCenterDiv"><div style="" class="add_to_cart_bttn dfe_add_to_cart_bttn" id="prd-add-to-cart-dfe"><div class="dfe_add_to_cart_text gCustomFont">Заказать</div></div></div><div class="clear"></div><div class="dfe_design_your_own txtlabel" onclick="' + l + '">Design Your own</div></div>'))
            }
        }
        c.click(this.hitScrollContainer);
        if (com.art.core.utils.BrowserUtil.isiOSDevice()) {
            var A = navigator.userAgent, m = (A.match(/iPad/i)) ? "touchstart" : "click";
            $("#captionAddToCart").bind(m, this.addToCartClicked)
        } else {
            $("#captionAddToCart").click(this.addToCartClicked)
        }
        return z
    };
    this.addToCartClicked = function () {
        var f = $("#captionAddToCart").data("clickitem");
        if (gaDFEApplicable.toLowerCase() == "true") {
            setDFETrackCookie("D", "A", f.FrameSku)
        }
        if (f.IsDFEItem == true) {
            var d = "/asp/place_order.asp/_/posters.htm?CREATE_BSK=Y";
            makeAjaxCallToAddToCartDFE(d, f.FrameSku)
        } else {
            var h = $("#PODConfigID").text();
            var e = $("#dimVals").text();
            var j = $("#SessionID").text();
            var c = f.DestinationURL;
            var g;
            var k;
            var b;
            var l = f.ZoneProductID;
            g = l.substr(0, l.length - 1);
            k = l.substr(l.length - 1, l.length);
            b = "/asp/place_order.asp/_/pdta--" + g;
            b = b + "/sp--" + k;
            b = b + "/posters.htm?Add_to_Cart=Y&RFID=&PODConfigID=" + h + "&dimvals=" + e + "&iku=" + f.FrameSku + "&pdApnum=" + pdApnum;
            b = b + "&IID=&ui=" + j;
            f.addToCartAction = b;
            linkForm(f.addToCartAction)
        }
        if (wt_Enabled == "true") {
            var m = $("#moreSizesDD_selected_zoneProductID").text()
        }
    };
    this.hitScrollContainer = function (b) {
        var c = $("#myImageFlow_previous").offset().left + 21;
        var g = $("#myImageFlow_next").offset().left;
        var d = $("#myImageFlow_slider").offset().left;
        var f = d + a.sliderWidth + a.sliderWidth;
        if (c < b.pageX && b.pageX < d) {
            a.MouseWheel.handle(1)
        } else {
            if (f < b.pageX && b.pageX < g) {
                a.MouseWheel.handle(-1)
            }
        }
    };
    this.loadingProgress = function () {
        var b = a.loadingStatus();
        if (b < 100 || a.firstCheck === true && a.preloadImages === true) {
            if (a.firstCheck === true && b == 100) {
                a.firstCheck = false;
                window.setTimeout(a.loadingProgress, 100)
            } else {
                window.setTimeout(a.loadingProgress, 40)
            }
        } else {
            $("#myImageFlowParent").removeClass("busyBackground");
            window.setTimeout(a.Helper.addResizeEvent, 1000);
            a.MouseWheel.init();
            a.MouseDrag.init();
            a.Touch.init();
            a.Key.init();
            a.refresh(true);
            document.getElementById(a.ImageFlowID + "_scrollbar").style.visibility = "visible";
            var c = a.startID - 1;
            a.glideTo(c);
            if (a.startAnimation === true) {
                a.moveTo(5000)
            }
            a.readyToUse = 0;
            if ($.browser.msie && $.browser.version == "6.0") {
            } else {
            }
            if ($.browser.msie && $.browser.version == "8.0" && $("#langID").text() == 6) {
                $("#myImageFlow").css("height", "313px")
            }
        }
    };
    this.loadingStatus = function () {
        var g = a.imagesDiv.childNodes.length;
        var d = 0, b = 0;
        var e = null;
        for (var f = 0; f < g; f++) {
            e = a.imagesDiv.childNodes[f];
            if (e && e.nodeType == 1 && e.nodeName == "IMG") {
                if (e.complete === true) {
                    b++
                }
                d++
            }
        }
        var c = Math.round((b / d) * 100);
        return c
    };
    this.refresh = function () {
        this.imagesDivWidth = a.imagesDiv.offsetWidth + a.imagesDiv.offsetLeft;
        this.maxHeight = Math.round(a.imagesDivWidth / a.aspectRatio);
        this.maxFocus = a.imageFocusMax * a.xStep;
        this.size = a.imagesDivWidth * 0.5;
        this.sliderWidth = a.sliderWidth * 0.5;
        this.scrollbarWidth = 350;
        this.imagesDivHeight = 250;
        this.captionTopDivHeight = 236;
        a.imagesDiv.style.height = a.imagesDivHeight + "px";
        a.navigationDiv.style.marginTop = ((a.externalImgData[0].ImageHeight - 190) / 2) + "px";
        a.captionDiv.style.width = a.imagesDivWidth + "px";
        if ($.browser.msie) {
            a.captionDiv.style.marginTop = 20
        } else {
            a.captionDiv.style.marginTop = 5
        }
        a.captionDiv.style.paddingTop = Math.round(a.imagesDivWidth * 0.02) + "px";
        a.captionDiv.style.display = "";
        a.scrollbarDiv.style.width = a.scrollbarWidth + "px";
        if ($.browser.msie) {
            a.scrollbarDiv.style.marginTop = "42px"
        } else {
            a.scrollbarDiv.style.marginTop = "45px"
        }
        a.scrollbarDiv.style.marginLeft = "100px";
        a.sliderDiv.style.cursor = a.sliderCursor;
        a.sliderDiv.onmousedown = function () {
            a.MouseDrag.start(this);
            return false
        };
        if (a.buttons) {
            a.buttonPreviousDiv.onclick = function () {
                a.MouseWheel.handle(1);
                return false
            };
            a.buttonNextDiv.onclick = function () {
                a.MouseWheel.handle(-1);
                return false
            }
        }
        var f = (a.reflections === true) ? a.reflectionP + 1 : 1;
        var e = a.imagesDiv.childNodes.length;
        var b = 0;
        var c = null;
        for (var d = 0; d < e; d++) {
            c = a.imagesDiv.childNodes[d];
            if (c !== null && c.nodeType == 1 && c.nodeName == "IMG") {
                this.indexArray[b] = d;
                if (a.externalImgData[b].IsDFEItem == true) {
                    c.url = a.externalImgData[b].DestinationURL + "&ui=" + SessionID
                } else {
                    c.url = a.externalImgData[b].DestinationURL + "?pdApnum=" + pdApnum + "&iku=" + a.externalImgData[b].FrameSku + "&ui=" + SessionID
                }
                c.xPosition = (-b * a.xStep);
                c.i = b;
                if (a.firstRefresh) {
                    c.w = a.externalImgData[b].ImageWidth;
                    c.h = a.externalImgData[b].ImageHeight
                }
                if (c.w > c.h) {
                    c.pc = a.percentLandscape;
                    c.pcMem = a.percentLandscape
                } else {
                    c.pc = a.percentOther;
                    c.pcMem = a.percentOther
                }
                if (a.imageScaling === false) {
                    c.style.position = "relative";
                    c.style.display = "inline"
                }
                b++
            }
        }
        this.max = a.indexArray.length;
        if (a.imageScaling === false) {
            c = a.imagesDiv.childNodes[a.indexArray[0]];
            this.totalImagesWidth = c.w * a.max;
            c.style.paddingLeft = (a.imagesDivWidth / 2) + (c.w / 2) + "px";
            a.imagesDiv.style.height = c.h + "px";
            a.navigationDiv.style.height = (a.maxHeight - c.h) + "px"
        }
        if (a.firstRefresh) {
            a.firstRefresh = false
        }
        a.glideTo(a.imageID);
        a.moveTo(a.current)
    };
    this.consolidateSize = function (b) {
        if (346 < b && b < 354) {
            return 350
        }
        if (316 < b && b < 324) {
            return 320
        }
        return b
    };
    this.moveTo = function (j) {
        this.current = j;
        this.zIndex = a.max;
        var b;
        for (var e = 0; e < a.max; e++) {
            var d = a.imagesDiv.childNodes[a.indexArray[e]];
            var c = e * -a.xStep;
            if (a.imageScaling) {
                if ((c + a.maxFocus) < a.memTarget || (c - a.maxFocus) > a.memTarget) {
                    d.style.visibility = "hidden";
                    d.style.display = "none"
                } else {
                    var l = (Math.sqrt(10000 + j * j) + 100) * ((e == a.imageID) ? 1 : a.imagesM);
                    var k = j / l * (a.size + 60) + a.size;
                    d.style.display = "block";
                    var f = Math.round(d.h / l * 200);
                    var h = 0;
                    switch (f > a.maxHeight) {
                        case false:
                            h = Math.round(d.w * f / d.h);
                            break;
                        default:
                            f = a.maxHeight;
                            h = d.w * f / d.h;
                            break
                    }
                    f = this.consolidateSize(f);
                    h = this.consolidateSize(h);
                    var g = (a.imagesDivHeight - f) + ((f / (a.reflectionP + 1)) * a.reflectionP) + 15 - GlobalOffset;
                    d.style.left = k - (h / 2) + "px";
                    if (h && f) {
                        if (h == 320 && 1 > GlobalRatio) {
                            d.style.height = "";
                            b = $(d);
                            b.removeAttr("width");
                            b.removeAttr("height")
                        } else {
                            d.style.height = f + "px"
                        }
                        if (f == 350 || (f == 320 && 1 <= GlobalRatio && GlobalRatio <= 1.2)) {
                            d.style.width = "";
                            b = $(d);
                            b.removeAttr("width");
                            b.removeAttr("height")
                        } else {
                            d.style.width = h + "px"
                        }
                        d.style.top = g + "px";
                        if ($.browser.msie && items.length == 1) {
                            var b = $(d);
                            b.removeAttr("width");
                            b.removeAttr("height")
                        }
                    }
                    d.style.visibility = "visible";
                    switch (j < 0) {
                        case true:
                            this.zIndex++;
                            break;
                        default:
                            this.zIndex = a.zIndex - 1;
                            break
                    }
                    switch (d.i == a.imageID) {
                        case false:
                            d.onclick = function () {
                                a.glideTo(this.i)
                            };
                            break;
                        default:
                            this.zIndex = a.zIndex + 1;
                            if (d.url !== "") {
                                d.onclick = a.onClick
                            }
                            break
                    }
                    d.style.zIndex = a.zIndex
                }
            } else {
                if ((c + a.maxFocus) < a.memTarget || (c - a.maxFocus) > a.memTarget) {
                    d.style.visibility = "hidden"
                } else {
                    d.style.visibility = "visible";
                    switch (d.i == a.imageID) {
                        case false:
                            d.onclick = function () {
                                a.glideTo(this.i)
                            };
                            break;
                        default:
                            if (d.url !== "") {
                                d.onclick = a.onClick
                            }
                            break
                    }
                }
                a.imagesDiv.style.marginLeft = (j - a.totalImagesWidth) + "px"
            }
            j += a.xStep
        }
    };
    this.glideTo = function (g) {
        if (a.onStart != null && a.readyToUse === 0) {
            a.readyToUse = 1;
            a.onStart()
        }
        var k = -g * a.xStep;
        this.target = k;
        this.memTarget = k;
        $(a.imagesDiv.childNodes[this.imageID]).removeClass("centerItem");
        this.imageID = g;
        var b = a.externalImgData[g].productDisplayName;
        $("#dfeDisplayingX").text(this.imageID + 1 + "&nbsp;");
        var c = $("#gTxt_displaying").text();
        if (b === "" || a.captions === false) {
            b = "&nbsp;"
        }
        if (gaDFEApplicable.toLowerCase() == "true") {
            var f = $(".captionTop").text().split("/");
            $("#myImageFlow_captionTop").attr("pselected", f[0])
        }
        $("#myImageFlow_captionTop").text("");
        $("#myImageFlow_captionTop").text((this.imageID + 1) + "/" + gitemsCnt);
        var j = a.externalImgData[g].PriceConfig.RegularPrice;
        var h = a.externalImgData[g].PriceConfig.Price;
        var e = "<span class='dfe_regPrice'>" + a.externalImgData[g].PriceConfig.DisplayRegularPrice + "</span>";
        var d = "<span class='dfe_salePrice'>" + a.externalImgData[g].PriceConfig.DisplayPrice + "</span>";
        $("#captionPrice").html($("<div/>").html(d).html());
        $("#captionDetail").text(a.externalImgData[g].ProductDisplayName);
        $("#captionAddToCart").data("clickitem", a.externalImgData[g]);
        $(a.imagesDiv.childNodes[g]).addClass("centerItem");
        if (a.MouseDrag.busy === false) {
            if (a.max == 1) {
                this.newSliderX = -a.MouseDrag.newX
            } else {
                this.newSliderX = (g * (a.scrollbarWidth - a.sliderWidth - a.sliderWidth)) / (a.max - 1) - a.MouseDrag.newX
            }
            if ($.browser.msie && $.browser.version == "6.0") {
                a.sliderDiv.style.marginLeft = (a.newSliderX - 20) + "px"
            } else {
                a.sliderDiv.style.marginLeft = a.newSliderX + "px"
            }
        }
        if (a.opacity === true || a.imageFocusM !== a.defaults.imageFocusM) {
            a.imagesDiv.childNodes[g].pc = 1
        }
        if (a.busy === false) {
            window.setTimeout(a.animate, 50);
            a.busy = true
        }
        $("#myImageFlow").data("timestamp", new Date())
    };
    this.animate = function () {
        switch (a.target < a.current - 1 || a.target > a.current + 1) {
            case true:
                a.moveTo(a.current + (a.target - a.current) / 3);
                window.setTimeout(a.animate, 50);
                a.busy = true;
                break;
            default:
                a.busy = false;
                break
        }
    };
    this.MouseWheel = {
        init: function () {
            if (window.addEventListener) {
                a.ImageFlowDiv.addEventListener("DOMMouseScroll", a.MouseWheel.get, false)
            }
            a.Helper.addEvent(a.ImageFlowDiv, "mousewheel", a.MouseWheel.get)
        }, get: function (c) {
            var b = 0;
            if (!c) {
                c = window.event
            }
            if (c.wheelDelta) {
                b = c.wheelDelta / 120
            } else {
                if (c.detail) {
                    b = -c.detail / 3
                }
            }
            if (b) {
                a.MouseWheel.handle(b)
            }
            a.Helper.suppressBrowserDefault(c)
        }, handle: function (c) {
            var b = false;
            var d = 0;
            if (c > 0) {
                if (a.imageID >= 1) {
                    d = a.imageID - 1;
                    b = true
                }
            } else {
                if (a.imageID < (a.max - 1)) {
                    d = a.imageID + 1;
                    b = true
                }
            }
            if (b === true) {
                a.glideTo(d);
                setMouseGA()
            }
        }
    };
    this.MouseDrag = {
        object: null, objectX: 0, mouseX: 0, newX: 0, busy: false, init: function () {
            a.Helper.addEvent(a.ImageFlowDiv, "mousemove", a.MouseDrag.drag);
            a.Helper.addEvent(a.ImageFlowDiv, "mouseup", a.MouseDrag.stop);
            a.Helper.addEvent(document, "mouseup", a.MouseDrag.stop);
            a.ImageFlowDiv.onselectstart = function () {
                var b = true;
                if (a.MouseDrag.busy === true) {
                    b = false
                }
                return b
            }
        }, start: function (b) {
            a.MouseDrag.object = b;
            a.MouseDrag.objectX = a.MouseDrag.mouseX - b.offsetLeft + a.newSliderX
        }, stop: function () {
            a.MouseDrag.object = null;
            a.MouseDrag.busy = false
        }, drag: function (b) {
            var f = 0;
            if (!b) {
                b = window.event
            }
            if (b.pageX) {
                f = b.pageX
            } else {
                if (b.clientX) {
                    f = b.clientX + document.body.scrollLeft + document.documentElement.scrollLeft
                }
            }
            a.MouseDrag.mouseX = f;
            if (a.MouseDrag.object !== null) {
                var d = (a.MouseDrag.mouseX - a.MouseDrag.objectX) + a.sliderWidth;
                if (d < (-a.newSliderX)) {
                    d = -a.newSliderX
                }
                if (d > (a.scrollbarWidth - a.sliderWidth - a.sliderWidth - a.newSliderX)) {
                    d = a.scrollbarWidth - a.sliderWidth - a.sliderWidth - a.newSliderX
                }
                var g = (d + a.newSliderX) / ((a.scrollbarWidth - a.sliderWidth - a.sliderWidth) / (a.max - 1));
                var c = Math.round(g);
                a.MouseDrag.newX = d;
                a.MouseDrag.object.style.left = d + "px";
                if (a.imageID !== c) {
                    a.glideTo(c);
                    setMouseGA()
                }
                a.MouseDrag.busy = true
            }
        }
    };
    this.Touch = {
        x: 0, startX: 0, stopX: 0, busy: false, first: true, init: function () {
            a.Helper.addEvent(a.navigationDiv, "touchstart", a.Touch.start);
            a.Helper.addEvent(document, "touchmove", a.Touch.handle);
            a.Helper.addEvent(document, "touchend", a.Touch.stop)
        }, isOnNavigationDiv: function (b) {
            var c = false;
            if (b.touches) {
                var d = b.touches[0].target;
                if (d === a.navigationDiv || d === a.sliderDiv || d === a.scrollbarDiv) {
                    c = true
                }
            }
            return c
        }, getX: function (b) {
            var c = 0;
            if (b.touches) {
                c = b.touches[0].pageX
            }
            return c
        }, start: function (b) {
            a.Touch.startX = a.Touch.getX(b);
            a.Touch.busy = true;
            a.Helper.suppressBrowserDefault(b)
        }, isBusy: function () {
            var b = false;
            if (a.Touch.busy === true) {
                b = true
            }
            return b
        }, handle: function (b) {
            if (a.Touch.isBusy && a.Touch.isOnNavigationDiv(b)) {
                if (a.Touch.first) {
                    a.Touch.stopX = ((a.max - 1) - a.imageID) * (a.imagesDivWidth / (a.max - 1));
                    a.Touch.first = false
                }
                var d = -(a.Touch.getX(b) - a.Touch.startX - a.Touch.stopX);
                if (d < 0) {
                    d = 0
                }
                if (d > a.imagesDivWidth) {
                    d = a.imagesDivWidth
                }
                a.Touch.x = d;
                var c = Math.round(d / (a.imagesDivWidth / (a.max - 1)));
                c = (a.max - 1) - c;
                if (a.imageID !== c) {
                    a.glideTo(c)
                }
                a.Helper.suppressBrowserDefault(b)
            }
        }, stop: function () {
            a.Touch.stopX = a.Touch.x;
            a.Touch.busy = false
        }
    };
    this.Key = {
        init: function () {
            document.onkeydown = function (b) {
                a.Key.handle(b)
            }
        }, handle: function (c) {
            var b = a.Key.get(c);
            switch (b) {
                case 39:
                    a.MouseWheel.handle(-1);
                    break;
                case 37:
                    a.MouseWheel.handle(1);
                    break
            }
        }, get: function (b) {
            b = b || window.event;
            return b.keyCode
        }
    };
    this.Helper = {
        addEvent: function (c, d, b) {
            if (c.addEventListener) {
                c.addEventListener(d, b, false)
            } else {
                if (c.attachEvent) {
                    c["e" + d + b] = b;
                    c[d + b] = function () {
                        c["e" + d + b](window.event)
                    };
                    c.attachEvent("on" + d, c[d + b])
                }
            }
        }, setOpacity: function (b, c) {
            if (a.opacity === true) {
                b.style.opacity = c / 10;
                b.style.filter = "alpha(opacity=" + c * 10 + ")"
            }
        }, createDocumentElement: function (e, c, d) {
            var b = document.createElement(e);
            b.setAttribute("id", a.ImageFlowID + "_" + c);
            if (d !== undefined) {
                c += " " + d
            }
            b.setAttribute("class", c);
            b.setAttribute("className", c);
            return b
        }, suppressBrowserDefault: function (b) {
            if (b.preventDefault) {
                b.preventDefault()
            } else {
                b.returnValue = false
            }
            return false
        }, addResizeEvent: function () {
        }
    }
}
var domReadyEvent = {
    name: "domReadyEvent", events: {}, domReadyID: 1, bDone: false, DOMContentLoadedCustom: null, add: function (a) {
        if (!a.$$domReadyID) {
            a.$$domReadyID = this.domReadyID++;
            if (this.bDone) {
                a()
            }
            this.events[a.$$domReadyID] = a
        }
    }, remove: function (a) {
        if (a.$$domReadyID) {
            delete this.events[a.$$domReadyID]
        }
    }, run: function () {
        if (this.bDone) {
            return
        }
        this.bDone = true;
        for (var a in this.events) {
            this.events[a]()
        }
    }, schedule: function () {
        if (this.bDone) {
            return
        }
        if (/KHTML|WebKit/i.test(navigator.userAgent)) {
            if (/loaded|complete/.test(document.readyState)) {
                this.run()
            } else {
                setTimeout(this.name + ".schedule()", 100)
            }
        } else {
            if (document.getElementById("__ie_onload")) {
                return true
            }
        }
        if (typeof this.DOMContentLoadedCustom === "function") {
            if (typeof document.getElementsByTagName !== "undefined" && (document.getElementsByTagName("body")[0] !== null || document.body !== null)) {
                if (this.DOMContentLoadedCustom()) {
                    this.run()
                } else {
                    setTimeout(this.name + ".schedule()", 250)
                }
            }
        }
        return true
    }, init: function () {
        if (document.addEventListener) {
            document.addEventListener("DOMContentLoaded", function () {
                domReadyEvent.run()
            }, false)
        }
        setTimeout("domReadyEvent.schedule()", 100);
        function run() {
            domReadyEvent.run()
        }

        if (typeof addEvent !== "undefined") {
            addEvent(window, "load", run)
        } else {
            if (document.addEventListener) {
                document.addEventListener("load", run, false)
            } else {
                if (typeof window.onload === "function") {
                    var oldonload = window.onload;
                    window.onload = function () {
                        domReadyEvent.run();
                        oldonload()
                    }
                } else {
                    window.onload = run
                }
            }
        }
        /*@cc_on
         @if (@_win32 || @_win64)
         document.write("<script id=__ie_onload defer src=\"//:\"><\/script>");
         var script = document.getElementById("__ie_onload");
         script.onreadystatechange = function()
         {
         if (this.readyState == "complete")
         {
         domReadyEvent.run(); // call the onload handler
         }
         };
         @end
         @*/
    }
};
String.prototype.endsWith = function (a) {
    return (this.substr(this.length - a.length) === a)
};
function prepareCoverFlowData(b) {
    for (i = 0; i < items.length; i++) {
        $("#greatphoto").attr({alt: "Beijing Brush Seller", title: "photo by Kelly Clark"});
        var a = $("<img />").attr({
            src: items[i].url,
            longdesc: com.art.core.utils.StringUtil.format("test {0} is here {1}", items[i].ProductDisplayName, "xxx"),
            width: "200"
        });
        b.append(a)
    }
}
function setDelayGA() {
    var a = $(".captionTop").text().replace("/", " of ");
    trackGAWithCategory("DFE", "DFE product image " + a + " - viewed", "Product Page")
}
function setDelayGAScroll() {
    if (!gaArrowClickEventFiring) {
        trackGAWithCategory("DFE", "DFE slider - slider scrolled", "Product Page")
    }
    gaArrowClickEventFiring = false
}
function setMouseGA() {
    if (gaDFEApplicable.toLowerCase() == "true") {
        if (gaDelayScroll != undefined && gaDelayScroll != null) {
            clearTimeout(gaDelayScroll)
        }
        gaDelayScroll = window.setTimeout("setDelayGAScroll()", 1000);
        if ($("#myImageFlow_images img").length == 15) {
            if (gaDelayView != undefined && gaDelayView != null) {
                clearTimeout(gaDelayView)
            }
            gaDelayView = window.setTimeout("setDelayGA()", 1000)
        }
    }
}
function adjustData() {
    GlobalRatio = items[0].ImageHeight / items[0].ImageWidth;
    if (GlobalRatio < 0.8) {
        GlobalOffset = (1 - GlobalRatio) * 100
    }
    var a = 320;
    if (GlobalRatio > 1.2) {
        a = 350
    }
    for (i = 0; i < items.length; i++) {
        if (items[i].ImageHeight > a) {
            items[i].ImageWidth = items[i].ImageWidth * a / items[i].ImageHeight;
            items[i].ImageHeight = a
        }
        if (items[i].ImageWidth > a) {
            items[i].ImageHeight = items[i].ImageHeight * a / items[i].ImageWidth;
            items[i].ImageWidth = a
        }
    }
}
$(document).ready(function () {
    wt_Enabled = $("#wtEnabled").text();
    wt_Curr = $("#currencyCode").text();
    if (document.URL.endsWith("#dfeModule")) {
        $(document).trigger("mousemove")
    } else {
        $("#dfeModule").hide()
    }
});
$(document).ready(function () {
    var a;
    a = com.art.core.utils.BrowserUtil.isiOSDevice();
    if (a == true) {
        LoadDfe()
    }
    $(document).one("mousemove", function (b) {
        if (a == false) {
            LoadDfe()
        }
    })
});
function LoadDfe() {
    var a = $("#currentAPNum").text();
    var c = $("#dfePODConfigID").text();
    if ($("#DFEServiceURL").length > 0) {
        try {
            console.log("KK_1::Loading DFE")
        } catch (b) {
        }
        LoadCoverFlow(a, c)
    } else {
        try {
            console.log("KK_1::No DFE detected")
        } catch (b) {
        }
    }
}
function LoadCoverFlow(c, k) {
    $("#myImageFlow").empty();
    $("#coverflowToolbar").remove();
    $("#myImageFlow_captionTop").remove();
    var f;
    var h = 15;
    var e = $("#frameSku").text();
    var m = $("#zoneID").text();
    var g = $("#langID").text();
    var a = $("#currencyCode").text();
    var b = $("#pdAPNum").text();
    var j = $("#nonDFEframeSku").text();
    var d = $("#DFEServiceURL").text();
    if (k === "") {
        k = 0
    }
    if (e === "" || j !== "") {
        e = j
    }
    var l = d + "/ajax/GetFrameConfigurationsForCoverFlow?method=?&PrintAPNum=" + c + "&PrintPODConfigID=" + k + "&Crop=0&ColorID=0&ColorName=&StyleID=0&StyleName=&MaxConfigurations=" + h + "&CustomerZoneID=" + m + "&LanguageID=" + g + "&FitFixedFrame=true&CurrencyCode=" + a + "&WriteToDB=true&ImageMaxW=350&ImageMaxH=350&ShowTrace=false&frameSku=" + e + "&Domain=com&MergePreFrameData=true&SendLiteResponse=true";
    $.ajax({
        url: l,
        type: "get",
        cache: false,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (p) {
            if (p.d != undefined) {
                gitemsCnt = p.d.FrameCount;
                items = p.d.FrameConfigurationsLite
            } else {
                gitemsCnt = p.FrameCount;
                items = p.FrameConfigurationsLite
            }
            $("#myImageFlow_captionTop").text("3/" + gitemsCnt);
            if (items.length == 0) {
                $("#dfeModule").hide();
                $(".PreCutomFrameOuter").hide();
                $("#customFraming").show();
                $("#DFELoadedStatus").text("NoDFELoad")
            } else {
                $("#dfeModule").show();
                $(".PreCutomFrameOuter").show();
                $("#customFraming").hide();
                $("#ga_event_dfeload").text("DFE module - loaded");
                $("#ga_dfeload_page").text("Product Page");
                pdApnum = c;
                if (pdApnum === "") {
                    pdApnum = c
                }
                SessionID = $("#SessionID").text();
                var s;
                if (items.length > 3) {
                    s = items[0];
                    items[0] = items[1];
                    items[1] = items[2];
                    items[2] = s
                } else {
                    if (items.length == 3) {
                        s = items[0];
                        items[0] = items[1];
                        items[1] = s
                    }
                }
            }
            if (items.length > 0) {
                adjustData()
            }
            var r;
            if (items.length > 3) {
                r = 3
            } else {
                if (items.length == 3) {
                    r = 2
                } else {
                    r = items.length
                }
            }
            var q = new ImageFlow();
            q.init({
                aspectRatio: 1.07924,
                buttons: true,
                captions: true,
                centerItemID: 1,
                externalImgData: items,
                ImageFlowID: "myImageFlow",
                imageFocusM: 1.2,
                imageFocusMax: 2,
                imagesM: 1.2,
                imagesHeight: 0.61,
                onStart: myStart,
                partialLoad: false,
                percentLandscape: 150,
                percentOther: 170,
                reflections: false,
                sliderWidth: 82,
                startID: r,
                xStep: 110
            });
            $(".preFrameInner").css("height", "99%");
            if (gaDFEApplicable != "undefined" && gaDFEApplicable != "" && gaDFEApplicable.toLowerCase() == "true") {
                var n = $("#ga_event_dfeload").text();
                var o = $("#ga_dfeload_page").text();
                if (n != undefined && n != "" && o != undefined && o != "") {
                    trackGAWithCategory("DFE", n, o, true)
                }
                SetDFEGATracking()
            }
        },
        complete: function () {
            $("#DFELoadedStatus").text("DFELoaded")
        },
        error: function (q, p, o) {
            try {
                console.log("error in ajax response");
                console.log(q.statusText)
            } catch (n) {
            }
        }
    })
}
function toggleIt() {
    if ($("#myFlatView").length < 2) {
        buildFlatView()
    }
}
function buildFlatView() {
    var a = $("#myFlatView");
    busyStatusMode(true);
    for (i = 0; i < items.length; i++) {
    }
    busyStatusMode(false)
}
function myStart() {
    $("#myImageFlow img").each(function () {
        if ($(this).attr("mkz")) {
            $(this).attr("src", $(this).attr("mkz"));
            $(this).attr("mkz", "")
        }
    })
}
function getParameterByName(a) {
    a = a.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var c = "[\\?&]" + a + "=([^&#]*)";
    var b = new RegExp(c);
    var d = b.exec(window.location.href);
    if (d == null) {
        return ""
    } else {
        return d[1]
    }
}
var colorMap = {
    c0: "ffe3e4",
    c1: "fff4d6",
    c2: "fff7d0",
    c3: "efede7",
    c4: "ebf0e4",
    c5: "edf7fa",
    c6: "ffffff",
    c7: "e2c7c7",
    c8: "f0debd",
    c9: "eee7c3",
    c10: "d4d0c2",
    c11: "ced7d3",
    c12: "cfd9dd",
    c13: "d6d7d3",
    c14: "9f6d73",
    c15: "e4bd8b",
    c16: "decfad",
    c17: "bdb1a5",
    c18: "94a6a1",
    c19: "b3c4ce",
    c20: "868f94",
    c21: "733b4b",
    c22: "ca9153",
    c23: "907761",
    c24: "847467",
    c25: "526965",
    c26: "607d90",
    c27: "4c545b",
    c28: "84303d",
    c29: "8b4a26",
    c30: "584035",
    c31: "584d43",
    c32: "364643",
    c33: "31343a",
    c34: "1c2d3c"
};
function createColorItems(e, b) {
    var a = $("#iconmenu");
    if (wt_Enabled == "true") {
        for (i = 0; i < e; i++) {
            var c = "WT.ti";
            var d = "Change Wall Color"
        }
    } else {
        for (i = 0; i < e; i++) {
            a.append($("<li id='c" + i + "'><a href='#'></a></li>").css("background-position", -(b * i) + "px 0px"))
        }
    }
}
function createRoomItems(c, b) {
    var a = $("#roommenu");
    for (i = 0; i < c; i++) {
        a.append($("<li id='r" + i + "'><a href='#'></a></li>").css("background-position", -(b * i) + "px 0px"))
    }
}
function createColorPicker() {
    createColorItems(35, 33);
    $("#colorpickericon").click(function () {
        toggle_colorpicker()
    });
    $("#colorPicker").click(function (b) {
        b.preventDefault();
        var a = colorMap[$(b.target).parent().attr("id")];
        $("#myImageFlowParent").css("background-color", "#" + a);
        hide_colorpicker()
    });
    $("#colorPickerClose").click(function () {
        hide_colorpicker()
    })
}
function toggle_colorpicker() {
    var a = $("#colorPicker").css("display");
    if (a == "none") {
        show_colorpicker()
    } else {
        hide_colorpicker()
    }
}
function show_colorpicker() {
    $("#colorPicker").css({display: "block", position: "relative", top: "-245px", left: "-222px"})
}
function hide_colorpicker() {
    $("#colorPicker").css("display", "none")
}
$(document).ready(function () {
    $("#myImageFlow_previous").on({
        mouseenter: function () {
            if (!$(this).hasClass("hoverprvsBtn")) {
                $(this).addClass("hoverprvsBtn")
            }
        }, mouseleave: function () {
            if ($(this).hasClass("hoverprvsBtn")) {
                $(this).removeClass("hoverprvsBtn")
            }
        }
    });
    $("#myImageFlow_next").on({
        mouseenter: function () {
            if (!$(this).hasClass("hovernxtBtn")) {
                $(this).addClass("hovernxtBtn")
            }
        }, mouseleave: function () {
            if ($(this).hasClass("hovernxtBtn")) {
                $(this).removeClass("hovernxtBtn")
            }
        }
    });
    $("#myImageFlow_slider").on({
        mouseenter: function () {
            if (!$(this).hasClass("scrollBarHvr")) {
                $(this).addClass("scrollBarHvr")
            }
        }, mouseleave: function () {
            if ($(this).hasClass("scrollBarHvr")) {
                $(this).removeClass("scrollBarHvr")
            }
        }
    });
    $("#myImageFlow_slider").on({
        mousedown: function () {
            if (!$(this).hasClass("scrollBarHvr")) {
                $(this).addClass("scrollBarHvr")
            }
        }, mouseup: function () {
            if ($(this).hasClass("scrollBarHvr")) {
                $(this).removeClass("scrollBarHvr")
            }
        }
    })
});

