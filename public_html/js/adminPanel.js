/**
 * Created by cofirazak on 02/09/15.
 */
function ajaxAdminPanelSubmit(action) {
    var data;
    var infoDialog = $('#infoDialog');
    switch (action) {
        case 'addCategory':
            var addCategoryButton = $("#addCategoryButton");
            var addedCategory = $('input[name=categoryName]').val();
            data = $('#addCategoryForm').serialize();
            addCategoryButton.prop('disabled', true);
            break;
        case 'updateCategory':
            var updateCategoryButton = $("#updateCategoryButton");
            var updatedCategory = $('input[name=updateCategoryName]').val();
            data = $('#updateCategoryForm').serialize();
            updateCategoryButton.prop('disabled', true);
            break;
        case 'deleteCategory':
            var deleteCategoryButton = $("#deleteCategoryButton");
            var deletedCategory = $('select[name=categoryId]').eq(1).find('option:selected').html();
            data = $('#deleteCategoryForm').serialize();
            deleteCategoryButton.prop('disabled', true);
            break;
        case 'addProductType':
            var addProductTypeButton = $("#addProductTypeButton");
            var addedProductType = $('input[name=productTypeName]').val();
            data = $('#addProductTypeForm').serialize();
            addProductTypeButton.prop('disabled', true);
            break;
        case 'updateProductType':
            var updateProductTypeButton = $("#updateProductTypeButton");
            var updatedProductType = $('input[name=updateProductTypeName]').val();
            data = $('#updateProductTypeForm').serialize();
            updateProductTypeButton.prop('disabled', true);
            break;
        case 'deleteProductType':
            var deleteProductTypeButton = $("#deleteProductTypeButton");
            var deletedProductType = $('select[name=productTypeId]').eq(1).find('option:selected').html();
            data = $('#deleteProductTypeForm').serialize();
            deleteProductTypeButton.prop('disabled', true);
            break;
        case 'addProduct':
            var productName = $('textarea[name=productName]').val();
            var productDesc = $('textarea[name=productDescription]').val();
            var productTypeId = $('select[name=productTypeId]').find('option:selected').val();
            var productSize = $('input[name=productSize]').val();
            var productPrice = $('input[name=productPrice]').val();
            var productImage = $('select[name=productImage]').find('option:selected').val();
            data = $('#addProductForm').serialize();
            break;
        case 'deleteProduct':
            var productTypeName = $('#productName').find('option:selected').html();
            var deleteProductButton = $("#deleteProductButton");
            data = $('#deleteProductForm').serialize();
            deleteProductButton.prop('disabled', true);
            break;
        case 'deleteImage':
            data = $('#deleteImageForm').serialize();
            $.ajax({
                type: 'post',
                url: 'admin',
                data: data,
                response: 'text',
                async: true,
                success: function (result) {
                    $('.imagesList').each(function(){
                        $(this).html(result);
                    });
                }
            });
            return false;
            break;
        default:
            return false;
    }

    var emptyAddCategory = action === 'addCategory' && addedCategory === '';
    var emptyUpdateCategory = action === 'updateCategory' && updatedCategory === '';
    var emptyDelCategory = action === 'deleteCategory' && deletedCategory === undefined;
    var emptyAddProductType = action === 'addProductType' && addedProductType === '';
    var emptyUpdateProductType = action === 'updateProductType' && updatedProductType === '';
    var emptyDelProductType = action === 'deleteProductType' && deletedProductType === undefined;
    var emptyProductName = action === 'addProduct' && productName === '';
    var emptyProductDesc = action === 'addProduct' && productDesc === '';
    var emptyProductTypeId = action === 'addProduct' && productTypeId === undefined;
    var emptyProductSize = action === 'addProduct' && productSize === '';
    var emptyProductPrice = action === 'addProduct' && productPrice === '';
    var emptyProductImage = action === 'addProduct' && productImage === '';
    if (!emptyAddCategory && !emptyUpdateCategory && !emptyDelCategory &&
        !emptyAddProductType && !emptyUpdateProductType && !emptyDelProductType &&
        !emptyProductName && !emptyProductDesc && !emptyProductTypeId && !emptyProductSize &&
        !emptyProductPrice && ! emptyProductImage) {
        $.ajax({
            type: 'post',
            url: 'admin',
            data: data,
            response: 'text',
            async: true,
            success: function (result) {
                var productList = $('.productList');
                switch (action) {
                    case 'addCategory':
                    case 'updateCategory':
                    case 'deleteCategory':
                        $('.categoryList').each(function(){
                            $(this).html(result);
                        });
                        $('input[name=categoryName]').val('');
                        $('input[name=updateCategoryName]').val('');
                        break;
                    case 'addProductType':
                    case 'updateProductType':
                    case 'deleteProductType':
                        $('.productTypeList').each(function(){
                            $(this).html(result);
                        });
                        $('input[name=productTypeName]').val('');
                        $('input[name=updateProductTypeName]').val('');
                        break;
                    case 'addProduct':
                        var resultArray = JSON.parse(result);
                        if (resultArray['ok'] === 1) {
                            infoDialog.html('Добавлен новый продукт.');
                            makeDialog(infoDialog);
                            infoDialog.show();
                            $('textarea[name=productName]').val('');
                            $('textarea[name=productDescription]').val('');
                            $('input[name=productSize]').val('');
                            $('input[name=productPrice]').val('');
                            productList.each(function(){
                                $(this).html(resultArray['products']);
                            });
                        } else {
                            infoDialog.html('Ошибка ' + resultArray['error']);
                            makeDialog(infoDialog);
                            infoDialog.show();
                        }
                        break;
                    case 'deleteProduct':
                        productList.each(function(){
                            $(this).html(result);
                        });
                        break;
                    default:
                        return false;
                }
            },
            complete: function () {
                var errorCategory = $('#errorCategory').text();
                var errorProductType = $('#errorProductType').text();
                if (errorCategory === '' && errorProductType === '') {
                    switch (action) {
                        case 'addCategory':
                            infoDialog.html('Добавлена категория: ' + addedCategory);
                            makeDialog(infoDialog);
                            infoDialog.show();
                            addCategoryButton.prop('disabled', false);
                            break;
                        case 'updateCategory':
                            infoDialog.html('Название категории изменено на: ' + updatedCategory);
                            makeDialog(infoDialog);
                            infoDialog.show();
                            updateCategoryButton.prop('disabled', false);
                            break;
                        case 'deleteCategory':
                            infoDialog.html('Удалена категория: ' + deletedCategory);
                            makeDialog(infoDialog);
                            infoDialog.show();
                            deleteCategoryButton.prop('disabled', false);
                            break;
                        case 'addProductType':
                            infoDialog.html('Добавлен тип продукта: ' + addedProductType);
                            makeDialog(infoDialog);
                            infoDialog.show();
                            addProductTypeButton.prop('disabled', false);
                            break;
                        case 'updateProductType':
                            infoDialog.html('Тип продукта ' + updatedProductType + ' обновлен');
                            makeDialog(infoDialog);
                            infoDialog.show();
                            updateProductTypeButton.prop('disabled', false);
                            break;
                        case 'deleteProductType':
                            infoDialog.html('Удален тип продукта: ' + deletedProductType);
                            makeDialog(infoDialog);
                            infoDialog.show();
                            deleteProductTypeButton.prop('disabled', false);
                            break;
                        case 'deleteProduct':
                            infoDialog.html('Удален продукт: ' + productTypeName);
                            makeDialog(infoDialog);
                            infoDialog.show();
                            deleteProductButton.prop('disabled', false);
                            break;
                        default:
                            return false;
                    }
                }
            }
        });
    } else {
        switch (action) {
            case 'addCategory':
                addCategoryButton.prop('disabled', false);
                break;
            case 'updateCategory':
                updateCategoryButton.prop('disabled', false);
                break;
            case 'deleteCategory':
                deleteCategoryButton.prop('disabled', false);
                break;
            case 'addProductType':
                addProductTypeButton.prop('disabled', false);
                break;
            case 'updateProductType':
                updateProductTypeButton.prop('disabled', false);
                break;
            case 'deleteProductType':
                deleteProductTypeButton.prop('disabled', false);
                break;
            case 'addProduct':
                if (emptyProductName) {
                    infoDialog.html('Заполните поле "Название продукта"');
                    makeDialog(infoDialog);
                    infoDialog.show();
                } else if (emptyProductDesc) {
                    infoDialog.html('Заполните поле "Описание продукта"');
                    makeDialog(infoDialog);
                    infoDialog.show();
                } else if (emptyProductTypeId) {
                    infoDialog.html('Заполните поле "Относится к типу"');
                    makeDialog(infoDialog);
                    infoDialog.show();
                } else if (emptyProductSize) {
                    infoDialog.html('Заполните поле Размер');
                    makeDialog(infoDialog);
                    infoDialog.show();
                } else if (emptyProductPrice) {
                    infoDialog.html('Заполните поле Цена');
                    makeDialog(infoDialog);
                    infoDialog.show();
                } else if (emptyProductImage) {
                    infoDialog.html('Добавьте изображение');
                    makeDialog(infoDialog);
                    infoDialog.show();
                } else {
                    infoDialog.html('Ошибка 714');
                    makeDialog(infoDialog);
                    infoDialog.show();
                }
                break;
        }
    }

    return false;
}

function makeDialog(id) {
    id.dialog({
        modal: true,
        buttons: {
            Ok: function () {
                $(this).dialog("close");
            }
        }
    });
}
