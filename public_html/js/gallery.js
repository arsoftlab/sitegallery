/**
 * Created by php on 04.10.15.
 */

function updateProductList(productID)
{
    var data = {'action': 'updateProductList', 'productId': productID};
    $.ajax({
        type: 'post',
        url: '../../src/Gallery.php',
        data: data,
        response: 'text',
        async: true,
        success: function (result) {
            $('.galmin').html(result);
        }
    });
}
