<?php
/**
 * Created by PhpStorm.
 * User: cofirazak
 * Date: 09/10/15
 * Time: 22:00
 */

namespace siteGallery;

require_once 'vendor/autoload.php';
require_once 'src/AdminPanel.php';
require_once 'src/Gallery.php';
require_once 'src/Front.php';

use siteGallery\src\AdminPanel;
use siteGallery\src\Front;
use siteGallery\src\Gallery;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$routes = new RouteCollection();
$route = new Route('/', array('controller' => 'src/Front.php'));
$routes->add('/', $route);
$route = new Route('/admin', array('controller' => 'src/AdminPanel.php'));
$routes->add('AdminPanel', $route);
$route = new Route('/gallery', array('controller' => 'src/Gallery.php'));
$routes->add('Gallery', $route);

$context = new RequestContext();
$context->fromRequest(Request::createFromGlobals());

$matcher = new UrlMatcher($routes, $context);
try {
    $parameters = $matcher->match($context->getPathInfo());
} catch (\Exception $e) {
    echo "<h1 style='text-align: center;'>Введите корректный адрес</h1><h3>Введено:{$context->getPathInfo()}</h3>";
}
if (isset($parameters['_route'])) {
    switch ($parameters['_route']) {
        case '/':
            new Front();
            break;
        case 'AdminPanel':
            new AdminPanel();
            break;
        case 'Gallery':
            new Gallery();
            break;
    }
}
